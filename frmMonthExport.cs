﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmMonthExport : Form
    {
        public string DataType = string.Empty;
        public int year;
        public int month;
        clsDatabases database = new clsDatabases();
        clsControls control = new clsControls();

        public frmMonthExport()
        {
            InitializeComponent();
        }

        private void frmMonthExport_Load(object sender, EventArgs e)
        {
            lbMessage.Text = "";
            var query = string.Empty;
            switch (DataType)
            {
                case "CONDENSATE":
                    query = "SELECT DISTINCT CONVERT(VARCHAR, YEAR(MONTH)) YEAR FROM VSP_CONDENSATE ORDER BY YEAR DESC";
                    break;
                default:
                    query = "SELECT DISTINCT CONVERT(VARCHAR, YEAR(MONTH)) YEAR FROM VSP_PRODUCTION ORDER BY YEAR DESC";
                    break;
            }
            control.FillControls(cboNam, query);
            cboThang.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            switch (DataType)
            {
                case "CONDENSATE":
                    CheckifExistCondensateData();
                    break;
                default:
                    CheckifExistProductionData();
                    break;
            }
        }

        private void CheckifExistProductionData()
        {
            if ((cboNam.SelectedIndex >= 0) && (cboThang.SelectedIndex >= 0))
            {
                bool bExist = false;
                string sqlText = "SELECT DISTINCT MONTH FROM VSP_PRODUCTION";
                DataTable table = database.FillDataTable(sqlText, CommandType.Text);
                foreach (DataRow dr in table.Rows)
                {
                    if ((Convert.ToDateTime(dr[0].ToString()).Month == Convert.ToInt16(cboThang.Text.Substring(6))) &&
                            (Convert.ToDateTime(dr[0].ToString()).Year == Convert.ToInt16(cboNam.Text)))
                    {
                        bExist = true;
                        month = Convert.ToInt16(cboThang.Text.Substring(6));
                        year = Convert.ToInt16(cboNam.Text);
                        break;
                    }
                }
                if (bExist)
                    btnOK.DialogResult = DialogResult.OK;
                else
                {
                    lbMessage.Text = "Tháng " + cboThang.Text.Substring(6)
                        + " năm " + cboNam.Text + " chưa có số liệu!";
                    btnOK.DialogResult = DialogResult.None;
                }

            }
            else lbMessage.Text = "Chọn tháng năm cần xuất số liệu!";
        }

        private void CheckifExistCondensateData()
        {
            if ((cboNam.SelectedIndex >= 0) && (cboThang.SelectedIndex >= 0))
            {
                bool bExist = false;
                string sqlText = "SELECT DISTINCT MONTH FROM VSP_CONDENSATE";
                DataTable table = database.FillDataTable(sqlText, CommandType.Text);
                foreach (DataRow dr in table.Rows)
                {
                    if ((Convert.ToDateTime(dr[0].ToString()).Month == Convert.ToInt16(cboThang.Text.Substring(6))) &&
                            (Convert.ToDateTime(dr[0].ToString()).Year == Convert.ToInt16(cboNam.Text)))
                    {
                        bExist = true;
                        month = Convert.ToInt16(cboThang.Text.Substring(6));
                        year = Convert.ToInt16(cboNam.Text);
                        break;
                    }
                }
                if (bExist)
                    btnOK.DialogResult = DialogResult.OK;
                else
                {
                    lbMessage.Text = "Tháng " + cboThang.Text.Substring(6)
                        + " năm " + cboNam.Text + " chưa có số liệu!";
                    btnOK.DialogResult = DialogResult.None;
                }

            }
            else lbMessage.Text = "Chọn tháng năm cần xuất số liệu!";
        }

        private void cboThang_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbMessage.Text = "";
            if (cboNam.SelectedIndex >= 0)
            {
                switch (DataType)
                {
                    case "CONDENSATE":
                        CheckifExistCondensateData();
                        break;
                    default:
                        CheckifExistProductionData();
                        break;
                }
            }
        }

        private void cboNam_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbMessage.Text = "";
            if (cboThang.SelectedIndex >= 0)
            {
                switch (DataType)
                {
                    case "CONDENSATE":
                        CheckifExistCondensateData();
                        break;
                    default:
                        CheckifExistProductionData();
                        break;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
