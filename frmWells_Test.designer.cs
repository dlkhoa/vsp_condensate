﻿namespace TrueTech.VSP.VSPCondensate
{
    partial class frmWells_Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWells_Test));
            this.gridControlWellHdr = new DevExpress.XtraGrid.GridControl();
            this.gridViewWellHdr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.uwi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.well_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.well_number = new DevExpress.XtraGrid.Columns.GridColumn();
            this.well_number_sort = new DevExpress.XtraGrid.Columns.GridColumn();
            this.platform = new DevExpress.XtraGrid.Columns.GridColumn();
            this.field = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWellHdr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWellHdr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControlWellHdr
            // 
            this.gridControlWellHdr.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlWellHdr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlWellHdr.Location = new System.Drawing.Point(3, 3);
            this.gridControlWellHdr.MainView = this.gridViewWellHdr;
            this.gridControlWellHdr.Name = "gridControlWellHdr";
            this.gridControlWellHdr.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1});
            this.gridControlWellHdr.Size = new System.Drawing.Size(927, 529);
            this.gridControlWellHdr.TabIndex = 25;
            this.gridControlWellHdr.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWellHdr});
            // 
            // gridViewWellHdr
            // 
            this.gridViewWellHdr.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridViewWellHdr.Appearance.Row.Options.UseFont = true;
            this.gridViewWellHdr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.uwi,
            this.well_name,
            this.well_number,
            this.well_number_sort,
            this.platform,
            this.field});
            this.gridViewWellHdr.GridControl = this.gridControlWellHdr;
            this.gridViewWellHdr.Name = "gridViewWellHdr";
            this.gridViewWellHdr.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWellHdr.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWellHdr.OptionsEditForm.FormCaptionFormat = "Cập nhật thông tin giếng";
            this.gridViewWellHdr.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewWellHdr.OptionsView.ShowAutoFilterRow = true;
            this.gridViewWellHdr.OptionsView.ShowGroupPanel = false;
            this.gridViewWellHdr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewWellHdr_KeyDown);
            // 
            // uwi
            // 
            this.uwi.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.uwi.AppearanceCell.Options.UseFont = true;
            this.uwi.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.uwi.AppearanceHeader.Options.UseFont = true;
            this.uwi.AppearanceHeader.Options.UseTextOptions = true;
            this.uwi.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.uwi.Caption = "UWI";
            this.uwi.FieldName = "UWI";
            this.uwi.Name = "uwi";
            this.uwi.Visible = true;
            this.uwi.VisibleIndex = 0;
            this.uwi.Width = 147;
            // 
            // well_name
            // 
            this.well_name.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.well_name.AppearanceCell.Options.UseFont = true;
            this.well_name.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.well_name.AppearanceHeader.Options.UseFont = true;
            this.well_name.AppearanceHeader.Options.UseTextOptions = true;
            this.well_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.well_name.Caption = "WELL NAME";
            this.well_name.FieldName = "WELL_NAME";
            this.well_name.Name = "well_name";
            this.well_name.Visible = true;
            this.well_name.VisibleIndex = 1;
            this.well_name.Width = 184;
            // 
            // well_number
            // 
            this.well_number.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.well_number.AppearanceCell.Options.UseFont = true;
            this.well_number.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.well_number.AppearanceHeader.Options.UseFont = true;
            this.well_number.AppearanceHeader.Options.UseTextOptions = true;
            this.well_number.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.well_number.Caption = "WELL NUMBER";
            this.well_number.FieldName = "WELL_NUMBER";
            this.well_number.Name = "well_number";
            this.well_number.Visible = true;
            this.well_number.VisibleIndex = 2;
            this.well_number.Width = 110;
            // 
            // well_number_sort
            // 
            this.well_number_sort.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.well_number_sort.AppearanceCell.Options.UseFont = true;
            this.well_number_sort.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.well_number_sort.AppearanceHeader.Options.UseFont = true;
            this.well_number_sort.AppearanceHeader.Options.UseTextOptions = true;
            this.well_number_sort.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.well_number_sort.Caption = "WELL NUMBER SORT";
            this.well_number_sort.FieldName = "WELL_NUMBER_SORT";
            this.well_number_sort.Name = "well_number_sort";
            this.well_number_sort.Visible = true;
            this.well_number_sort.VisibleIndex = 3;
            this.well_number_sort.Width = 110;
            // 
            // platform
            // 
            this.platform.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.platform.AppearanceCell.Options.UseFont = true;
            this.platform.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.platform.AppearanceHeader.Options.UseFont = true;
            this.platform.AppearanceHeader.Options.UseTextOptions = true;
            this.platform.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.platform.Caption = "PLATFORM";
            this.platform.FieldName = "PLATFORM";
            this.platform.Name = "platform";
            this.platform.Visible = true;
            this.platform.VisibleIndex = 4;
            this.platform.Width = 109;
            // 
            // field
            // 
            this.field.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.field.AppearanceCell.Options.UseFont = true;
            this.field.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.field.AppearanceHeader.Options.UseFont = true;
            this.field.AppearanceHeader.Options.UseTextOptions = true;
            this.field.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.field.Caption = "FIELD";
            this.field.FieldName = "FIELD_ID";
            this.field.Name = "field";
            this.field.Visible = true;
            this.field.VisibleIndex = 5;
            this.field.Width = 112;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 561);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(941, 44);
            this.panel1.TabIndex = 26;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(773, 9);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 6;
            this.btnRefresh.Text = "Nạp lại";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(854, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(692, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(941, 561);
            this.tabControl1.TabIndex = 27;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gridControlWellHdr);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(933, 535);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin chung";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // frmWells_Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 605);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmWells_Test";
            this.Text = "Thông tin giếng khoan";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmWells_Test_FormClosed);
            this.Load += new System.EventHandler(this.frmWells_Test_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWellHdr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWellHdr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlWellHdr;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWellHdr;
        private DevExpress.XtraGrid.Columns.GridColumn uwi;
        private DevExpress.XtraGrid.Columns.GridColumn well_name;
        private DevExpress.XtraGrid.Columns.GridColumn well_number;
        private DevExpress.XtraGrid.Columns.GridColumn well_number_sort;
        private DevExpress.XtraGrid.Columns.GridColumn platform;
        private DevExpress.XtraGrid.Columns.GridColumn field;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnSave;
    }
}