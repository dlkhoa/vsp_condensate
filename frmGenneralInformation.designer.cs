﻿namespace TrueTech.VSP.VSPCondensate
{
    partial class frmGenneralInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGenneralInformation));
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabMethod = new System.Windows.Forms.TabPage();
            this.gridControlMethod = new DevExpress.XtraGrid.GridControl();
            this.gridViewMethod = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox7 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabZone = new System.Windows.Forms.TabPage();
            this.gridControlZone = new DevExpress.XtraGrid.GridControl();
            this.gridViewZone = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabDome = new System.Windows.Forms.TabPage();
            this.gridControlDome = new DevExpress.XtraGrid.GridControl();
            this.gridViewDome = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabReservoir = new System.Windows.Forms.TabPage();
            this.gridControlReservoir = new DevExpress.XtraGrid.GridControl();
            this.gridViewReservoir = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabPlatform = new System.Windows.Forms.TabPage();
            this.gridControlPlatform = new DevExpress.XtraGrid.GridControl();
            this.gridViewPlatform = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.platform = new DevExpress.XtraGrid.Columns.GridColumn();
            this.field_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabField = new System.Windows.Forms.TabPage();
            this.gridControlField = new DevExpress.XtraGrid.GridControl();
            this.gridViewField = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.fieldID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.field_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.field_name_r = new DevExpress.XtraGrid.Columns.GridColumn();
            this.field_name_v = new DevExpress.XtraGrid.Columns.GridColumn();
            this.field_name_r1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.data_source = new DevExpress.XtraGrid.Columns.GridColumn();
            this.vsp_percent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.field_order_num = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.tabMethod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.tabZone.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.tabDome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.tabReservoir.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReservoir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReservoir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            this.tabPlatform.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlatform)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlatform)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            this.tabField.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Location = new System.Drawing.Point(576, 466);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Lưu";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(738, 466);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(657, 466);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Nạp lại";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // tabMethod
            // 
            this.tabMethod.Controls.Add(this.gridControlMethod);
            this.tabMethod.Location = new System.Drawing.Point(4, 22);
            this.tabMethod.Name = "tabMethod";
            this.tabMethod.Padding = new System.Windows.Forms.Padding(3);
            this.tabMethod.Size = new System.Drawing.Size(796, 412);
            this.tabMethod.TabIndex = 4;
            this.tabMethod.Text = "Phương pháp";
            this.tabMethod.UseVisualStyleBackColor = true;
            // 
            // gridControlMethod
            // 
            this.gridControlMethod.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlMethod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMethod.Location = new System.Drawing.Point(3, 3);
            this.gridControlMethod.MainView = this.gridViewMethod;
            this.gridControlMethod.Name = "gridControlMethod";
            this.gridControlMethod.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox7});
            this.gridControlMethod.Size = new System.Drawing.Size(790, 406);
            this.gridControlMethod.TabIndex = 31;
            this.gridControlMethod.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMethod,
            this.gridView2});
            // 
            // gridViewMethod
            // 
            this.gridViewMethod.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridViewMethod.Appearance.Row.Options.UseFont = true;
            this.gridViewMethod.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33});
            this.gridViewMethod.GridControl = this.gridControlMethod;
            this.gridViewMethod.Name = "gridViewMethod";
            this.gridViewMethod.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewMethod.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewMethod.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewMethod.OptionsEditForm.FormCaptionFormat = "Cập nhật thông tin giếng";
            this.gridViewMethod.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewMethod.OptionsView.ShowAutoFilterRow = true;
            this.gridViewMethod.OptionsView.ShowGroupPanel = false;
            this.gridViewMethod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewMethod_KeyDown);
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn26.AppearanceCell.Options.UseFont = true;
            this.gridColumn26.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn26.AppearanceHeader.Options.UseFont = true;
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.Caption = "Mã phương pháp";
            this.gridColumn26.FieldName = "METHOD_ID";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 0;
            this.gridColumn26.Width = 87;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.Caption = "Tên ngắn";
            this.gridColumn27.FieldName = "METHOD_SHORT_NAME";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 1;
            this.gridColumn27.Width = 78;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn28.AppearanceCell.Options.UseFont = true;
            this.gridColumn28.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn28.AppearanceHeader.Options.UseFont = true;
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.Caption = "Tên phương pháp";
            this.gridColumn28.FieldName = "METHOD_NAME";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 2;
            this.gridColumn28.Width = 90;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn29.AppearanceCell.Options.UseFont = true;
            this.gridColumn29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn29.AppearanceHeader.Options.UseFont = true;
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.Caption = "Tên phương pháp (Việt)";
            this.gridColumn29.FieldName = "METHOD_NAME_V";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 3;
            this.gridColumn29.Width = 110;
            // 
            // gridColumn30
            // 
            this.gridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn30.Caption = "Tên phương pháp (Rus)";
            this.gridColumn30.FieldName = "METHOD_NAME_R";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 4;
            this.gridColumn30.Width = 131;
            // 
            // gridColumn31
            // 
            this.gridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn31.Caption = "Tên phương pháp (Rus 1)";
            this.gridColumn31.FieldName = "METHOD_NAME_R_1";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 5;
            this.gridColumn31.Width = 135;
            // 
            // gridColumn32
            // 
            this.gridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.Caption = "Loại";
            this.gridColumn32.FieldName = "METHOD_TYPE";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 6;
            this.gridColumn32.Width = 86;
            // 
            // gridColumn33
            // 
            this.gridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.Caption = "Thứ tự";
            this.gridColumn33.FieldName = "ORDER_NUM";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 7;
            this.gridColumn33.Width = 55;
            // 
            // repositoryItemComboBox7
            // 
            this.repositoryItemComboBox7.AutoHeight = false;
            this.repositoryItemComboBox7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox7.Name = "repositoryItemComboBox7";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControlMethod;
            this.gridView2.Name = "gridView2";
            // 
            // tabZone
            // 
            this.tabZone.Controls.Add(this.gridControlZone);
            this.tabZone.Location = new System.Drawing.Point(4, 22);
            this.tabZone.Name = "tabZone";
            this.tabZone.Padding = new System.Windows.Forms.Padding(3);
            this.tabZone.Size = new System.Drawing.Size(796, 412);
            this.tabZone.TabIndex = 3;
            this.tabZone.Text = "Vùng";
            this.tabZone.UseVisualStyleBackColor = true;
            // 
            // gridControlZone
            // 
            this.gridControlZone.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlZone.Location = new System.Drawing.Point(3, 3);
            this.gridControlZone.MainView = this.gridViewZone;
            this.gridControlZone.Name = "gridControlZone";
            this.gridControlZone.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox6});
            this.gridControlZone.Size = new System.Drawing.Size(790, 406);
            this.gridControlZone.TabIndex = 31;
            this.gridControlZone.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewZone,
            this.gridView3});
            // 
            // gridViewZone
            // 
            this.gridViewZone.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridViewZone.Appearance.Row.Options.UseFont = true;
            this.gridViewZone.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn18,
            this.gridColumn23,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn22,
            this.gridColumn24,
            this.gridColumn25});
            this.gridViewZone.GridControl = this.gridControlZone;
            this.gridViewZone.Name = "gridViewZone";
            this.gridViewZone.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewZone.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewZone.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewZone.OptionsEditForm.FormCaptionFormat = "Cập nhật thông tin giếng";
            this.gridViewZone.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewZone.OptionsView.ShowAutoFilterRow = true;
            this.gridViewZone.OptionsView.ShowGroupPanel = false;
            this.gridViewZone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewZone_KeyDown);
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn18.AppearanceCell.Options.UseFont = true;
            this.gridColumn18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn18.AppearanceHeader.Options.UseFont = true;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = "Mã vùng";
            this.gridColumn18.FieldName = "ZONE_ID";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 0;
            this.gridColumn18.Width = 99;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.Caption = "Tên ngắn";
            this.gridColumn23.FieldName = "ZONE_SHORT_NAME";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            this.gridColumn23.Width = 103;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "Tên vùng";
            this.gridColumn19.FieldName = "ZONE_NAME";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 2;
            this.gridColumn19.Width = 105;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn20.AppearanceCell.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn20.AppearanceHeader.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "Tên vùng (Việt)";
            this.gridColumn20.FieldName = "ZONE_NAME_V";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 3;
            this.gridColumn20.Width = 115;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "Tên vùng (Rus)";
            this.gridColumn22.FieldName = "ZONE_NAME_R";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 4;
            this.gridColumn22.Width = 142;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.Caption = "Tên vùng (Rus 1)";
            this.gridColumn24.FieldName = "ZONE_NAME_R_1";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 5;
            this.gridColumn24.Width = 125;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.Caption = "Thứ tự";
            this.gridColumn25.FieldName = "ORDER_NUM";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 6;
            this.gridColumn25.Width = 83;
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridControlZone;
            this.gridView3.Name = "gridView3";
            // 
            // tabDome
            // 
            this.tabDome.Controls.Add(this.gridControlDome);
            this.tabDome.Location = new System.Drawing.Point(4, 22);
            this.tabDome.Name = "tabDome";
            this.tabDome.Padding = new System.Windows.Forms.Padding(3);
            this.tabDome.Size = new System.Drawing.Size(796, 412);
            this.tabDome.TabIndex = 2;
            this.tabDome.Text = "Vòm";
            this.tabDome.UseVisualStyleBackColor = true;
            // 
            // gridControlDome
            // 
            this.gridControlDome.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlDome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDome.Location = new System.Drawing.Point(3, 3);
            this.gridControlDome.MainView = this.gridViewDome;
            this.gridControlDome.Name = "gridControlDome";
            this.gridControlDome.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox5});
            this.gridControlDome.Size = new System.Drawing.Size(790, 406);
            this.gridControlDome.TabIndex = 30;
            this.gridControlDome.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDome,
            this.gridView4});
            // 
            // gridViewDome
            // 
            this.gridViewDome.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridViewDome.Appearance.Row.Options.UseFont = true;
            this.gridViewDome.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridViewDome.GridControl = this.gridControlDome;
            this.gridViewDome.Name = "gridViewDome";
            this.gridViewDome.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewDome.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewDome.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewDome.OptionsEditForm.FormCaptionFormat = "Cập nhật thông tin giếng";
            this.gridViewDome.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewDome.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDome.OptionsView.ShowGroupPanel = false;
            this.gridViewDome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewDome_KeyDown);
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn10.AppearanceCell.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "Mã vòm";
            this.gridColumn10.FieldName = "DOME_ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 46;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Tên ngắn";
            this.gridColumn11.FieldName = "DOME_NAME_ID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 62;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn12.AppearanceCell.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn12.AppearanceHeader.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "Tên vòm";
            this.gridColumn12.FieldName = "DOME_NAME";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 134;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn13.AppearanceCell.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "Tên vòm (Việt)";
            this.gridColumn13.FieldName = "DOME_NAME_V";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 3;
            this.gridColumn13.Width = 151;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "Tên vòm (Rus)";
            this.gridColumn14.FieldName = "DOME_NAME_R";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 4;
            this.gridColumn14.Width = 151;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "Tên vòm (Rus 1)";
            this.gridColumn15.FieldName = "DOME_NAME_R_1";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 5;
            this.gridColumn15.Width = 73;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "Tên vòm (Rus 2)";
            this.gridColumn16.FieldName = "DOME_NAME_R_2";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 6;
            this.gridColumn16.Width = 109;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "Thứ tự";
            this.gridColumn17.FieldName = "ORDER_NUM";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 7;
            this.gridColumn17.Width = 46;
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.gridControlDome;
            this.gridView4.Name = "gridView4";
            // 
            // tabReservoir
            // 
            this.tabReservoir.Controls.Add(this.gridControlReservoir);
            this.tabReservoir.Location = new System.Drawing.Point(4, 22);
            this.tabReservoir.Name = "tabReservoir";
            this.tabReservoir.Padding = new System.Windows.Forms.Padding(3);
            this.tabReservoir.Size = new System.Drawing.Size(796, 412);
            this.tabReservoir.TabIndex = 1;
            this.tabReservoir.Text = "Tầng";
            this.tabReservoir.UseVisualStyleBackColor = true;
            // 
            // gridControlReservoir
            // 
            this.gridControlReservoir.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlReservoir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlReservoir.Location = new System.Drawing.Point(3, 3);
            this.gridControlReservoir.MainView = this.gridViewReservoir;
            this.gridControlReservoir.Name = "gridControlReservoir";
            this.gridControlReservoir.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox4});
            this.gridControlReservoir.Size = new System.Drawing.Size(790, 406);
            this.gridControlReservoir.TabIndex = 29;
            this.gridControlReservoir.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewReservoir,
            this.gridView5});
            // 
            // gridViewReservoir
            // 
            this.gridViewReservoir.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridViewReservoir.Appearance.Row.Options.UseFont = true;
            this.gridViewReservoir.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn6,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn7,
            this.gridColumn5,
            this.gridColumn9});
            this.gridViewReservoir.GridControl = this.gridControlReservoir;
            this.gridViewReservoir.Name = "gridViewReservoir";
            this.gridViewReservoir.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewReservoir.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewReservoir.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewReservoir.OptionsEditForm.FormCaptionFormat = "Cập nhật thông tin giếng";
            this.gridViewReservoir.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewReservoir.OptionsView.ShowAutoFilterRow = true;
            this.gridViewReservoir.OptionsView.ShowGroupPanel = false;
            this.gridViewReservoir.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewReservoir_KeyDown);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Mã tầng";
            this.gridColumn1.FieldName = "RESERVOIR_ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 103;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "Tên ngắn";
            this.gridColumn6.FieldName = "RESERVOIR_SHORT_NAME";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "Tên tầng";
            this.gridColumn2.FieldName = "RESERVOIR_NAME";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 103;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Tên tầng (Rus)";
            this.gridColumn3.FieldName = "RESERVOIR_NAME_R";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 103;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Tên tầng (Việt)";
            this.gridColumn4.FieldName = "RESERVOIR_NAME_V";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            this.gridColumn4.Width = 150;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "Tên tầng (Rus 1)";
            this.gridColumn7.FieldName = "RESERVOIR_NAME_R_1";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Loại";
            this.gridColumn5.FieldName = "RESERVOIR_TYPE";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.Caption = "Thứ tự";
            this.gridColumn9.FieldName = "ORDER_NUM";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // gridView5
            // 
            this.gridView5.GridControl = this.gridControlReservoir;
            this.gridView5.Name = "gridView5";
            // 
            // tabPlatform
            // 
            this.tabPlatform.Controls.Add(this.gridControlPlatform);
            this.tabPlatform.Location = new System.Drawing.Point(4, 22);
            this.tabPlatform.Name = "tabPlatform";
            this.tabPlatform.Padding = new System.Windows.Forms.Padding(3);
            this.tabPlatform.Size = new System.Drawing.Size(796, 412);
            this.tabPlatform.TabIndex = 6;
            this.tabPlatform.Text = "Giàn";
            this.tabPlatform.UseVisualStyleBackColor = true;
            // 
            // gridControlPlatform
            // 
            this.gridControlPlatform.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlPlatform.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlatform.Location = new System.Drawing.Point(3, 3);
            this.gridControlPlatform.MainView = this.gridViewPlatform;
            this.gridControlPlatform.Name = "gridControlPlatform";
            this.gridControlPlatform.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox3});
            this.gridControlPlatform.Size = new System.Drawing.Size(790, 406);
            this.gridControlPlatform.TabIndex = 28;
            this.gridControlPlatform.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPlatform,
            this.gridView6});
            // 
            // gridViewPlatform
            // 
            this.gridViewPlatform.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridViewPlatform.Appearance.Row.Options.UseFont = true;
            this.gridViewPlatform.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.platform,
            this.field_ID,
            this.gridColumn8});
            this.gridViewPlatform.GridControl = this.gridControlPlatform;
            this.gridViewPlatform.Name = "gridViewPlatform";
            this.gridViewPlatform.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewPlatform.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewPlatform.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewPlatform.OptionsEditForm.FormCaptionFormat = "Cập nhật thông tin giếng";
            this.gridViewPlatform.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewPlatform.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPlatform.OptionsView.ShowGroupPanel = false;
            this.gridViewPlatform.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewPlatform_KeyDown);
            // 
            // platform
            // 
            this.platform.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.platform.AppearanceCell.Options.UseFont = true;
            this.platform.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.platform.AppearanceHeader.Options.UseFont = true;
            this.platform.AppearanceHeader.Options.UseTextOptions = true;
            this.platform.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.platform.Caption = "Mã giàn";
            this.platform.FieldName = "PLATFORM";
            this.platform.Name = "platform";
            this.platform.Visible = true;
            this.platform.VisibleIndex = 0;
            this.platform.Width = 103;
            // 
            // field_ID
            // 
            this.field_ID.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.field_ID.AppearanceCell.Options.UseFont = true;
            this.field_ID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.field_ID.AppearanceHeader.Options.UseFont = true;
            this.field_ID.AppearanceHeader.Options.UseTextOptions = true;
            this.field_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.field_ID.Caption = "Mã mỏ";
            this.field_ID.FieldName = "FIELD_ID";
            this.field_ID.Name = "field_ID";
            this.field_ID.Visible = true;
            this.field_ID.VisibleIndex = 1;
            this.field_ID.Width = 103;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "Thứ tự";
            this.gridColumn8.FieldName = "ORDER_NUM";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            this.gridColumn8.Width = 150;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // gridView6
            // 
            this.gridView6.GridControl = this.gridControlPlatform;
            this.gridView6.Name = "gridView6";
            // 
            // tabField
            // 
            this.tabField.Controls.Add(this.gridControlField);
            this.tabField.Location = new System.Drawing.Point(4, 22);
            this.tabField.Name = "tabField";
            this.tabField.Padding = new System.Windows.Forms.Padding(3);
            this.tabField.Size = new System.Drawing.Size(796, 412);
            this.tabField.TabIndex = 0;
            this.tabField.Text = "Mỏ";
            this.tabField.UseVisualStyleBackColor = true;
            // 
            // gridControlField
            // 
            this.gridControlField.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlField.Location = new System.Drawing.Point(3, 3);
            this.gridControlField.MainView = this.gridViewField;
            this.gridControlField.Name = "gridControlField";
            this.gridControlField.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox2});
            this.gridControlField.Size = new System.Drawing.Size(790, 406);
            this.gridControlField.TabIndex = 27;
            this.gridControlField.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewField,
            this.gridView7});
            // 
            // gridViewField
            // 
            this.gridViewField.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gridViewField.Appearance.Row.Options.UseFont = true;
            this.gridViewField.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.fieldID,
            this.field_name,
            this.field_name_r,
            this.field_name_v,
            this.field_name_r1,
            this.data_source,
            this.vsp_percent,
            this.field_order_num});
            this.gridViewField.GridControl = this.gridControlField;
            this.gridViewField.Name = "gridViewField";
            this.gridViewField.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewField.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewField.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewField.OptionsEditForm.FormCaptionFormat = "Cập nhật thông tin giếng";
            this.gridViewField.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewField.OptionsView.ShowAutoFilterRow = true;
            this.gridViewField.OptionsView.ShowGroupPanel = false;
            this.gridViewField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewField_KeyDown);
            // 
            // fieldID
            // 
            this.fieldID.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.fieldID.AppearanceCell.Options.UseFont = true;
            this.fieldID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.fieldID.AppearanceHeader.Options.UseFont = true;
            this.fieldID.AppearanceHeader.Options.UseTextOptions = true;
            this.fieldID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fieldID.Caption = "Mã mỏ";
            this.fieldID.FieldName = "FIELD_ID";
            this.fieldID.Name = "fieldID";
            this.fieldID.Visible = true;
            this.fieldID.VisibleIndex = 0;
            this.fieldID.Width = 103;
            // 
            // field_name
            // 
            this.field_name.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.field_name.AppearanceCell.Options.UseFont = true;
            this.field_name.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.field_name.AppearanceHeader.Options.UseFont = true;
            this.field_name.AppearanceHeader.Options.UseTextOptions = true;
            this.field_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.field_name.Caption = "Tên mỏ";
            this.field_name.FieldName = "FIELD_NAME";
            this.field_name.Name = "field_name";
            this.field_name.Visible = true;
            this.field_name.VisibleIndex = 1;
            this.field_name.Width = 103;
            // 
            // field_name_r
            // 
            this.field_name_r.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.field_name_r.AppearanceCell.Options.UseFont = true;
            this.field_name_r.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.field_name_r.AppearanceHeader.Options.UseFont = true;
            this.field_name_r.AppearanceHeader.Options.UseTextOptions = true;
            this.field_name_r.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.field_name_r.Caption = "Tên mỏ (Rus)";
            this.field_name_r.FieldName = "FIELD_NAME_R";
            this.field_name_r.Name = "field_name_r";
            this.field_name_r.Visible = true;
            this.field_name_r.VisibleIndex = 2;
            this.field_name_r.Width = 103;
            // 
            // field_name_v
            // 
            this.field_name_v.AppearanceHeader.Options.UseTextOptions = true;
            this.field_name_v.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.field_name_v.Caption = "Tên mỏ (Việt)";
            this.field_name_v.FieldName = "FIELD_NAME_V";
            this.field_name_v.Name = "field_name_v";
            this.field_name_v.Visible = true;
            this.field_name_v.VisibleIndex = 3;
            this.field_name_v.Width = 103;
            // 
            // field_name_r1
            // 
            this.field_name_r1.AppearanceHeader.Options.UseTextOptions = true;
            this.field_name_r1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.field_name_r1.Caption = "Tên mỏ (Rus 1)";
            this.field_name_r1.FieldName = "FIELD_NAME_R_1";
            this.field_name_r1.Name = "field_name_r1";
            this.field_name_r1.Visible = true;
            this.field_name_r1.VisibleIndex = 4;
            this.field_name_r1.Width = 103;
            // 
            // data_source
            // 
            this.data_source.AppearanceHeader.Options.UseTextOptions = true;
            this.data_source.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.data_source.Caption = "Nguồn dữ liệu";
            this.data_source.FieldName = "DATA_SOURCE";
            this.data_source.Name = "data_source";
            this.data_source.Visible = true;
            this.data_source.VisibleIndex = 5;
            this.data_source.Width = 108;
            // 
            // vsp_percent
            // 
            this.vsp_percent.AppearanceHeader.Options.UseTextOptions = true;
            this.vsp_percent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.vsp_percent.Caption = "%VSP";
            this.vsp_percent.FieldName = "VSP_PERCENT";
            this.vsp_percent.Name = "vsp_percent";
            this.vsp_percent.Visible = true;
            this.vsp_percent.VisibleIndex = 6;
            this.vsp_percent.Width = 128;
            // 
            // field_order_num
            // 
            this.field_order_num.AppearanceHeader.Options.UseTextOptions = true;
            this.field_order_num.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.field_order_num.Caption = "Thứ tự";
            this.field_order_num.FieldName = "ORDER_NUM";
            this.field_order_num.Name = "field_order_num";
            this.field_order_num.Visible = true;
            this.field_order_num.VisibleIndex = 7;
            this.field_order_num.Width = 150;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // gridView7
            // 
            this.gridView7.GridControl = this.gridControlField;
            this.gridView7.Name = "gridView7";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabField);
            this.tabControl1.Controls.Add(this.tabPlatform);
            this.tabControl1.Controls.Add(this.tabReservoir);
            this.tabControl1.Controls.Add(this.tabDome);
            this.tabControl1.Controls.Add(this.tabZone);
            this.tabControl1.Controls.Add(this.tabMethod);
            this.tabControl1.Location = new System.Drawing.Point(13, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(804, 438);
            this.tabControl1.TabIndex = 1;
            // 
            // frmGenneralInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 501);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmGenneralInformation";
            this.Text = "Thông tin chung";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmGenneralInformation_FormClosed);
            this.Load += new System.EventHandler(this.frmGenneralInformation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.tabMethod.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.tabZone.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.tabDome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.tabReservoir.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReservoir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReservoir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            this.tabPlatform.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlatform)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlatform)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            this.tabField.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnUpdate;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.TabPage tabMethod;
        private DevExpress.XtraGrid.GridControl gridControlMethod;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMethod;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.TabPage tabZone;
        private DevExpress.XtraGrid.GridControl gridControlZone;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewZone;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.TabPage tabDome;
        private DevExpress.XtraGrid.GridControl gridControlDome;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDome;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.TabPage tabReservoir;
        private DevExpress.XtraGrid.GridControl gridControlReservoir;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewReservoir;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private System.Windows.Forms.TabPage tabPlatform;
        private DevExpress.XtraGrid.GridControl gridControlPlatform;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPlatform;
        private DevExpress.XtraGrid.Columns.GridColumn platform;
        private DevExpress.XtraGrid.Columns.GridColumn field_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private System.Windows.Forms.TabPage tabField;
        private DevExpress.XtraGrid.GridControl gridControlField;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewField;
        private DevExpress.XtraGrid.Columns.GridColumn fieldID;
        private DevExpress.XtraGrid.Columns.GridColumn field_name;
        private DevExpress.XtraGrid.Columns.GridColumn field_name_r;
        private DevExpress.XtraGrid.Columns.GridColumn field_name_v;
        private DevExpress.XtraGrid.Columns.GridColumn field_name_r1;
        private DevExpress.XtraGrid.Columns.GridColumn data_source;
        private DevExpress.XtraGrid.Columns.GridColumn vsp_percent;
        private DevExpress.XtraGrid.Columns.GridColumn field_order_num;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private System.Windows.Forms.TabControl tabControl1;

    }
}