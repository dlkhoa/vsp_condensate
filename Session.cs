﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrueTech.VSP.VSPCondensate
{
    class Session
    {
        public static frmUserManager frmUserManager = null;
        public static frmGenneralInformation frmGenneralInformation = null;
        public static frmWells_Test frmWells = null;
        public static frmImportMonthlyCondensate frmImportMonthlyCondensate = null;
        public static frmWellData frmWellData = null;
    }
}
