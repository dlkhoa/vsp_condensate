using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
namespace TrueTech.VSP.VSPCondensate
{
    public class clsDatabases
    {

        private string strError = "";
        public string Error
        {
            get { return strError; }
        }

        #region Define SqlParameter
        private void DefineSqlParameter(SqlCommand sqlCommand,
            string[] Parameters, string[] Values)
        {
            SqlParameter sqlParameter;
            for (int i = 0; i < Parameters.Length; i++)
            {
                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = Parameters[i];
                sqlParameter.SqlValue = Values[i];
                sqlCommand.Parameters.Add(sqlParameter);
            }
        }
        private void DefineSqlParameter(SqlCommand sqlCommand, 
            string[] Parameters, object[] Values)
        {
            SqlParameter sqlParameter;
            for (int i = 0; i < Parameters.Length; i++)
            {
                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = Parameters[i];
                sqlParameter.SqlValue = Values[i];
                sqlCommand.Parameters.Add(sqlParameter);
            }
        }

        private void DefineSqlParameter(SqlCommand sqlCommand,
            string[] Parameters, object[] Values, SqlDbType[] sqlDbType)
        {
            SqlParameter sqlParameter;
            for (int i = 0; i < Parameters.Length; i++)
            {
                sqlParameter = new SqlParameter();
                sqlParameter.ParameterName = Parameters[i];
                sqlParameter.SqlValue = Values[i];
                sqlParameter.SqlDbType = sqlDbType[i];
                sqlCommand.Parameters.Add(sqlParameter);
            }
        }

        private void DefineSqlParameter(SqlCommand sqlCommand,
            string  Parameter, object Value)
        {
            SqlParameter sqlParameter= new SqlParameter();
            sqlParameter.ParameterName = Parameter ;
            sqlParameter.SqlValue = Value ;
            sqlCommand.Parameters.Add(sqlParameter);             
        }
        #endregion

        #region ExecuteNonQuery
        public int ExecuteNonQuery(string strQuery,
        CommandType commandType, string  Parameter ,
         string  Value )
        {
            int efftectRecord = 0;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;

            DefineSqlParameter(sqlCommand, Parameter, Value);
             
            try
            {
                efftectRecord = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return efftectRecord;
        }

        public int ExecuteNonQuery(string strQuery,
        CommandType commandType, string[] Parameters,
         object[] Values)
        {
            int efftectRecord = 0;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            DefineSqlParameter(sqlCommand, Parameters, Values);
            try
            {
                efftectRecord = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return efftectRecord;
        }

        public int ExecuteNonQuery(string strQuery,
        CommandType commandType, string[] Parameters,
         object[] Values, SqlDbType[] sqlDbType, ref string strError)
        {
            int efftectRecord = 0;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            DefineSqlParameter(sqlCommand, Parameters, Values, sqlDbType);
            try
            {
                efftectRecord = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                strError = ex.Message;
            }

            return efftectRecord;
        }

        public int ExecuteNonQuery(string strQuery,
            CommandType commandType, string[] Parameters,
             object[] Values, SqlDbType[] sqlDbType)
        {
            int efftectRecord = 0;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            DefineSqlParameter(sqlCommand, Parameters, Values, sqlDbType);
            try
            {
                efftectRecord = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                strError = "\r\n" + ex.Message;
            }

            return efftectRecord;
        }


        public int ExecuteNonQuery(string strQuery,
        CommandType commandType, string[] Parameters,
         string[] Values)
        {
            int efftectRecord = 0;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            DefineSqlParameter(sqlCommand, Parameters, Values);
            try
            {
                efftectRecord = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return efftectRecord;
        }

        public int ExecuteNonQuery(string strQuery, CommandType commandType, ref string msgReport)
        {
            int efftectRecord = 0;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            try
            {
                efftectRecord = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                msgReport += "\r\nError: " + ex.Message;
            }

            return efftectRecord;
        }

        public int ExecuteNonQuery(string strQuery, CommandType commandType)
        {
            int efftectRecord = 0;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            try
            {
                efftectRecord = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                strError += "\r\nError: " + ex.Message;
            }

            return efftectRecord;
        }

        public int ExecuteNonQuery(string strQuery,
        CommandType commandType, string[] Parameters,
         object[] Values, int ReturnParameter)
        {
            int efftectRecord = 0;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            SqlParameter sqlParameter;
            for (int i = 0; i < Parameters.Length; i++)
            {

                if (i == ReturnParameter)
                {
                    sqlParameter = new SqlParameter(Parameters[i], SqlDbType.Int);
                    sqlParameter.Direction =
                    ParameterDirection.ReturnValue;
                }
                else
                {
                    sqlParameter = new SqlParameter();
                    sqlParameter.ParameterName = Parameters[i];
                    sqlParameter.SqlValue = Values[i];
                }
                sqlCommand.Parameters.Add(sqlParameter);
            }
            try
            {
                sqlCommand.ExecuteNonQuery();
                efftectRecord = (int)sqlCommand.Parameters[Parameters[ReturnParameter]].Value;
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return efftectRecord;
        }
        #endregion

        #region ExecuteScalar
        public object ExecuteScalar(string strQuery, CommandType commandType)
        {
            object objValue = null;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            try
            {
                objValue = sqlCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return objValue;
        }

        public object ExecuteScalar(string strQuery,
         CommandType commandType, string[] Parameters,
         object[] Values)
        {
            object objValue = null;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            DefineSqlParameter(sqlCommand, Parameters, Values);
            try
            {
                objValue = sqlCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return objValue;
        }
        public object ExecuteScalar(string strQuery,
         CommandType commandType, string[] Parameters,
         object[] Values, SqlDbType[] sqlDbType)
        {
            object objValue = null;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = strQuery;
            sqlCommand.Connection = clsConnection.sqlConnection;
            sqlCommand.CommandType = commandType;
            DefineSqlParameter(sqlCommand, Parameters, Values, sqlDbType);
            try
            {
                objValue = sqlCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return objValue;
        }
        #endregion

        #region ExecuteReader and ArrayList

        public ArrayList GetArrayLists(string strSQL,
            CommandType commandType, string[] Parameters,
         string[] Values, int[] NextResult)
        {

            ArrayList arrayList = new ArrayList();
            try
            {
                SqlCommand sqlCommand = new SqlCommand();

                sqlCommand.CommandText = strSQL.ToString();
                sqlCommand.Connection = clsConnection.sqlConnection;
                sqlCommand.CommandType = commandType;
                int i = 0;
                DefineSqlParameter(sqlCommand, Parameters, Values );
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                i = 0; NextResult[0] = 1;
                object[] info;
                while (sqlDataReader.Read())
                {
                    info = new object[sqlDataReader.FieldCount];
                    sqlDataReader.GetValues(info);
                    arrayList.Add(info);
                    i++;
                }
                while (sqlDataReader.NextResult())
                {
                    NextResult[0] = i;
                    while (sqlDataReader.Read())
                    {
                        info = new object[sqlDataReader.FieldCount];
                        sqlDataReader.GetValues(info);
                        arrayList.Add(info);
                        i++;
                    }
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return arrayList;
        }

        public ArrayList GetArrayLists(string[] strSQL,
            CommandType commandType,int[] NextResult)
        {

            ArrayList arrayList = new ArrayList();
            try
            {
                int i = 0;
                SqlCommand sqlCommand = new SqlCommand();
                for(i=0;i<strSQL.Length;i++)
                    sqlCommand.CommandText += strSQL[i]+";";
                sqlCommand.Connection = clsConnection.sqlConnection;
                sqlCommand.CommandType = commandType;
               
                
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                i = 0; NextResult[0] = 1;
                object[] info;
                while (sqlDataReader.Read())
                {
                    info = new object[sqlDataReader.FieldCount];
                    sqlDataReader.GetValues(info);
                    arrayList.Add(info);
                    i++;
                }
                while (sqlDataReader.NextResult())
                {
                    NextResult[0] = i;
                    while (sqlDataReader.Read())
                    {
                        info = new object[sqlDataReader.FieldCount];
                        sqlDataReader.GetValues(info);
                        arrayList.Add(info);
                        i++;
                    }
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return arrayList;
        }

        public ArrayList GetArrayLists(string strSQL,
            CommandType commandType, string[] Parameters,
         string[] Values)
        {
            ArrayList arrayList = new ArrayList();
            try
            {
                SqlCommand sqlCommand = new SqlCommand(
                    strSQL, clsConnection.sqlConnection);
                sqlCommand.CommandType = commandType;

                DefineSqlParameter(sqlCommand, Parameters, Values );
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    object[] info = new object[sqlDataReader.FieldCount];
                    sqlDataReader.GetValues(info);
                    arrayList.Add(info);
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return arrayList;
        }

        public ArrayList GetArrayLists(string strSQL,
            CommandType commandType, string[] Parameters,
         string[] Values, SqlDbType[] sqlDbType)
        {
            ArrayList arrayList = new ArrayList();
            try
            {
                SqlCommand sqlCommand = new SqlCommand(
                    strSQL, clsConnection.sqlConnection);
                sqlCommand.CommandType = commandType;

                DefineSqlParameter(sqlCommand, Parameters, Values, sqlDbType);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    object[] info = new object[sqlDataReader.FieldCount];
                    sqlDataReader.GetValues(info);
                    arrayList.Add(info);
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return arrayList;
        }

        public ArrayList GetArrayLists(string strQuery,
            CommandType commandType)
        {
            ArrayList arrayList = new ArrayList();
            try
            {
                SqlCommand sqlCommand = new SqlCommand(
                    strQuery, clsConnection.sqlConnection);
                sqlCommand.CommandType = commandType;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    object[] objs = new object[sqlDataReader.FieldCount];
                    sqlDataReader.GetValues(objs);
                    arrayList.Add(objs);
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }

            return arrayList;
        }

        #endregion

        #region ExecuteReader and object
        public object[] GetObjects(string strQuery,
            CommandType commandType)
        {
            object[] info = null;
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.Connection = clsConnection.sqlConnection;
                sqlCommand.CommandType = commandType;
                SqlDataReader sqlDataReader;
                sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.Read())
                {
                    info = new object[sqlDataReader.FieldCount];
                    sqlDataReader.GetValues(info);
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;

            }

            return info;
        }


        public object[] GetObjects(string strQuery,
            CommandType commandType, string[] Parameters,
         object[] Values)
        {
            object[] objValue = null;
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.Connection = clsConnection.sqlConnection;
                sqlCommand.CommandType = commandType;
                int i = 0;
                DefineSqlParameter(sqlCommand, Parameters, Values );

                SqlDataReader sqlDataReader;
                sqlDataReader = sqlCommand.ExecuteReader();
                i = sqlDataReader.FieldCount;
                if (sqlDataReader.Read())
                {
                    objValue = new object[i];
                    sqlDataReader.GetValues(objValue);
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;

            }

            return objValue;
        }

        public object[] GetObjects(string strQuery,
            CommandType commandType, string[] Parameters,
         object[] Values, SqlDbType[] sqlDbType)
        {
            object[] info = null;
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.Connection = clsConnection.sqlConnection;
                sqlCommand.CommandType = commandType;
                int i = 0;
                DefineSqlParameter(sqlCommand, Parameters, Values, sqlDbType);
                SqlDataReader sqlDataReader;
                sqlDataReader = sqlCommand.ExecuteReader();
                i = sqlDataReader.FieldCount;

                if (sqlDataReader.Read())
                {
                    info = new object[i];
                    sqlDataReader.GetValues(info);
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;

            }

            return info;
        }

        #endregion

        #region DataSet
        public DataSet FillDataSet(string strParentQuery,
            string parentMember, string strChildQuery,
             string childMember,
            CommandType commandType)
        {
            DataSet dataSet = new DataSet();
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strParentQuery;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection =
                    clsConnection.sqlConnection;
                SqlDataAdapter sqlDataAdapter =
                    new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet, parentMember);

                sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strChildQuery;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection =
                    clsConnection.sqlConnection;
                sqlDataAdapter =
                    new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet, childMember);

                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return dataSet;

        }

        public DataSet FillDataSet(string strQuery,
            CommandType commandType)
        {
            DataSet dataSet = new DataSet();
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection =
                    clsConnection.sqlConnection;
                SqlDataAdapter sqlDataAdapter =
                    new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return dataSet;

        }
        public DataSet FillDataSet(string strQuery,
            CommandType commandType, string[] Parameters,
            string[] Values)
        {
            DataSet dataSet = new DataSet();
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;
                SqlParameter sqlParameter;
                for (int i = 0; i < Parameters.Length; i++)
                {
                    sqlParameter = new SqlParameter();
                    sqlParameter.ParameterName = Parameters[i];
                    sqlParameter.SqlValue = Values[i];
                    sqlCommand.Parameters.Add(sqlParameter);
                }
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return dataSet;

        }

        public int UpdateDataSet(string strStoreProcedure,
            CommandType commandType, string[] Parameters,
            string[] Values, DataSet dataSet)
        {
            int effectRecord = 0;
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strStoreProcedure;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;
                SqlParameter sqlParameter;
                for (int i = 0; i < Parameters.Length; i++)
                {
                    sqlParameter = new SqlParameter();
                    sqlParameter.ParameterName = Parameters[i];
                    sqlParameter.SqlValue = Values[i];
                    sqlCommand.Parameters.Add(sqlParameter);
                }
                SqlDataAdapter sqlDataAdapter =
                    new SqlDataAdapter(sqlCommand);
                SqlCommandBuilder sqlCommandBuilder =
                    new SqlCommandBuilder(sqlDataAdapter);
                effectRecord = sqlDataAdapter.Update(dataSet);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return effectRecord;

        }
        public int UpdateDataSet(string strQuery, DataSet dataSet)
        {
            int effectRecord = 0;
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.Connection = clsConnection.sqlConnection;

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
                effectRecord = sqlDataAdapter.Update(dataSet);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return effectRecord;

        }
        public int UpdateDataSet(string strStoreProcedure,
            CommandType commandType, DataSet dataSet)
        {
            int effectRecord = 0;
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strStoreProcedure;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;

                SqlDataAdapter sqlDataAdapter =
                    new SqlDataAdapter(sqlCommand);
                SqlCommandBuilder sqlCommandBuilder =
                    new SqlCommandBuilder(sqlDataAdapter);
                effectRecord = sqlDataAdapter.Update(dataSet);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return effectRecord;

        }
        #endregion

        #region FillDataTableReader
        public DataTableReader FillDataTableReader(string strQuery,
            CommandType commandType)
        {
            DataTableReader dataTableReader = null;
            try
            {
                DataTable dataTable = new DataTable();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;
                SqlDataAdapter sqlDataAdapter =
                    new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                dataTableReader = dataTable.CreateDataReader();
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return dataTableReader;

        }
        #endregion

        #region FillSqlDataReader
        public SqlDataReader FillSqlDataReader(string strQuery,
            CommandType commandType)
        {
            SqlDataReader dataTableReader = null;
            try
            {
                DataTable dataTable = new DataTable();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;
                dataTableReader = sqlCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return dataTableReader;

        }
                #endregion
        #region DataTable
        public DataTable FillDataTable(string strQuery,
            CommandType commandType)
        {
            DataTable dataTable = new DataTable();
            try
             {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;
                SqlDataAdapter sqlDataAdapter =
                    new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return dataTable;

        }

        public DataTable FillDataTable(string strQuery,
            CommandType commandType,
            string[] Parameters, string[] Values)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;
                DefineSqlParameter(sqlCommand, Parameters, Values);
                SqlDataAdapter sqlDataAdapter =
                    new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return dataTable;

        }

        public DataTable FillDataTable(string strQuery,
    CommandType commandType,
    string[] Parameters, object[] Values, SqlDbType[] sqldbType )
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strQuery;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;
                DefineSqlParameter(sqlCommand, Parameters, Values, sqldbType);
                SqlDataAdapter sqlDataAdapter =
                    new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return dataTable;

        }

        public int UpdateDataTable(
            string strStoreProcedure,
            CommandType commandType, DataTable dataTable)
        {
            int effectRecord = 0;
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strStoreProcedure;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);

                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
                effectRecord = sqlDataAdapter.Update(dataTable);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return effectRecord;

        }

        public int UpdateDataTable(string strStoreProcedure,
             CommandType commandType, string[] Parameters,
             string[] Values, DataTable dataTable)
        {
            int effectRecord = 0;
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = strStoreProcedure;
                sqlCommand.CommandType = commandType;
                sqlCommand.Connection = clsConnection.sqlConnection;
                DefineSqlParameter(sqlCommand, Parameters, Values);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);

                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
                effectRecord = sqlDataAdapter.Update(dataTable);
                sqlDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return effectRecord;

        }

        #endregion
    }
}
