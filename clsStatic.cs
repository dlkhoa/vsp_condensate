using System.Windows.Forms;
using System.Collections;

namespace TrueTech.VSP.VSPCondensate
{
    class clsStatic
    {
        public static string strServerName = "";

        public static string strUser = "";
        public static string strPwd = "";
        public static bool bIsAdminGroup = false;
        public static bool bAccessProduction = false;
        public static bool bAccessInjection = false;
        public static bool bCanChangeData = false;

        public static string strInput = "";
        public static string strMasterInput = "";
        public static string connectionString = "";
        
        public static int scHeight
        {
            get
            {
                Screen scr = Screen.PrimaryScreen;
                return scr.WorkingArea.Height;
            }
        }
        public static int scWidth
        {
            get
            {
                Screen scr = Screen.PrimaryScreen;
                return scr.WorkingArea.Width;
            }
        }

    }
}
