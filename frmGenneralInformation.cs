﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmGenneralInformation : Form
    {
        clsControls control = new clsControls();
        clsDatabases database = new clsDatabases();

        SqlDataAdapter sdaField;
        SqlCommandBuilder scbField;
        DataTable dtField;

        SqlDataAdapter sdaPlatform;
        SqlCommandBuilder scbPlatform;
        DataTable dtPlatform;

        SqlDataAdapter sdaReservoir;
        SqlCommandBuilder scbReservoir;
        DataTable dtReservoir;

        SqlDataAdapter sdaDome;
        SqlCommandBuilder scbDome;
        DataTable dtDome;

        SqlDataAdapter sdaZone;
        SqlCommandBuilder scbZone;
        DataTable dtZone;

        SqlDataAdapter sdaMethod;
        SqlCommandBuilder scbMethod;
        DataTable dtMethod;

        public frmGenneralInformation()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SaveField()
        {
            scbField = new SqlCommandBuilder(sdaField);
            sdaField.Update(dtField);
        }

        private void SavePlatform()
        {
            scbPlatform = new SqlCommandBuilder(sdaPlatform);
            sdaPlatform.Update(dtPlatform);
        }

        private void SaveReservoir()
        {
            scbReservoir = new SqlCommandBuilder(sdaReservoir);
            sdaReservoir.Update(dtReservoir);
        }

        private void SaveDome()
        {
            scbDome = new SqlCommandBuilder(sdaDome);
            sdaDome.Update(dtDome);
        }

        private void SaveZone()
        {
            scbZone = new SqlCommandBuilder(sdaZone);
            sdaZone.Update(dtZone);
        }

        private void SaveMethod()
        {
            scbMethod = new SqlCommandBuilder(sdaMethod);
            sdaMethod.Update(dtMethod);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tabField.Focus())
                SaveField();
            if (tabPlatform.Focus())
                SavePlatform();
            if (tabReservoir.Focus())
                SaveReservoir();
            if (tabDome.Focus())
                SaveDome();
            if (tabZone.Focus())
                SaveZone();
            if (tabMethod.Focus())
                SaveMethod();
        }



        private void frmGenneralInformation_Load(object sender, EventArgs e)
        {
            LoadDataToGrid();

            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboField = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            field_ID.ColumnEdit = cboField;
            cboField.DataSource = database.FillDataTable("SELECT FIELD_ID, FIELD_NAME_V FROM VSP_FIELD_HDR", CommandType.Text);
            cboField.DisplayMember = "FIELD_NAME_V";
            cboField.ValueMember = "FIELD_ID";  
        }

        private void gridViewField_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (MessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) !=
                  DialogResult.Yes)
                    return;
                DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        private void gridViewPlatform_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (MessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) !=
                  DialogResult.Yes)
                    return;
                DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        private void gridViewReservoir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (MessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) !=
                  DialogResult.Yes)
                    return;
                DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        private void gridViewZone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (MessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) !=
                  DialogResult.Yes)
                    return;
                DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        private void gridViewDome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (MessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) !=
                  DialogResult.Yes)
                    return;
                DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        private void gridViewMethod_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (MessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) !=
                  DialogResult.Yes)
                    return;
                DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadDataToGrid();
        }


        private void LoadDataToGrid()
        {
            //Field tab
            sdaField = new SqlDataAdapter(@"SELECT * FROM VSP_FIELD_HDR ORDER BY ORDER_NUM", clsConnection.sqlConnection);
            dtField = new DataTable();
            sdaField.Fill(dtField);
            gridControlField.DataSource = dtField;

            //Platform tab
            sdaPlatform = new SqlDataAdapter(@"SELECT * FROM VSP_PLATFORM ORDER BY ORDER_NUM", clsConnection.sqlConnection);
            dtPlatform = new DataTable();
            sdaPlatform.Fill(dtPlatform);
            gridControlPlatform.DataSource = dtPlatform;

            //Reservoir tab
            sdaReservoir = new SqlDataAdapter(@"SELECT * FROM VSP_EPOCH ORDER BY ORDER_NUM", clsConnection.sqlConnection);
            dtReservoir = new DataTable();
            sdaReservoir.Fill(dtReservoir);
            gridControlReservoir.DataSource = dtReservoir;

            //Dome tab
            sdaDome = new SqlDataAdapter(@"SELECT * FROM VSP_DOME ORDER BY ORDER_NUM", clsConnection.sqlConnection);
            dtDome = new DataTable();
            sdaDome.Fill(dtDome);
            gridControlDome.DataSource = dtDome;

            //Zone tab
            sdaZone = new SqlDataAdapter(@"SELECT * FROM VSP_ZONE ORDER BY ORDER_NUM", clsConnection.sqlConnection);
            dtZone = new DataTable();
            sdaZone.Fill(dtZone);
            gridControlZone.DataSource = dtZone;

            //Method tab
            sdaMethod = new SqlDataAdapter(@"SELECT * FROM VSP_METHOD ORDER BY ORDER_NUM", clsConnection.sqlConnection);
            dtMethod = new DataTable();
            sdaMethod.Fill(dtMethod);
            gridControlMethod.DataSource = dtMethod;
        }

        private void frmGenneralInformation_FormClosed(object sender, FormClosedEventArgs e)
        {
            Session.frmGenneralInformation = null;
        }
    }
}
