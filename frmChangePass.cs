using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmChangePass : Form
    {
        clsDatabases database = new clsDatabases();

        public frmChangePass()
        {
            InitializeComponent();
        }

        private void frmChangePass_Load(object sender, EventArgs e)
        {
            txtUserID.Text = clsStatic.strUser;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            ChangePass();
        }
        private void ChangePass()
        {
            //database = new clsDatabases();
            string sqlCmd = "SELECT UserName FROM SYS_Users WHERE UserName = @userID AND Convert(varchar(100), DecryptByPassPhrase('DBProd', PassWord)) = '"
                + txtOldPass.Text + "'";
            DataTable table = database.FillDataTable(sqlCmd, CommandType.Text, new string[1] { "userID" },
                new string[1] { txtUserID.Text});
            if (table.Rows.Count == 0)
            {
                MessageBox.Show("Sai mật khẩu");
                txtOldPass.Focus();
            }
            else
            {
                if (txtNewPass.Text != txtReNewPass.Text)
                {
                    MessageBox.Show("Mật khẩu mới phải trùng nhau!");
                    txtReNewPass.Focus();
                }
                else
                {
                    sqlCmd = "UPDATE SYS_Users SET Password = EncryptByPassPhrase('DBProd', Convert(varchar(MAX), '"
                    + txtNewPass.Text + "')) WHERE UserName = '" + txtUserID.Text + "'";
                    int i = database.ExecuteNonQuery(sqlCmd, CommandType.Text, new string[2] { "password", "userID" },
                        new string[2] { txtReNewPass.Text, txtUserID.Text }, new SqlDbType[2] { SqlDbType.NVarChar, SqlDbType.NVarChar});
                    if (i > 0)
                    {
                        MessageBox.Show("Thay đổi mật khẩu thành công!");
                        this.Close();
                    }
                }
            }
        }
        private void txtOldPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ChangePass();
        }

        private void txtNewPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ChangePass();
        }

        private void txtReNewPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ChangePass();
        }
    }
}