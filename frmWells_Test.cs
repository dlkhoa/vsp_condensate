﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmWells_Test : Form
    {
        SqlDataAdapter sdaWellHdr;
        SqlCommandBuilder scbWellHdr;
        DataTable dtWellHdr;
        clsDatabases database = new clsDatabases();

        public frmWells_Test()
        {
            InitializeComponent();
        }

        private void frmWells_Test_Load(object sender, EventArgs e)
        {
            //tab Thong tin chung
            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboField = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            field.ColumnEdit = cboField;
            cboField.DataSource = database.FillDataTable("SELECT FIELD_ID, FIELD_NAME_V FROM VSP_FIELD_HDR", CommandType.Text);
            cboField.DisplayMember = "FIELD_NAME_V";
            cboField.ValueMember = "FIELD_ID";

            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboPlatform = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            platform.ColumnEdit = cboPlatform;
            cboPlatform.DataSource = database.FillDataTable("SELECT PLATFORM FROM VSP_PLATFORM", CommandType.Text);
            cboPlatform.DisplayMember = "PLATFORM";
            cboPlatform.ValueMember = "PLATFORM";

            sdaWellHdr = new SqlDataAdapter(@"SELECT UWI, WELL_NAME, WELL_NUMBER, WELL_NUMBER_SORT,
                                        PLATFORM, FIELD_ID FROM VSP_WELL_HDR", clsConnection.sqlConnection);
            dtWellHdr = new DataTable();
            sdaWellHdr.Fill(dtWellHdr);
            gridControlWellHdr.DataSource = dtWellHdr;
        }


        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (tabPage1.Focus())
            {
                scbWellHdr = new SqlCommandBuilder(sdaWellHdr);
                sdaWellHdr.Update(dtWellHdr);
            }
        }

        private void gridViewWellHdr_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (MessageBox.Show("Xóa toàn bộ dữ liệu khai thác và bơm ép của giếng?", "Confirmation", MessageBoxButtons.YesNo) !=
                  DialogResult.Yes)
                    return;
                DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmWells_Test_FormClosed(object sender, FormClosedEventArgs e)
        {
            Session.frmWells = null;
        }
    }
}
