﻿using DevExpress.XtraBars;
using DevExpress.XtraSplashScreen;
namespace TrueTech.VSP.VSPCondensate
{
    partial class frmMain {

        #region Designer generated code
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup2 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            this.iWeb = new DevExpress.XtraBars.BarButtonItem();
            this.iAbout = new DevExpress.XtraBars.BarButtonItem();
            this.iCenter = new DevExpress.XtraBars.BarButtonItem();
            this.iSelectAll = new DevExpress.XtraBars.BarButtonItem();
            this.iCopy = new DevExpress.XtraBars.BarButtonItem();
            this.iCut = new DevExpress.XtraBars.BarButtonItem();
            this.iPaste = new DevExpress.XtraBars.BarButtonItem();
            this.iClear = new DevExpress.XtraBars.BarButtonItem();
            this.iFont = new DevExpress.XtraBars.BarButtonItem();
            this.beiFontSize = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bbiFontColorPopup = new DevExpress.XtraBars.BarButtonItem();
            this.popupControlContainer1 = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.ribbonControlMain = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageCollectionSmall = new DevExpress.Utils.ImageCollection(this.components);
            this.iCondensateImport = new DevExpress.XtraBars.BarButtonItem();
            this.iSave = new DevExpress.XtraBars.BarButtonItem();
            this.iUndo = new DevExpress.XtraBars.BarButtonItem();
            this.iReplace = new DevExpress.XtraBars.BarButtonItem();
            this.iPassword = new DevExpress.XtraBars.BarButtonItem();
            this.iClose = new DevExpress.XtraBars.BarButtonItem();
            this.iSaveAs = new DevExpress.XtraBars.BarButtonItem();
            this.iPrint = new DevExpress.XtraBars.BarButtonItem();
            this.iExit = new DevExpress.XtraBars.BarButtonItem();
            this.iFind = new DevExpress.XtraBars.BarButtonItem();
            this.iBullets = new DevExpress.XtraBars.BarButtonItem();
            this.iProtected = new DevExpress.XtraBars.BarButtonItem();
            this.iAdminSettings = new DevExpress.XtraBars.BarButtonItem();
            this.iAdminWells = new DevExpress.XtraBars.BarButtonItem();
            this.iCondensateReportMonthly = new DevExpress.XtraBars.BarButtonItem();
            this.iAlignLeft = new DevExpress.XtraBars.BarButtonItem();
            this.iAlignRight = new DevExpress.XtraBars.BarButtonItem();
            this.iFontColor = new DevExpress.XtraBars.BarButtonItem();
            this.siPosition = new DevExpress.XtraBars.BarButtonItem();
            this.siModified = new DevExpress.XtraBars.BarButtonItem();
            this.siDocName = new DevExpress.XtraBars.BarStaticItem();
            this.bgFontStyle = new DevExpress.XtraBars.BarButtonGroup();
            this.bgAlign = new DevExpress.XtraBars.BarButtonGroup();
            this.bgFont = new DevExpress.XtraBars.BarButtonGroup();
            this.bgBullets = new DevExpress.XtraBars.BarButtonGroup();
            this.sbiSave = new DevExpress.XtraBars.BarSubItem();
            this.idAbout = new DevExpress.XtraBars.BarSubItem();
            this.iPasteSpecial = new DevExpress.XtraBars.BarButtonItem();
            this.sbiFind = new DevExpress.XtraBars.BarSubItem();
            this.iNew = new DevExpress.XtraBars.BarButtonItem();
            this.iAdminUser = new DevExpress.XtraBars.BarLargeButtonItem();
            this.iTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.iPaintStyle = new DevExpress.XtraBars.BarButtonItem();
            this.rgbiSkins = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.rgbiFont = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.rgbiFontColor = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.biStyle = new DevExpress.XtraBars.BarEditItem();
            this.riicStyle = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.editButtonGroup = new DevExpress.XtraBars.BarButtonGroup();
            this.beScheme = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barToggleSwitchItem1 = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.bbColorMix = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonGroup1 = new DevExpress.XtraBars.BarButtonGroup();
            this.iFileAbout = new DevExpress.XtraBars.BarButtonItem();
            this.imageCollectionLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonPageApp = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupFile = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupCondensate = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAdmin = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupExit = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.pccBottom = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.sbExit = new DevExpress.XtraEditors.SimpleButton();
            this.pcAppMenuFileLabels = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.backstageViewClientControl7 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl1 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl3 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.iCondensateReportChart = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riicStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccBottom)).BeginInit();
            this.pccBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcAppMenuFileLabels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // iWeb
            // 
            this.iWeb.Caption = "&DevExpress on the Web";
            this.iWeb.CategoryGuid = new System.Guid("e07a4c24-66ac-4de6-bbcb-c0b6cfa7798b");
            this.iWeb.Description = "Opens the web page.";
            this.iWeb.Hint = "DevExpress on the Web";
            this.iWeb.Id = 21;
            this.iWeb.ImageOptions.ImageIndex = 24;
            this.iWeb.Name = "iWeb";
            // 
            // iAbout
            // 
            this.iAbout.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.iAbout.Caption = "&About";
            this.iAbout.CategoryGuid = new System.Guid("e07a4c24-66ac-4de6-bbcb-c0b6cfa7798b");
            this.iAbout.Description = "Displays the description of this program.";
            this.iAbout.Hint = "Displays the About dialog";
            this.iAbout.Id = 22;
            this.iAbout.ImageOptions.ImageIndex = 28;
            this.iAbout.Name = "iAbout";
            this.iAbout.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // iCenter
            // 
            this.iCenter.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iCenter.Caption = "&Center";
            this.iCenter.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iCenter.Description = "Centers the selected text.";
            this.iCenter.GroupIndex = 1;
            this.iCenter.Hint = "Center";
            this.iCenter.Id = 28;
            this.iCenter.ImageOptions.ImageIndex = 19;
            this.iCenter.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E));
            this.iCenter.Name = "iCenter";
            // 
            // iSelectAll
            // 
            this.iSelectAll.Caption = "Select A&ll";
            this.iSelectAll.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iSelectAll.Description = "Selects all text in the active document.";
            this.iSelectAll.Hint = "Selects all text in the active document.";
            this.iSelectAll.Id = 13;
            this.iSelectAll.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A));
            this.iSelectAll.Name = "iSelectAll";
            // 
            // iCopy
            // 
            this.iCopy.Caption = "&Copy";
            this.iCopy.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iCopy.Description = "Copies the selection to the Clipboard.";
            this.iCopy.Hint = "Copy";
            this.iCopy.Id = 10;
            this.iCopy.ImageOptions.ImageIndex = 1;
            this.iCopy.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C));
            this.iCopy.Name = "iCopy";
            // 
            // iCut
            // 
            this.iCut.Caption = "Cu&t";
            this.iCut.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iCut.Description = "Removes the selection from the active document and places it on the Clipboard.";
            this.iCut.Hint = "Cut";
            this.iCut.Id = 9;
            this.iCut.ImageOptions.ImageIndex = 2;
            this.iCut.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X));
            this.iCut.Name = "iCut";
            // 
            // iPaste
            // 
            this.iPaste.Caption = "&Paste";
            this.iPaste.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iPaste.Description = "Inserts the contents of the Clipboard at the insertion point, and replaces any se" +
    "lection. This command is available only if you have cut or copied a text.";
            this.iPaste.Hint = "Paste";
            this.iPaste.Id = 11;
            this.iPaste.ImageOptions.ImageIndex = 8;
            this.iPaste.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V));
            this.iPaste.Name = "iPaste";
            // 
            // iClear
            // 
            this.iClear.Caption = "Cle&ar";
            this.iClear.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iClear.Description = "Deletes the selected text without putting it on the Clipboard. This command is av" +
    "ailable only if a text is selected. ";
            this.iClear.Hint = "Clear";
            this.iClear.Id = 12;
            this.iClear.ImageOptions.ImageIndex = 13;
            this.iClear.Name = "iClear";
            // 
            // iFont
            // 
            this.iFont.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.iFont.Caption = "&Font...";
            this.iFont.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iFont.Description = "Changes the font and character spacing formats of the selected text.";
            this.iFont.Hint = "Font Dialog";
            this.iFont.Id = 17;
            this.iFont.ImageOptions.ImageIndex = 4;
            this.iFont.Name = "iFont";
            this.iFont.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // beiFontSize
            // 
            this.beiFontSize.Caption = "Font Size";
            this.beiFontSize.Edit = this.repositoryItemSpinEdit1;
            this.beiFontSize.Hint = "Font Size";
            this.beiFontSize.Id = 27;
            this.beiFontSize.Name = "beiFontSize";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // bbiFontColorPopup
            // 
            this.bbiFontColorPopup.ActAsDropDown = true;
            this.bbiFontColorPopup.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiFontColorPopup.Caption = "Font Color";
            this.bbiFontColorPopup.Description = "Formats the selected text with the color you click";
            this.bbiFontColorPopup.DropDownControl = this.popupControlContainer1;
            this.bbiFontColorPopup.Hint = "Formats the selected text with the color you click";
            this.bbiFontColorPopup.Id = 36;
            this.bbiFontColorPopup.Name = "bbiFontColorPopup";
            // 
            // popupControlContainer1
            // 
            this.popupControlContainer1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.popupControlContainer1.Location = new System.Drawing.Point(0, 0);
            this.popupControlContainer1.Name = "popupControlContainer1";
            this.popupControlContainer1.Size = new System.Drawing.Size(0, 0);
            this.popupControlContainer1.TabIndex = 6;
            this.popupControlContainer1.Visible = false;
            // 
            // ribbonControlMain
            // 
            this.ribbonControlMain.ApplicationButtonText = null;
            this.ribbonControlMain.AutoSizeItems = true;
            this.ribbonControlMain.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("File", new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f")),
            new DevExpress.XtraBars.BarManagerCategory("Edit", new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1")),
            new DevExpress.XtraBars.BarManagerCategory("Format", new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258")),
            new DevExpress.XtraBars.BarManagerCategory("Help", new System.Guid("e07a4c24-66ac-4de6-bbcb-c0b6cfa7798b")),
            new DevExpress.XtraBars.BarManagerCategory("Status", new System.Guid("77795bb7-9bc5-4dd2-a297-cc758682e23d"))});
            this.ribbonControlMain.ExpandCollapseItem.Id = 0;
            this.ribbonControlMain.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControlMain.Images = this.imageCollectionSmall;
            this.ribbonControlMain.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControlMain.ExpandCollapseItem,
            this.ribbonControlMain.SearchEditItem,
            this.iCondensateImport,
            this.iSave,
            this.iUndo,
            this.iReplace,
            this.iPassword,
            this.iClose,
            this.iSaveAs,
            this.iPrint,
            this.iExit,
            this.iCut,
            this.iCopy,
            this.iPaste,
            this.iClear,
            this.iSelectAll,
            this.iFind,
            this.iFont,
            this.iBullets,
            this.iProtected,
            this.iWeb,
            this.iAbout,
            this.iAdminSettings,
            this.iAdminWells,
            this.iCondensateReportMonthly,
            this.iAlignLeft,
            this.iCenter,
            this.iAlignRight,
            this.iFontColor,
            this.siPosition,
            this.siModified,
            this.siDocName,
            this.bgFontStyle,
            this.bgAlign,
            this.bgFont,
            this.bgBullets,
            this.sbiSave,
            this.idAbout,
            this.sbiFind,
            this.iPasteSpecial,
            this.iNew,
            this.iAdminUser,
            this.iTemplate,
            this.iPaintStyle,
            this.rgbiSkins,
            this.beiFontSize,
            this.rgbiFont,
            this.bbiFontColorPopup,
            this.rgbiFontColor,
            this.barEditItem1,
            this.biStyle,
            this.editButtonGroup,
            this.beScheme,
            this.barToggleSwitchItem1,
            this.bbColorMix,
            this.barButtonGroup1,
            this.iFileAbout,
            this.iCondensateReportChart});
            this.ribbonControlMain.LargeImages = this.imageCollectionLarge;
            this.ribbonControlMain.Location = new System.Drawing.Point(0, 0);
            this.ribbonControlMain.MaxItemId = 256;
            this.ribbonControlMain.Name = "ribbonControlMain";
            this.ribbonControlMain.PageCategoryAlignment = DevExpress.XtraBars.Ribbon.RibbonPageCategoryAlignment.Right;
            this.ribbonControlMain.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageApp});
            this.ribbonControlMain.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemPictureEdit1,
            this.riicStyle,
            this.repositoryItemComboBox1,
            this.repositoryItemTrackBar1});
            this.ribbonControlMain.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2019;
            this.ribbonControlMain.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControlMain.ShowCategoryInCaption = false;
            this.ribbonControlMain.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControlMain.ShowMoreCommandsButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControlMain.ShowToolbarCustomizeItem = false;
            this.ribbonControlMain.Size = new System.Drawing.Size(953, 178);
            this.ribbonControlMain.StatusBar = this.ribbonStatusBar1;
            this.ribbonControlMain.Toolbar.ShowCustomizeItem = false;
            // 
            // imageCollectionSmall
            // 
            this.imageCollectionSmall.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionSmall.ImageStream")));
            this.imageCollectionSmall.Images.SetKeyName(0, "Key16.png");
            this.imageCollectionSmall.Images.SetKeyName(1, "app16.png");
            this.imageCollectionSmall.Images.SetKeyName(3, "import16.png");
            this.imageCollectionSmall.Images.SetKeyName(4, "Report16.png");
            this.imageCollectionSmall.Images.SetKeyName(5, "User16.png");
            this.imageCollectionSmall.Images.SetKeyName(6, "Settings16.png");
            this.imageCollectionSmall.Images.SetKeyName(7, "Well16.png");
            this.imageCollectionSmall.Images.SetKeyName(8, "chart16.png");
            // 
            // iCondensateImport
            // 
            this.iCondensateImport.Caption = "Nhập dữ liệu từ Excel";
            this.iCondensateImport.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iCondensateImport.Description = "Nhập dữ liệu từ Excel";
            this.iCondensateImport.Hint = "Nhập dữ liệu từ Excel";
            this.iCondensateImport.Id = 1;
            this.iCondensateImport.ImageOptions.ImageIndex = 3;
            this.iCondensateImport.ImageOptions.LargeImageIndex = 3;
            this.iCondensateImport.Name = "iCondensateImport";
            this.iCondensateImport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCondensateImport_ItemClick);
            // 
            // iSave
            // 
            this.iSave.Caption = "&Save";
            this.iSave.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iSave.Description = "Saves the active document with its current file name.";
            this.iSave.Hint = "Saves the active document with its current file name";
            this.iSave.Id = 3;
            this.iSave.ImageOptions.ImageIndex = 10;
            this.iSave.ImageOptions.LargeImageIndex = 7;
            this.iSave.Name = "iSave";
            this.iSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // iUndo
            // 
            this.iUndo.Caption = "&Undo";
            this.iUndo.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iUndo.Description = "Reverses the last command or deletes the last entry you typed.";
            this.iUndo.Hint = "Undo";
            this.iUndo.Id = 8;
            this.iUndo.ImageOptions.ImageIndex = 11;
            this.iUndo.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z));
            this.iUndo.Name = "iUndo";
            this.iUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAdminUser_ItemClick);
            // 
            // iReplace
            // 
            this.iReplace.Caption = "R&eplace...";
            this.iReplace.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iReplace.Description = "Searches for and replaces the specified text.";
            this.iReplace.Hint = "Replace";
            this.iReplace.Id = 15;
            this.iReplace.ImageOptions.ImageIndex = 14;
            this.iReplace.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H));
            this.iReplace.Name = "iReplace";
            // 
            // iPassword
            // 
            this.iPassword.Caption = "Đổi MK";
            this.iPassword.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iPassword.Description = "Đổi mật khẩu";
            this.iPassword.Hint = "Đổi mật khẩu";
            this.iPassword.Id = 0;
            this.iPassword.ImageOptions.ImageIndex = 6;
            this.iPassword.ImageOptions.LargeImageIndex = 0;
            this.iPassword.Name = "iPassword";
            this.iPassword.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iPassword_ItemClick);
            // 
            // iClose
            // 
            this.iClose.Caption = "&Close";
            this.iClose.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iClose.Description = "Closes the active document.";
            this.iClose.Hint = "Closes the active document";
            this.iClose.Id = 2;
            this.iClose.ImageOptions.ImageIndex = 12;
            this.iClose.ImageOptions.LargeImageIndex = 8;
            this.iClose.Name = "iClose";
            this.iClose.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // iSaveAs
            // 
            this.iSaveAs.Caption = "Save &As...";
            this.iSaveAs.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iSaveAs.Description = "Saves the active document with a different file name.";
            this.iSaveAs.Hint = "Saves the active document with a different file name";
            this.iSaveAs.Id = 4;
            this.iSaveAs.ImageOptions.ImageIndex = 21;
            this.iSaveAs.ImageOptions.LargeImageIndex = 2;
            this.iSaveAs.Name = "iSaveAs";
            // 
            // iPrint
            // 
            this.iPrint.Caption = "&Print";
            this.iPrint.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iPrint.Description = "Prints the active document.";
            this.iPrint.Hint = "Prints the active document";
            this.iPrint.Id = 5;
            this.iPrint.ImageOptions.ImageIndex = 9;
            this.iPrint.ImageOptions.LargeImageIndex = 6;
            this.iPrint.Name = "iPrint";
            this.iPrint.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // iExit
            // 
            this.iExit.Caption = "Thoát";
            this.iExit.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iExit.Description = "Thoát";
            this.iExit.Hint = "Thoát";
            this.iExit.Id = 6;
            this.iExit.ImageOptions.ImageIndex = 22;
            this.iExit.ImageOptions.LargeImageIndex = 2;
            this.iExit.Name = "iExit";
            this.iExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iExit_ItemClick);
            // 
            // iFind
            // 
            this.iFind.Caption = "&Find...";
            this.iFind.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iFind.Description = "Searches for the specified text.";
            this.iFind.Hint = "Find";
            this.iFind.Id = 14;
            this.iFind.ImageOptions.ImageIndex = 3;
            this.iFind.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F));
            this.iFind.Name = "iFind";
            // 
            // iBullets
            // 
            this.iBullets.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iBullets.Caption = "&Bullets";
            this.iBullets.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iBullets.Description = "Adds bullets to or removes bullets from selected paragraphs.";
            this.iBullets.Hint = "Bullets";
            this.iBullets.Id = 18;
            this.iBullets.ImageOptions.ImageIndex = 0;
            this.iBullets.Name = "iBullets";
            // 
            // iProtected
            // 
            this.iProtected.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iProtected.Caption = "P&rotected";
            this.iProtected.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iProtected.Description = "Protects the selected text.";
            this.iProtected.Hint = "Protects the selected text";
            this.iProtected.Id = 19;
            this.iProtected.Name = "iProtected";
            // 
            // iAdminSettings
            // 
            this.iAdminSettings.Caption = "Tham số chung";
            this.iAdminSettings.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iAdminSettings.Description = "Tham số chung";
            this.iAdminSettings.Hint = "Tham số chung";
            this.iAdminSettings.Id = 24;
            this.iAdminSettings.ImageOptions.ImageIndex = 6;
            this.iAdminSettings.ImageOptions.LargeImageIndex = 6;
            this.iAdminSettings.Name = "iAdminSettings";
            this.iAdminSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAdminSettings_ItemClick);
            // 
            // iAdminWells
            // 
            this.iAdminWells.Caption = "Giếng khai thác";
            this.iAdminWells.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iAdminWells.Description = "Giếng khai thác";
            this.iAdminWells.Hint = "Giếng khai thác";
            this.iAdminWells.Id = 25;
            this.iAdminWells.ImageOptions.ImageIndex = 7;
            this.iAdminWells.ImageOptions.LargeImageIndex = 7;
            this.iAdminWells.Name = "iAdminWells";
            this.iAdminWells.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAdminWells_ItemClick);
            // 
            // iCondensateReportMonthly
            // 
            this.iCondensateReportMonthly.Caption = "Báo cáo khai thác theo tháng";
            this.iCondensateReportMonthly.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iCondensateReportMonthly.Description = "Báo cáo khai thác theo tháng";
            this.iCondensateReportMonthly.Hint = "Báo cáo khai thác theo tháng";
            this.iCondensateReportMonthly.Id = 26;
            this.iCondensateReportMonthly.ImageOptions.ImageIndex = 4;
            this.iCondensateReportMonthly.ImageOptions.LargeImageIndex = 4;
            this.iCondensateReportMonthly.Name = "iCondensateReportMonthly";
            this.iCondensateReportMonthly.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCondensateReportMonthly_ItemClick);
            // 
            // iAlignLeft
            // 
            this.iAlignLeft.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iAlignLeft.Caption = "Align &Left";
            this.iAlignLeft.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iAlignLeft.Description = "Aligns the selected text to the left.";
            this.iAlignLeft.GroupIndex = 1;
            this.iAlignLeft.Hint = "Align Left";
            this.iAlignLeft.Id = 27;
            this.iAlignLeft.ImageOptions.ImageIndex = 18;
            this.iAlignLeft.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L));
            this.iAlignLeft.Name = "iAlignLeft";
            // 
            // iAlignRight
            // 
            this.iAlignRight.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.iAlignRight.Caption = "Align &Right";
            this.iAlignRight.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iAlignRight.Description = "Aligns the selected text to the right.";
            this.iAlignRight.GroupIndex = 1;
            this.iAlignRight.Hint = "Align Right";
            this.iAlignRight.Id = 29;
            this.iAlignRight.ImageOptions.ImageIndex = 20;
            this.iAlignRight.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R));
            this.iAlignRight.Name = "iAlignRight";
            // 
            // iFontColor
            // 
            this.iFontColor.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.iFontColor.Caption = "Font C&olor";
            this.iFontColor.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this.iFontColor.Description = "Formats the selected text with the color you click.";
            this.iFontColor.Hint = "Font Color";
            this.iFontColor.Id = 30;
            this.iFontColor.ImageOptions.ImageIndex = 5;
            this.iFontColor.Name = "iFontColor";
            // 
            // siPosition
            // 
            this.siPosition.CategoryGuid = new System.Guid("77795bb7-9bc5-4dd2-a297-cc758682e23d");
            this.siPosition.Id = 0;
            this.siPosition.Name = "siPosition";
            // 
            // siModified
            // 
            this.siModified.CategoryGuid = new System.Guid("77795bb7-9bc5-4dd2-a297-cc758682e23d");
            this.siModified.Id = 1;
            this.siModified.ImageOptions.ImageIndex = 27;
            this.siModified.Name = "siModified";
            // 
            // siDocName
            // 
            this.siDocName.CategoryGuid = new System.Guid("77795bb7-9bc5-4dd2-a297-cc758682e23d");
            this.siDocName.Id = 2;
            this.siDocName.Name = "siDocName";
            // 
            // bgFontStyle
            // 
            this.bgFontStyle.Caption = "FontStyle";
            this.bgFontStyle.Id = 0;
            this.bgFontStyle.ItemLinks.Add(this.iAdminSettings);
            this.bgFontStyle.ItemLinks.Add(this.iAdminWells);
            this.bgFontStyle.ItemLinks.Add(this.iCondensateReportMonthly);
            this.bgFontStyle.Name = "bgFontStyle";
            this.bgFontStyle.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bgAlign
            // 
            this.bgAlign.Caption = "Align";
            this.bgAlign.Id = 0;
            this.bgAlign.ItemLinks.Add(this.iAlignLeft);
            this.bgAlign.ItemLinks.Add(this.iCenter);
            this.bgAlign.ItemLinks.Add(this.iAlignRight);
            this.bgAlign.Name = "bgAlign";
            this.bgAlign.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bgFont
            // 
            this.bgFont.Caption = "Font";
            this.bgFont.Id = 0;
            this.bgFont.ItemLinks.Add(this.iFont);
            this.bgFont.ItemLinks.Add(this.iFontColor);
            this.bgFont.Name = "bgFont";
            this.bgFont.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bgBullets
            // 
            this.bgBullets.Caption = "Bullets";
            this.bgBullets.Id = 1;
            this.bgBullets.ItemLinks.Add(this.iBullets);
            this.bgBullets.ItemLinks.Add(this.iProtected);
            this.bgBullets.Name = "bgBullets";
            this.bgBullets.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // sbiSave
            // 
            this.sbiSave.Caption = "Save";
            this.sbiSave.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.sbiSave.Description = "Saves the active document";
            this.sbiSave.Hint = "Saves the active document";
            this.sbiSave.Id = 0;
            this.sbiSave.ImageOptions.ImageIndex = 10;
            this.sbiSave.ImageOptions.LargeImageIndex = 2;
            this.sbiSave.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.iSaveAs)});
            this.sbiSave.MenuCaption = "Save";
            this.sbiSave.Name = "sbiSave";
            this.sbiSave.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // idAbout
            // 
            this.idAbout.Caption = "Giới thiệu";
            this.idAbout.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.idAbout.Description = "Giới thiệu";
            this.idAbout.Hint = "Giới thiệu";
            this.idAbout.Id = 1;
            this.idAbout.ImageOptions.ImageIndex = 1;
            this.idAbout.ImageOptions.LargeImageIndex = 1;
            this.idAbout.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iPaste),
            new DevExpress.XtraBars.LinkPersistInfo(this.iPasteSpecial)});
            this.idAbout.MenuCaption = "Giới thiệu";
            this.idAbout.Name = "idAbout";
            this.idAbout.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // iPasteSpecial
            // 
            this.iPasteSpecial.Caption = "Paste &Special...";
            this.iPasteSpecial.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iPasteSpecial.Description = "Opens the Paste Special dialog";
            this.iPasteSpecial.Enabled = false;
            this.iPasteSpecial.Hint = "Opens the Paste Special dialog";
            this.iPasteSpecial.Id = 3;
            this.iPasteSpecial.ImageOptions.ImageIndex = 8;
            this.iPasteSpecial.Name = "iPasteSpecial";
            // 
            // sbiFind
            // 
            this.sbiFind.Caption = "Find";
            this.sbiFind.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.sbiFind.Description = "Searches for the specified text";
            this.sbiFind.Hint = "Searches for the specified text";
            this.sbiFind.Id = 2;
            this.sbiFind.ImageOptions.ImageIndex = 3;
            this.sbiFind.ImageOptions.LargeImageIndex = 4;
            this.sbiFind.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iFind),
            new DevExpress.XtraBars.LinkPersistInfo(this.iReplace)});
            this.sbiFind.MenuCaption = "Find and Replace";
            this.sbiFind.Name = "sbiFind";
            this.sbiFind.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.sbiFind.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // iNew
            // 
            this.iNew.Caption = "&New";
            this.iNew.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iNew.Description = "Creates a new, blank file.";
            this.iNew.Hint = "Creates a new, blank file";
            this.iNew.Id = 0;
            this.iNew.ImageOptions.ImageIndex = 6;
            this.iNew.ImageOptions.LargeImageIndex = 0;
            this.iNew.Name = "iNew";
            // 
            // iAdminUser
            // 
            this.iAdminUser.Caption = "Người sử dụng";
            this.iAdminUser.CategoryGuid = new System.Guid("7c2486e1-92ea-4293-ad55-b819f61ff7f1");
            this.iAdminUser.Hint = "Quản trị người sử dụng";
            this.iAdminUser.Id = 0;
            this.iAdminUser.ImageOptions.ImageIndex = 5;
            this.iAdminUser.ImageOptions.LargeImageIndex = 5;
            this.iAdminUser.Name = "iAdminUser";
            this.iAdminUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAdminUser_ItemClick);
            // 
            // iTemplate
            // 
            this.iTemplate.Caption = "Template...";
            this.iTemplate.CategoryGuid = new System.Guid("4b511317-d784-42ba-b4ed-0d2a746d6c1f");
            this.iTemplate.Description = "Creates a new template";
            this.iTemplate.Enabled = false;
            this.iTemplate.Hint = "Creates a new template";
            this.iTemplate.Id = 1;
            this.iTemplate.ImageOptions.ImageIndex = 6;
            this.iTemplate.Name = "iTemplate";
            // 
            // iPaintStyle
            // 
            this.iPaintStyle.ActAsDropDown = true;
            this.iPaintStyle.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.iPaintStyle.Caption = "Paint style";
            this.iPaintStyle.Description = "Select a paint scheme";
            this.iPaintStyle.Hint = "Select a paint scheme";
            this.iPaintStyle.Id = 7;
            this.iPaintStyle.ImageOptions.ImageIndex = 26;
            this.iPaintStyle.Name = "iPaintStyle";
            // 
            // rgbiSkins
            // 
            this.rgbiSkins.Caption = "Skins";
            // 
            // 
            // 
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Id = 13;
            this.rgbiSkins.Name = "rgbiSkins";
            // 
            // rgbiFont
            // 
            this.rgbiFont.Caption = "Font";
            // 
            // 
            // 
            galleryItemGroup1.Caption = "Main";
            this.rgbiFont.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.rgbiFont.Gallery.ImageSize = new System.Drawing.Size(40, 40);
            this.rgbiFont.Id = 29;
            this.rgbiFont.Name = "rgbiFont";
            // 
            // rgbiFontColor
            // 
            this.rgbiFontColor.Caption = "Color";
            // 
            // 
            // 
            this.rgbiFontColor.Gallery.ColumnCount = 10;
            galleryItemGroup2.Caption = "Main";
            this.rgbiFontColor.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup2});
            this.rgbiFontColor.Gallery.ImageSize = new System.Drawing.Size(20, 14);
            this.rgbiFontColor.Id = 37;
            this.rgbiFontColor.Name = "rgbiFontColor";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItem1.CanOpenEdit = false;
            this.barEditItem1.Edit = this.repositoryItemPictureEdit1;
            this.barEditItem1.EditWidth = 130;
            this.barEditItem1.Id = 94;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.AllowFocused = false;
            this.repositoryItemPictureEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // biStyle
            // 
            this.biStyle.Edit = this.riicStyle;
            this.biStyle.EditWidth = 75;
            this.biStyle.Hint = "Ribbon Style";
            this.biStyle.Id = 106;
            this.biStyle.Name = "biStyle";
            // 
            // riicStyle
            // 
            this.riicStyle.Appearance.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.riicStyle.Appearance.Options.UseFont = true;
            this.riicStyle.AutoHeight = false;
            this.riicStyle.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riicStyle.Name = "riicStyle";
            // 
            // editButtonGroup
            // 
            this.editButtonGroup.Id = 145;
            this.editButtonGroup.ItemLinks.Add(this.iCut);
            this.editButtonGroup.ItemLinks.Add(this.iCopy);
            this.editButtonGroup.ItemLinks.Add(this.iPaste);
            this.editButtonGroup.ItemLinks.Add(this.iClear);
            this.editButtonGroup.Name = "editButtonGroup";
            this.editButtonGroup.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // beScheme
            // 
            this.beScheme.Edit = this.repositoryItemComboBox1;
            this.beScheme.EditWidth = 75;
            this.beScheme.Id = 188;
            this.beScheme.Name = "beScheme";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // barToggleSwitchItem1
            // 
            this.barToggleSwitchItem1.Caption = "Auto Save";
            this.barToggleSwitchItem1.Id = 213;
            this.barToggleSwitchItem1.Name = "barToggleSwitchItem1";
            this.barToggleSwitchItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbColorMix
            // 
            this.bbColorMix.Caption = "&Color Mix";
            this.bbColorMix.Id = 238;
            this.bbColorMix.ImageOptions.Image = global::TrueTech.VSP.VSPCondensate.Properties.Resources.ColorMixer_16x16;
            this.bbColorMix.ImageOptions.LargeImage = global::TrueTech.VSP.VSPCondensate.Properties.Resources.ColorMixer_32x32;
            this.bbColorMix.ImageOptions.LargeImageIndex = 0;
            this.bbColorMix.Name = "bbColorMix";
            // 
            // barButtonGroup1
            // 
            this.barButtonGroup1.Caption = "barButtonGroup1";
            this.barButtonGroup1.Id = 242;
            this.barButtonGroup1.Name = "barButtonGroup1";
            // 
            // iFileAbout
            // 
            this.iFileAbout.Caption = "Giới thiệu";
            this.iFileAbout.Description = "Giới thiệu về ứng dụng";
            this.iFileAbout.Hint = "Giới thiệu về ứng dụng";
            this.iFileAbout.Id = 250;
            this.iFileAbout.ImageOptions.ImageIndex = 1;
            this.iFileAbout.ImageOptions.LargeImageIndex = 1;
            this.iFileAbout.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iFileAbout.ItemAppearance.Disabled.Options.UseFont = true;
            this.iFileAbout.Name = "iFileAbout";
            this.iFileAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iFileAbout_ItemClick);
            // 
            // imageCollectionLarge
            // 
            this.imageCollectionLarge.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionLarge.ImageStream")));
            this.imageCollectionLarge.Images.SetKeyName(0, "Key32.png");
            this.imageCollectionLarge.Images.SetKeyName(1, "app32.png");
            this.imageCollectionLarge.Images.SetKeyName(3, "import32.png");
            this.imageCollectionLarge.Images.SetKeyName(4, "Report32.png");
            this.imageCollectionLarge.Images.SetKeyName(5, "User32.png");
            this.imageCollectionLarge.Images.SetKeyName(6, "Settings32.png");
            this.imageCollectionLarge.Images.SetKeyName(7, "Well32.png");
            this.imageCollectionLarge.Images.SetKeyName(8, "chart32.png");
            // 
            // ribbonPageApp
            // 
            this.ribbonPageApp.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupFile,
            this.ribbonPageGroupCondensate,
            this.ribbonPageGroupAdmin,
            this.ribbonPageGroupExit});
            this.ribbonPageApp.Name = "ribbonPageApp";
            // 
            // ribbonPageGroupFile
            // 
            this.ribbonPageGroupFile.ImageOptions.ImageIndex = 1;
            this.ribbonPageGroupFile.ItemLinks.Add(this.iPassword);
            this.ribbonPageGroupFile.ItemLinks.Add(this.iFileAbout);
            this.ribbonPageGroupFile.Name = "ribbonPageGroupFile";
            this.ribbonPageGroupFile.ShowCaptionButton = false;
            toolTipItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipItem1.Appearance.Options.UseImage = true;
            toolTipItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipTitleItem1);
            this.ribbonPageGroupFile.SuperTip = superToolTip1;
            this.ribbonPageGroupFile.Text = "Hệ thống";
            // 
            // ribbonPageGroupCondensate
            // 
            this.ribbonPageGroupCondensate.ImageOptions.ImageIndex = 1;
            this.ribbonPageGroupCondensate.ItemLinks.Add(this.iCondensateImport);
            this.ribbonPageGroupCondensate.ItemLinks.Add(this.iCondensateReportMonthly);
            this.ribbonPageGroupCondensate.ItemLinks.Add(this.iCondensateReportChart);
            this.ribbonPageGroupCondensate.Name = "ribbonPageGroupCondensate";
            this.ribbonPageGroupCondensate.ShowCaptionButton = false;
            this.ribbonPageGroupCondensate.Text = "Condensate";
            // 
            // ribbonPageGroupAdmin
            // 
            this.ribbonPageGroupAdmin.ItemLinks.Add(this.iAdminUser);
            this.ribbonPageGroupAdmin.ItemLinks.Add(this.iAdminSettings);
            this.ribbonPageGroupAdmin.ItemLinks.Add(this.iAdminWells);
            this.ribbonPageGroupAdmin.Name = "ribbonPageGroupAdmin";
            this.ribbonPageGroupAdmin.ShowCaptionButton = false;
            this.ribbonPageGroupAdmin.Text = "Quản trị";
            // 
            // ribbonPageGroupExit
            // 
            this.ribbonPageGroupExit.AllowTextClipping = false;
            this.ribbonPageGroupExit.ImageOptions.ImageIndex = 22;
            this.ribbonPageGroupExit.ItemLinks.Add(this.iExit);
            this.ribbonPageGroupExit.Name = "ribbonPageGroupExit";
            this.ribbonPageGroupExit.ShowCaptionButton = false;
            // 
            // repositoryItemTrackBar1
            // 
            this.repositoryItemTrackBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemTrackBar1.LabelAppearance.Options.UseTextOptions = true;
            this.repositoryItemTrackBar1.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTrackBar1.Maximum = 1000;
            this.repositoryItemTrackBar1.Name = "repositoryItemTrackBar1";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 565);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControlMain;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(953, 37);
            // 
            // pccBottom
            // 
            this.pccBottom.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pccBottom.Appearance.Options.UseBackColor = true;
            this.pccBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pccBottom.Controls.Add(this.sbExit);
            this.pccBottom.Location = new System.Drawing.Point(688, 602);
            this.pccBottom.Name = "pccBottom";
            this.pccBottom.Ribbon = this.ribbonControlMain;
            this.pccBottom.Size = new System.Drawing.Size(138, 32);
            this.pccBottom.TabIndex = 6;
            this.pccBottom.Visible = false;
            // 
            // sbExit
            // 
            this.sbExit.AllowFocus = false;
            this.sbExit.ImageOptions.ImageIndex = 13;
            this.sbExit.ImageOptions.ImageList = this.imageCollectionSmall;
            this.sbExit.Location = new System.Drawing.Point(4, 3);
            this.sbExit.Name = "sbExit";
            this.sbExit.Size = new System.Drawing.Size(129, 24);
            this.sbExit.TabIndex = 0;
            this.sbExit.Text = "E&xit Application";
            // 
            // pcAppMenuFileLabels
            // 
            this.pcAppMenuFileLabels.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pcAppMenuFileLabels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcAppMenuFileLabels.Location = new System.Drawing.Point(10, 19);
            this.pcAppMenuFileLabels.Name = "pcAppMenuFileLabels";
            this.pcAppMenuFileLabels.Size = new System.Drawing.Size(300, 143);
            this.pcAppMenuFileLabels.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl1.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl1.LineVisible = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(300, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Recent Documents:";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(10, 162);
            this.panelControl1.TabIndex = 1;
            // 
            // backstageViewClientControl7
            // 
            this.backstageViewClientControl7.Location = new System.Drawing.Point(0, 0);
            this.backstageViewClientControl7.Name = "backstageViewClientControl7";
            this.backstageViewClientControl7.Size = new System.Drawing.Size(150, 150);
            this.backstageViewClientControl7.TabIndex = 6;
            // 
            // backstageViewClientControl1
            // 
            this.backstageViewClientControl1.Location = new System.Drawing.Point(0, 0);
            this.backstageViewClientControl1.Name = "backstageViewClientControl1";
            this.backstageViewClientControl1.Size = new System.Drawing.Size(150, 150);
            this.backstageViewClientControl1.TabIndex = 0;
            // 
            // backstageViewClientControl3
            // 
            this.backstageViewClientControl3.Location = new System.Drawing.Point(0, 0);
            this.backstageViewClientControl3.Name = "backstageViewClientControl3";
            this.backstageViewClientControl3.Size = new System.Drawing.Size(150, 150);
            this.backstageViewClientControl3.TabIndex = 2;
            // 
            // iCondensateReportChart
            // 
            this.iCondensateReportChart.Caption = "Số liệu khai thác theo giếng";
            this.iCondensateReportChart.Id = 255;
            this.iCondensateReportChart.ImageOptions.ImageIndex = 8;
            this.iCondensateReportChart.ImageOptions.LargeImageIndex = 8;
            this.iCondensateReportChart.Name = "iCondensateReportChart";
            this.iCondensateReportChart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iCondensateReportChart_ItemClick);
            // 
            // frmMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 16);
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Center;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.ClientSize = new System.Drawing.Size(953, 602);
            this.Controls.Add(this.pccBottom);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ribbonControlMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Ribbon = this.ribbonControlMain;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar1;
            this.Text = "VSP Condensate Production Management System - Hệ thống quản lý dữ liệu khai thác " +
    "condensate tháng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MdiChildActivate += new System.EventHandler(this.frmMain_MdiChildActivate);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riicStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccBottom)).EndInit();
            this.pccBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcAppMenuFileLabels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.BarButtonItem iClose;
        private DevExpress.XtraBars.BarButtonItem iSave;
        private DevExpress.XtraBars.BarButtonItem iCondensateImport;
        private DevExpress.XtraBars.BarButtonItem iSaveAs;
        private DevExpress.XtraBars.BarButtonItem iPassword;
        private DevExpress.XtraBars.BarButtonItem iExit;
        private DevExpress.XtraBars.BarButtonItem iPrint;
        private DevExpress.XtraBars.BarButtonItem iClear;
        private DevExpress.XtraBars.BarButtonItem iPaste;
        private DevExpress.XtraBars.BarButtonItem iFind;
        private DevExpress.XtraBars.BarButtonItem iCut;
        private DevExpress.XtraBars.BarButtonItem iCopy;
        private DevExpress.XtraBars.BarButtonItem iUndo;
        private DevExpress.XtraBars.BarButtonItem iReplace;
        private DevExpress.XtraBars.BarButtonItem iSelectAll;
        private DevExpress.XtraBars.BarButtonItem iAdminSettings;
        private DevExpress.XtraBars.BarButtonItem iAlignRight;
        private DevExpress.XtraBars.BarButtonItem iCenter;
        private DevExpress.XtraBars.BarButtonItem iCondensateReportMonthly;
        private DevExpress.XtraBars.BarButtonItem iAlignLeft;
        private DevExpress.XtraBars.BarButtonItem iAdminWells;
        private DevExpress.XtraBars.BarButtonItem iFont;
        private DevExpress.XtraBars.BarButtonItem iBullets;
        private DevExpress.XtraBars.BarButtonItem iProtected;
        private DevExpress.XtraBars.BarButtonItem iFontColor;
        private DevExpress.XtraBars.BarButtonItem iWeb;
        private DevExpress.XtraBars.BarButtonItem siPosition;
        private DevExpress.XtraBars.BarButtonItem siModified;
        private DevExpress.XtraBars.BarStaticItem siDocName;
        private DevExpress.XtraBars.PopupControlContainer popupControlContainer1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControlMain;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageApp;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupFile;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCondensate;
        private DevExpress.Utils.ImageCollection imageCollectionLarge;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarButtonGroup bgAlign;
        private DevExpress.XtraBars.BarButtonGroup bgFontStyle;
        private DevExpress.XtraBars.BarButtonGroup bgFont;
        private DevExpress.XtraBars.BarButtonGroup bgBullets;
        private DevExpress.XtraBars.BarSubItem sbiSave;
        private DevExpress.XtraBars.BarSubItem idAbout;
        private DevExpress.XtraBars.BarSubItem sbiFind;
        private DevExpress.XtraBars.BarButtonItem iPasteSpecial;
        private DevExpress.XtraBars.BarButtonItem iNew;
        private DevExpress.XtraBars.BarLargeButtonItem iAdminUser;
        private DevExpress.XtraBars.BarButtonItem iTemplate;
        private DevExpress.XtraBars.BarButtonItem iPaintStyle;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiSkins;
        private DevExpress.XtraBars.BarEditItem beiFontSize;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAdmin;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiFont;
        private DevExpress.XtraBars.BarButtonItem bbiFontColorPopup;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiFontColor;
        private DevExpress.XtraBars.BarButtonItem iAbout;
        private DevExpress.Utils.ImageCollection imageCollectionSmall;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pcAppMenuFileLabels;
        private BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private BarEditItem biStyle;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox riicStyle;
        private PopupControlContainer pccBottom;
        private DevExpress.XtraEditors.SimpleButton sbExit;
        private BarButtonGroup editButtonGroup;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl1;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl3;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl7;
        private BarEditItem beScheme;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private BarToggleSwitchItem barToggleSwitchItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar repositoryItemTrackBar1;
        private BarButtonItem bbColorMix;
        private System.ComponentModel.IContainer components;
        private BarButtonGroup barButtonGroup1;
        private BarButtonItem iFileAbout;
        private BarButtonItem iCondensateReportChart;
    }
}
