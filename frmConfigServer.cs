using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmConfigServer : Form
    {
        public frmConfigServer()
        {
            InitializeComponent();
        }

        private void frmConfigServer_Load(object sender, EventArgs e)
        {
            string path = Directory.GetCurrentDirectory() + "\\config.dat";
            if (!File.Exists(path))
            {
                MessageBox.Show("File:\t" + path + " is not found\n Application closes");
                this.Close();
            }
            StreamReader sr = new StreamReader(path, System.Text.Encoding.UTF8);
            txtServerName.Text = sr.ReadLine();
            txtDatabaseName.Text = sr.ReadLine();
            txtUserName.Text = sr.ReadLine();
            txtPassword.Text = sr.ReadLine();
            sr.Close();
            sr.Dispose();
        }
        clsConnection connection = new clsConnection();
        private void btnConnect_Click(object sender, EventArgs e)
        {
            connection = new clsConnection();
            try
            {
                if (!connection.ConnectToSQLServer(this.txtServerName.Text, this.txtDatabaseName.Text, this.txtUserName.Text, this.txtPassword.Text))
                {
                    MessageBox.Show("Lỗi truy cập cơ sở dữ liệu!");
                    this.txtServerName.Focus();
                    return;
                }
                else
                {
                    MessageBox.Show("Kết nối thành công, khởi động lại chương trình để sử dụng thiết lập mới!");
                    WriteToConfigFile();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void WriteToConfigFile()
        {
            StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + "\\config.dat");
            sw.WriteLine(txtServerName.Text);
            sw.WriteLine(txtDatabaseName.Text);
            sw.WriteLine(txtUserName.Text);
            sw.WriteLine(txtPassword.Text);
            sw.Close();
            sw.Dispose();
        }
    
    }
}