﻿using DevExpress.XtraCharts;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Spire.Xls;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmWellData : Form
    {
        clsDatabases database = new clsDatabases();
        clsControls control = new clsControls();
        DataTable rowsTable = new DataTable();
        WellDataSettingDisplay setting = new WellDataSettingDisplay();
        SecondaryAxisY wcAxisY;
        SecondaryAxisY waterAxisY;
        SecondaryAxisY gasAxisY;
        SecondaryAxisY gorAxisY;
        private char[] delimiters = new char[] { ',' };
        string strUwi = null;
        string wellNumber = null;
        string platform = null;
        string field = null;
        public frmWellData()
        {
            InitializeComponent();
        }

        private void frmWellData_Load(object sender, EventArgs e)
        {
            CreateColumns(treeList1);
            LoadToTreeList(treeList1);

            ShowCheckOrNotCheckListBox();
            
            // Create secondary axe, and add them to the chart's Diagram.
            wcAxisY = new SecondaryAxisY("wc Y-Axis");
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY.Add(wcAxisY);
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY[0].Title.Text = "Độ ngập nước (%)";
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY[0].Title.Visible = true;

            waterAxisY = new SecondaryAxisY("water Y-Axis");
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY.Add(waterAxisY);
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY[1].Title.Text = "Nước (m3)";
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY[1].Title.Visible = true;

            gasAxisY = new SecondaryAxisY("gas Y-Axis");
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY.Add(gasAxisY);
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY[2].Title.Text = "Khí (m3)";
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY[2].Title.Visible = true;

            gorAxisY = new SecondaryAxisY("gor Y-Axis");
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY.Add(gorAxisY);
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY[3].Title.Text = "Hệ số khí";
            ((XYDiagram)chartControl1.Diagram).SecondaryAxesY[3].Title.Visible = true;
        }

        private void CreateColumns(TreeList tl)
        {
            tl.BeginUpdate();
            tl.Columns.Add();
            tl.Columns[0].Caption = "";
            tl.Columns[0].VisibleIndex = 0;
            tl.EndUpdate();
        }

        private void LoadToTreeList(TreeList tl)
        {
            tl.Nodes.Clear();
            tl.BeginUnboundLoad();
            TreeListNode parentForRootNodes = null;
            DataTable dtField = database.FillDataTable("SELECT FIELD_ID, FIELD_NAME FROM VSP_FIELD_HDR ORDER BY ORDER_NUM",
                CommandType.Text);
            foreach (DataRow drField in dtField.Rows)
            {
                string field_id = drField[0].ToString();
                string field_name = drField[1].ToString();
                TreeListNode nodeField = tl.AppendNode(new object[] { field_name }, parentForRootNodes);
                DataTable dtPlatform = database.FillDataTable("SELECT PLATFORM FROM VSP_PLATFORM WHERE FIELD_ID = N'"
                    + field_id + "' ORDER BY ORDER_NUM", CommandType.Text);
                foreach (DataRow drPlatform in dtPlatform.Rows)
                {
                    string platform = drPlatform[0].ToString();
                    TreeListNode nodePlatform = tl.AppendNode(new object[] { platform }, nodeField);
                    DataTable dtWell = database.FillDataTable("SELECT DISTINCT UWI FROM VSP_CONDENSATE WHERE PLATFORM = N'"
                        + platform + "' ORDER BY UWI", CommandType.Text);
                    foreach (DataRow drWell in dtWell.Rows)
                    {
                        string uwi = drWell[0].ToString();
                        TreeListNode nodeWell = tl.AppendNode(new object[] { uwi }, nodePlatform);
                    }
                }
            }
            tl.EndUnboundLoad();
            tl.ExpandAll();
        }

        private void ShowOrHideColumnInGridView()
        {
            if (strUwi == null) return;
            //hien thi cac cot trong datagridview
            setting = new WellDataSettingDisplay();
            string ss = setting.Attributes;
            string[] sArray = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            for(int i = 0; i < rowsTable.Columns.Count; i++)
            {
                bool bExist = false;
                string colName = rowsTable.Columns[i].ColumnName;
                foreach (string s in sArray)
                {
                    if (colName == s)
                    {
                        bExist = true;
                        gridView1.Columns[colName].Visible = true;
                    }
                }
                if (!bExist) 
                {
                    gridView1.Columns[i].Visible = false;
                }
            }
            
        }

        private void ShowCheckOrNotCheckListBox()
        {
            if (setting.CondensateShow) chkCondensate.Checked = true;
            if (setting.CondensateYearShow) chkCondensateYear.Checked = true;
            if (setting.CondensateAccShow) chkCondensateAcc.Checked = true;
            if (setting.WaterShow) chkWater.Checked = true;
            if (setting.WaterYearShow) chkWaterYear.Checked = true;
            if (setting.WaterAccShow) chkWaterAcc.Checked = true;
            if (setting.GasShow) chkGas.Checked = true;
            if (setting.GasYearShow) chkGasYear.Checked = true;
            if (setting.GasAccShow) chkGasAcc.Checked = true;
            if (setting.WaterCutShow) chkWaterCut.Checked = true;
            if (setting.Gor) chkGor.Checked = true;
        }


        private void GetDataToTable(string strUwi)
        {
            string sqlText = "SELECT * FROM WELL_CONDENSATE_MONTH WHERE UWI = '" + strUwi + "' ORDER BY MONTH";
            rowsTable = database.FillDataTable(sqlText, CommandType.Text);
            gridControl1.DataSource = rowsTable;
        }

        private void GetMaxAndMinDateToTextBox(string uwi)
        {
            //max date
            string sqlText = "SELECT CONVERT(VARCHAR, MAX(MONTH), 103) FROM WELL_CONDENSATE_MONTH WHERE UWI = '" + strUwi + "'";
            foreach (DataRow dr in database.FillDataTable(sqlText, CommandType.Text).Rows)
            {
                txtToDate.Text = dr[0].ToString();
            }

            //min date
            sqlText = "SELECT CONVERT(VARCHAR, MIN(MONTH), 103) FROM WELL_CONDENSATE_MONTH WHERE UWI = '" + strUwi + "'";
            foreach (DataRow dr in database.FillDataTable(sqlText, CommandType.Text).Rows)
            {
                txtFromDate.Text = dr[0].ToString();
            }
        }

        private void GetWellInformation(string strUwi)
        {
            string sqlText = "SELECT WH.WELL_NUMBER, WH.PLATFORM, F.FIELD_NAME_R "
                    + "FROM VSP_WELL_HDR WH, VSP_FIELD_HDR F WHERE WH.FIELD_ID = F.FIELD_ID AND WH.UWI = '" + strUwi + "'";
            DataTable table = new DataTable();
            table = database.FillDataTable(sqlText, CommandType.Text);
            foreach (DataRow dr in table.Rows)
            {
                wellNumber = dr[0].ToString();
                if (wellNumber.Contains("S1"))
                    wellNumber = wellNumber.Substring(0, wellNumber.Length - 2) + "Б";
                else if (wellNumber.Contains("BT"))
                    wellNumber = wellNumber.Substring(0, wellNumber.Length - 2) + "БТ";
                platform = dr[1].ToString();
                field = dr[2].ToString();
            }
        }

        private void ExportToExcelFile(string strUwi, string str_FILE_NAME)
        {
            try
            {
                Spire.Xls.Workbook SpireWorkBook = new Spire.Xls.Workbook();
                SpireWorkBook.SaveToFile(str_FILE_NAME);
                Spire.Xls.Worksheet worksheet = SpireWorkBook.Worksheets[0];

                toolStripProgressBar1.Value = 0;
                toolStripProgressBar1.Maximum = rowsTable.Rows.Count;

                //Set column width, align, text format and font
                worksheet.SetColumnWidth(1, 6.57);
                worksheet.SetColumnWidth(2, 3.29);
                worksheet.SetColumnWidth(3, 4.71);
                worksheet.SetColumnWidth(4, 4.71);
                worksheet.SetColumnWidth(5, 5.71);
                worksheet.SetColumnWidth(6, 4.86);
                worksheet.SetColumnWidth(7, 5.29);
                worksheet.SetColumnWidth(8, 5.86);
                worksheet.SetColumnWidth(9, 6);
                worksheet.SetColumnWidth(10, 7);
                worksheet.SetColumnWidth(11, 6.43);
                worksheet.SetColumnWidth(12, 4.00);
                worksheet.SetColumnWidth(13, 4.86);
                worksheet.SetColumnWidth(14, 7.14);
                worksheet.SetColumnWidth(15, 6.14);
                worksheet.SetColumnWidth(16, 7);
                worksheet.SetColumnWidth(17, 6.86);
                worksheet.SetColumnWidth(18, 7);
                worksheet.SetColumnWidth(19, 7.71);
                worksheet.SetColumnWidth(20, 15.00);

                //Set header
                worksheet.Range[1, 1, 1, 20].Merge();
                worksheet.SetText(1, 1, "СВОДНАЯ ТАБЛИЦА РАБОТЫ СКВАЖИНЫ " + wellNumber);
                worksheet.SetRowHeight(1, 30);
                worksheet.Range[1, 1, 1, 20].Style.Font.FontName = "Arial";
                worksheet.Range[1, 1, 1, 20].Style.Font.Size = 12;
                worksheet.Range[1, 1, 1, 20].Style.Font.IsBold = true;
                worksheet.Range[1, 1, 1, 20].Style.HorizontalAlignment = HorizontalAlignType.Center;
                worksheet.Range[1, 1, 1, 20].Style.VerticalAlignment = VerticalAlignType.Center;


                //Set Column header
                worksheet.Range[2, 1, 3, 1].Merge();
                worksheet.SetText(2, 1, "Год \r\nМесяц");

                worksheet.Range[2, 2, 3, 2].Merge();
                worksheet.SetText(2, 2, "Пласт");

                worksheet.Range[2, 3, 3, 3].Merge();
                worksheet.SetText(2, 3, "Споб экспл.");

                worksheet.Range[2, 4, 3, 4].Merge();
                worksheet.SetText(2, 4, "Средний газовый фактор");

                worksheet.Range[2, 5, 2, 5].Merge();
                worksheet.SetText(2, 5, "Буферное");

                worksheet.Range[2, 6, 3, 6].Merge();
                worksheet.SetText(2, 6, "Способ эксплуатации");

                worksheet.Range[2, 7, 3, 7].Merge();
                worksheet.SetText(2, 7, "Число дней");

                worksheet.Range[2, 8, 2, 11].Merge();
                worksheet.SetText(2, 8, "Дoбыча за месяц");
                worksheet.SetText(3, 8, "Конденсата\r\nт.");
                worksheet.SetText(3, 9, "воды\r\nм3");
                worksheet.SetText(3, 10, "газа\r\nм3");
                worksheet.SetText(3, 11, "всего жидкости\r\nт.");

                worksheet.Range[2, 12, 3, 12].Merge();
                worksheet.SetText(2, 12, "Проц. Воды");

                worksheet.Range[2, 13, 3, 13].Merge();
                worksheet.SetText(2, 13, "Среднесуточный дебит нефти");

                worksheet.Range[2, 14, 2, 16].Merge();
                worksheet.SetText(2 , 14, "Дoбыча с начала  год.");
                worksheet.SetText(3, 14, "Конденсата\r\nт.");
                worksheet.SetText(3, 15, "воды\r\nм3");
                worksheet.SetText(3, 16, "газа\r\nм3");

                worksheet.Range[2, 17, 2, 19].Merge();
                worksheet.SetText(2, 17, "Дoбыча с начала  експ. данного пласта");
                worksheet.SetText(3, 17, "Конденсата\r\nт.");
                worksheet.SetText(3, 18, "воды\r\nм3");
                worksheet.SetText(3, 19, "газа\r\nм3");

                worksheet.Range[2, 20, 3, 20].Merge();
                worksheet.SetText(2, 20, "Примечание");

                worksheet.Range[2, 1, 3, 20].Style.WrapText = true;
                worksheet.Range[2, 1, 3, 20].Style.Font.FontName = "Arial";
                worksheet.Range[2, 1, 3, 20].Style.Font.Size = 8;
                worksheet.Range[2, 1, 3, 20].Style.HorizontalAlignment = HorizontalAlignType.Center;
                worksheet.Range[2, 1, 3, 20].Style.VerticalAlignment = VerticalAlignType.Center;
                worksheet.Range[2, 1, 3, 20].Borders.LineStyle = LineStyleType.Thin;
                worksheet.Range[2, 1, 3, 20].Borders[BordersLineType.DiagonalDown].LineStyle = LineStyleType.None;
                worksheet.Range[2, 1, 3, 20].Borders[BordersLineType.DiagonalUp].LineStyle = LineStyleType.None;
                worksheet.Range[2, 1, 3, 20].Borders.Color = Color.Black;



                ////Fill data
                int row_idx = 4; //start write to exel file at row number 4
                string cur_year;
                string old_year = "";
                foreach (DataRow dr in rowsTable.Rows)
                {
                    cur_year = dr[2].ToString();
                    if ((cur_year != old_year) || (old_year == ""))
                    {
                        worksheet.Range[row_idx, 1, row_idx, 20].Merge();
                        worksheet.Range[row_idx, 1, row_idx, 20].Style.Font.Size = 10;
                        worksheet.Range[row_idx, 1, row_idx, 20].Style.Font.FontName = "Arial";
                        worksheet.Range[row_idx, 1, row_idx, 20].Style.Font.IsBold = true;
                        worksheet.SetText(row_idx, 1, dr[2].ToString() + "Г.");
                        row_idx++;
                    }
                    worksheet.SetCellValue(row_idx, 1, dr[1].ToString() + "." + dr[2].ToString());
                    worksheet.SetCellValue(row_idx, 2, dr[5].ToString());
                    worksheet.SetCellValue(row_idx, 3, dr[4].ToString());
                    worksheet.SetCellValue(row_idx, 4, dr[6].ToString());
                    worksheet.SetCellValue(row_idx, 5, dr[7].ToString());
                    string choke_size = dr[8].ToString();
                    if (choke_size.Contains("500"))
                        choke_size = choke_size.Replace("500", "бш");
                    worksheet.SetCellValue(row_idx, 6, choke_size);
                    worksheet.SetCellValue(row_idx, 7, dr[20].ToString()); //prod day
                    worksheet.SetCellValue(row_idx, 8, dr[9].ToString()); //oil
                    worksheet.SetCellValue(row_idx, 9, dr[12].ToString()); // water
                    worksheet.SetCellValue(row_idx, 10, dr[15].ToString()); //gas
                    worksheet.SetCellValue(row_idx, 11, dr[18].ToString()); //fluid
                    worksheet.SetCellValue(row_idx, 12, dr[19].ToString()); //water cut
                    worksheet.SetCellValue(row_idx, 13, dr[21].ToString()); // oil rate
                    worksheet.SetCellValue(row_idx, 14, dr[10].ToString()); // oil year
                    worksheet.SetCellValue(row_idx, 15, dr[13].ToString()); // water year
                    worksheet.SetCellValue(row_idx, 16, dr[16].ToString()); // gas year
                    worksheet.SetCellValue(row_idx, 17, dr[11].ToString()); // oil acc
                    worksheet.SetCellValue(row_idx, 18, dr[14].ToString()); // water acc
                    worksheet.SetCellValue(row_idx, 19, dr[17].ToString()); // gas acc
                    worksheet.SetCellValue(row_idx, 20, dr[22].ToString()); //remarks
                    worksheet.Range[row_idx, 1, row_idx, 20].Style.Font.FontName = "Arial";
                    worksheet.Range[row_idx, 1, row_idx, 20].Style.Font.Size = 8;
                    row_idx++;

                    old_year = dr[2].ToString();
                    toolStripProgressBar1.Value++;
                }

                //save picture to excel
                Image image = null;
                using (MemoryStream s = new MemoryStream())
                {
                    chartControl1.ExportToImage(s, System.Drawing.Imaging.ImageFormat.Png);
                    image = Image.FromStream(s);
                }

                System.Windows.Forms.Clipboard.SetDataObject(image, true);
                worksheet.Pictures.Add(1, 1, image);

                SpireWorkBook.Save();
                MessageBox.Show("Hoàn thành");
                toolStripProgressBar1.Value = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripSetting_Click(object sender, EventArgs e)
        {
            frmWellDataSettings frm = new frmWellDataSettings();
            frm.ShowDialog();
            if (frm.bClosed)
            {
                ShowOrHideColumnInGridView();
            }
        }

        private void DrawGraphic()
        {
            if (strUwi == null) return;
            chartControl1.Series.Clear();
            chartControl1.Titles.Clear();

            if ((chkWater.Checked) || (chkWaterAcc.Checked) || (chkWaterYear.Checked))
                waterAxisY.Visible = true;
            else waterAxisY.Visible = false;

            if (chkWaterCut.Checked) wcAxisY.Visible = true;
            else wcAxisY.Visible = false;

            if (chkGor.Checked) gorAxisY.Visible = true;
            else gorAxisY.Visible = false;

            if ((chkGas.Checked) || (chkGasAcc.Checked) || (chkGasYear.Checked))
                gasAxisY.Visible = true;
            else gasAxisY.Visible = false;

            if (chkCondensate.Checked)
            {
                Series dsCondensate = new Series("CONDENSATE", ViewType.Line);
                dsCondensate.DataSource = rowsTable;
                dsCondensate.ArgumentScaleType = ScaleType.DateTime;
                dsCondensate.ArgumentDataMember = "MONTH";
                dsCondensate.ValueScaleType = ScaleType.Numerical;
                dsCondensate.ValueDataMembers.AddRange("CONDENSATE");
                dsCondensate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsCondensate);
            }

            if (chkCondensateYear.Checked)
            {
                Series dsCondensate = new Series("CONDENSATE_YEAR", ViewType.Line);
                dsCondensate.DataSource = rowsTable;
                dsCondensate.ArgumentScaleType = ScaleType.DateTime;
                dsCondensate.ArgumentDataMember = "MONTH";
                dsCondensate.ValueScaleType = ScaleType.Numerical;
                dsCondensate.ValueDataMembers.AddRange("CONDENSATE_YEAR");
                dsCondensate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsCondensate);
            }
            if (chkCondensateAcc.Checked)
            {
                Series dsCondensate = new Series("CONDENSATE_ACC", ViewType.Line);
                dsCondensate.DataSource = rowsTable;
                dsCondensate.ArgumentScaleType = ScaleType.DateTime;
                dsCondensate.ArgumentDataMember = "MONTH";
                dsCondensate.ValueScaleType = ScaleType.Numerical;
                dsCondensate.ValueDataMembers.AddRange("CONDENSATE_ACC");
                dsCondensate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsCondensate);
            }

            if (chkWater.Checked)
            {
                Series dsWater = new Series("WATER", ViewType.Line);
                dsWater.DataSource = rowsTable;
                ((LineSeriesView)(dsWater.View)).AxisY = waterAxisY;
                dsWater.ArgumentScaleType = ScaleType.DateTime;
                dsWater.ArgumentDataMember = "MONTH";
                dsWater.ValueScaleType = ScaleType.Numerical;
                dsWater.ValueDataMembers.AddRange("WATER");
                dsWater.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsWater);
            }

            if (chkWaterYear.Checked)
            {
                Series dsWater = new Series("WATER_YEAR", ViewType.Line);
                dsWater.DataSource = rowsTable;
                ((LineSeriesView)(dsWater.View)).AxisY = waterAxisY;
                dsWater.ArgumentScaleType = ScaleType.DateTime;
                dsWater.ArgumentDataMember = "MONTH";
                dsWater.ValueScaleType = ScaleType.Numerical;
                dsWater.ValueDataMembers.AddRange("WATER_YEAR");
                dsWater.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsWater);
            }

            if (chkWaterAcc.Checked)
            {
                Series dsWater = new Series("WATER_ACC", ViewType.Line);
                dsWater.DataSource = rowsTable;
                ((LineSeriesView)(dsWater.View)).AxisY = waterAxisY;
                dsWater.ArgumentScaleType = ScaleType.DateTime;
                dsWater.ArgumentDataMember = "MONTH";
                dsWater.ValueScaleType = ScaleType.Numerical;
                dsWater.ValueDataMembers.AddRange("WATER_ACC");
                dsWater.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsWater);
            }

            if (chkWaterCut.Checked)
            {
                Series dsWater_cut = new Series("WATER_CUT", ViewType.Line);
                dsWater_cut.DataSource = rowsTable;
                ((LineSeriesView)(dsWater_cut.View)).AxisY = wcAxisY;
                dsWater_cut.ArgumentScaleType = ScaleType.DateTime;
                dsWater_cut.ArgumentDataMember = "MONTH";
                dsWater_cut.ValueScaleType = ScaleType.Numerical;
                dsWater_cut.ValueDataMembers.AddRange("WATER_CUT");
                dsWater_cut.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsWater_cut);
            }

            if (chkGor.Checked)
            {
                Series dsGor = new Series("GOR", ViewType.Line);
                dsGor.DataSource = rowsTable;
                ((LineSeriesView)(dsGor.View)).AxisY = gorAxisY;
                dsGor.ArgumentScaleType = ScaleType.DateTime;
                dsGor.ArgumentDataMember = "MONTH";
                dsGor.ValueScaleType = ScaleType.Numerical;
                dsGor.ValueDataMembers.AddRange("GOR");
                dsGor.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsGor);
            }

            if (chkGas.Checked)
            {
                Series dsGas = new Series("GAS", ViewType.Line);
                dsGas.DataSource = rowsTable;
                ((LineSeriesView)(dsGas.View)).AxisY = gasAxisY;
                dsGas.ArgumentScaleType = ScaleType.DateTime;
                dsGas.ArgumentDataMember = "MONTH";
                dsGas.ValueScaleType = ScaleType.Numerical;
                dsGas.ValueDataMembers.AddRange("GAS");
                dsGas.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsGas);
            }

            if (chkGasYear.Checked)
            {
                Series dsGas = new Series("GAS_YEAR", ViewType.Line);
                dsGas.DataSource = rowsTable;
                ((LineSeriesView)(dsGas.View)).AxisY = gasAxisY;
                dsGas.ArgumentScaleType = ScaleType.DateTime;
                dsGas.ArgumentDataMember = "MONTH";
                dsGas.ValueScaleType = ScaleType.Numerical;
                dsGas.ValueDataMembers.AddRange("GAS_YEAR");
                dsGas.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsGas);
            }

            if (chkGasAcc.Checked)
            {
                Series dsGas = new Series("GAS_ACC", ViewType.Line);
                dsGas.DataSource = rowsTable;
                ((LineSeriesView)(dsGas.View)).AxisY = gasAxisY;
                dsGas.ArgumentScaleType = ScaleType.DateTime;
                dsGas.ArgumentDataMember = "MONTH";
                dsGas.ValueScaleType = ScaleType.Numerical;
                dsGas.ValueDataMembers.AddRange("GAS_ACC");
                dsGas.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
                chartControl1.Series.Add(dsGas);
            }

            //chart Title
            ChartTitle title = new ChartTitle();
            Font newFont = new Font("Tahoma", 12, FontStyle.Regular);
            title.Text = "ĐỒ THỊ KHAI THÁC CONDENSATE THEO THỜI GIAN \r\n GIẾNG " 
                + wellNumber + " - GIÀN " + platform + " - MỎ " + field;
            title.Dock = ChartTitleDockStyle.Top;
            title.Alignment = StringAlignment.Center;
            title.Font = newFont;
            chartControl1.Titles.Add(title);
        }

        private void FilterData()
        {
            try
            {
                DateTime dateFrom = Convert.ToDateTime(txtFromDate.Text);
                DateTime dateTo = Convert.ToDateTime(txtToDate.Text);
                rowsTable = database.FillDataTable("SELECT * FROM WELL_CONDENSATE_MONTH WHERE UWI = '" + strUwi
                    + "' AND MONTH BETWEEN CONVERT(DATETIME, '" + txtFromDate.Text + "', 103)"
                    + " AND CONVERT(DATETIME, '" + txtToDate.Text + "', 103) ORDER BY MONTH", CommandType.Text);
                ShowOrHideColumnInGridView();
                DrawGraphic();
            }
            catch
            { }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            FilterData();
        }

        private void SaveSettingFile()
        {
            if (chkCondensate.Checked) setting.CondensateShow = true;
            else setting.CondensateShow = false;

            if (chkCondensateYear.Checked) setting.CondensateYearShow = true;
            else setting.CondensateYearShow = false;

            if (chkCondensateAcc.Checked) setting.CondensateAccShow = true;
            else setting.CondensateAccShow = false;

            if (chkWater.Checked) setting.WaterShow = true;
            else setting.WaterShow = false;

            if (chkWaterYear.Checked) setting.WaterYearShow = true;
            else setting.WaterYearShow = false;

            if (chkWaterAcc.Checked) setting.WaterAccShow = true;
            else setting.WaterAccShow = false;

            if (chkGas.Checked) setting.GasShow = true;
            else setting.GasShow = false;

            if (chkGasYear.Checked) setting.GasYearShow = true;
            else setting.GasYearShow = false;

            if (chkGasAcc.Checked) setting.GasAccShow = true;
            else setting.GasAccShow = false;

            if (chkWaterCut.Checked) setting.WaterCutShow = true;
            else setting.WaterCutShow = false;

            if (chkGor.Checked) setting.Gor = true;
            else setting.Gor = false;

            setting.Save();
        }

        private void chkOil_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkOilYear_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkOilAcc_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkWater_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkWaterYear_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkWaterAcc_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkGasYear_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkGasAcc_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkWaterCut_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkGor_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void chkGas_CheckedChanged(object sender, EventArgs e)
        {
            DrawGraphic();
        }

        private void frmWellData_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveSettingFile();
            Session.frmWellData = null;
        }

        private void toolStripExport_Click(object sender, EventArgs e)
        {
            var fileName = "Well_Condensate_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Excel 97-2003 Workbook (*.xls)|*.xls";
            dlg.FileName = fileName;
            dlg.RestoreDirectory = true;
            DialogResult result = dlg.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                if (strUwi == null) return;
                try
                {
                    GetDataToTable(strUwi);
                    ExportToExcelFile(strUwi, dlg.FileName);
                    ExcelDocViewer(dlg.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ExcelDocViewer(string fileName)
        {
            try
            {
                System.Diagnostics.Process.Start(fileName);
            }
            catch { }
        }

        private void toolStripSaveChart_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Filter = "Bitmap files (*.bmp)|*.bmp|Png files (*.png)|*.png|Jpeg files (*.jpeg)|*.jpeg|Gif files (*.gif)|*.gif"  ;
            DialogResult result = saveDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                System.Drawing.Imaging.ImageFormat format = null;
                switch (saveDlg.FilterIndex)
                {
                    case 1: format = System.Drawing.Imaging.ImageFormat.Bmp; break;
                    case 2: format = System.Drawing.Imaging.ImageFormat.Png; break;
                    case 3: format = System.Drawing.Imaging.ImageFormat.Jpeg; break;
                    case 4: format = System.Drawing.Imaging.ImageFormat.Gif; break;
                }
                chartControl1.ExportToImage(saveDlg.FileName, format);
            }
        }

        private void toolStripCopyChart_Click(object sender, EventArgs e)
        {
            Image image = null;
            using (MemoryStream s = new MemoryStream())
            {
                chartControl1.ExportToImage(s, System.Drawing.Imaging.ImageFormat.Png);
                image = Image.FromStream(s);
            }

            System.Windows.Forms.Clipboard.SetDataObject(image, true);
        }


        private void txtToDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                FilterData();
        }

        private void txtFromDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                FilterData();
        }

        private void treeList1_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {
            if (e.Node == null) return;
            if (e.Node.Level == 2)
            {
                string uwi = e.Node.GetDisplayText(0);
                strUwi = uwi;
                //Get well information
                GetWellInformation(strUwi);
                GetDataToTable(strUwi);
                GetMaxAndMinDateToTextBox(strUwi);
                ShowOrHideColumnInGridView();
                DrawGraphic();
            }
        }
    }
}
