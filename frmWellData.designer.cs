﻿namespace TrueTech.VSP.VSPCondensate
{
    partial class frmWellData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWellData));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSaveChart = new System.Windows.Forms.ToolStripButton();
            this.toolStripCopyChart = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkCondensate = new System.Windows.Forms.CheckBox();
            this.chkGor = new System.Windows.Forms.CheckBox();
            this.chkCondensateYear = new System.Windows.Forms.CheckBox();
            this.chkWaterCut = new System.Windows.Forms.CheckBox();
            this.chkCondensateAcc = new System.Windows.Forms.CheckBox();
            this.chkGasAcc = new System.Windows.Forms.CheckBox();
            this.chkWater = new System.Windows.Forms.CheckBox();
            this.chkGasYear = new System.Windows.Forms.CheckBox();
            this.chkWaterYear = new System.Windows.Forms.CheckBox();
            this.chkGas = new System.Windows.Forms.CheckBox();
            this.chkWaterAcc = new System.Windows.Forms.CheckBox();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripSetting = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripLabel();
            this.txtFromDate = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.txtToDate = new System.Windows.Forms.ToolStripTextBox();
            this.btnFilter = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripExport = new System.Windows.Forms.ToolStripButton();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.oleDbConnection1 = new System.Data.OleDb.OleDbConnection();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeList1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1389, 705);
            this.splitContainer1.SplitterDistance = 257;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 5;
            // 
            // treeList1
            // 
            this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList1.FixedLineWidth = 3;
            this.treeList1.Location = new System.Drawing.Point(0, 0);
            this.treeList1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.treeList1.MinWidth = 27;
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.Editable = false;
            this.treeList1.OptionsView.EnableAppearanceEvenRow = true;
            this.treeList1.Size = new System.Drawing.Size(257, 705);
            this.treeList1.TabIndex = 4;
            this.treeList1.TreeLevelWidth = 24;
            this.treeList1.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeList1_FocusedNodeChanged);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.chartControl1);
            this.splitContainer2.Panel1.Controls.Add(this.toolStrip1);
            this.splitContainer2.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.gridControl1);
            this.splitContainer2.Panel2.Controls.Add(this.toolStrip2);
            this.splitContainer2.Size = new System.Drawing.Size(1127, 705);
            this.splitContainer2.SplitterDistance = 391;
            this.splitContainer2.SplitterWidth = 5;
            this.splitContainer2.TabIndex = 105;
            // 
            // chartControl1
            // 
            xyDiagram1.AxisX.AutoScaleBreaks.Enabled = true;
            xyDiagram1.AxisX.DateTimeScaleOptions.AutoGrid = false;
            xyDiagram1.AxisX.DateTimeScaleOptions.GridAlignment = DevExpress.XtraCharts.DateTimeGridAlignment.Month;
            xyDiagram1.AxisX.DateTimeScaleOptions.MeasureUnit = DevExpress.XtraCharts.DateTimeMeasureUnit.Month;
            xyDiagram1.AxisX.Label.Angle = 270;
            xyDiagram1.AxisX.MinorCount = 1;
            xyDiagram1.AxisX.Tickmarks.MinorVisible = false;
            xyDiagram1.AxisX.Title.Text = "Tháng";
            xyDiagram1.AxisX.Title.Visibility = DevExpress.Utils.DefaultBoolean.True;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Title.Text = "Condensate (tấn)";
            xyDiagram1.AxisY.Title.Visibility = DevExpress.Utils.DefaultBoolean.True;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.EnableAxisXScrolling = true;
            this.chartControl1.Diagram = xyDiagram1;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl1.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Location = new System.Drawing.Point(0, 27);
            this.chartControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chartControl1.Name = "chartControl1";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.Label = pointSeriesLabel1;
            series1.Name = "Series 1";
            series1.View = lineSeriesView1;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            pointSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl1.SeriesTemplate.Label = pointSeriesLabel2;
            this.chartControl1.SeriesTemplate.SeriesColorizer = null;
            this.chartControl1.SeriesTemplate.View = lineSeriesView2;
            this.chartControl1.Size = new System.Drawing.Size(907, 364);
            this.chartControl1.SmallChartText.Text = "Increase the chart\'s size,\r\nto view its layout.\r\n    ";
            this.chartControl1.TabIndex = 16;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSaveChart,
            this.toolStripCopyChart});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(907, 27);
            this.toolStrip1.TabIndex = 15;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSaveChart
            // 
            this.toolStripSaveChart.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSaveChart.Image")));
            this.toolStripSaveChart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSaveChart.Name = "toolStripSaveChart";
            this.toolStripSaveChart.Size = new System.Drawing.Size(57, 24);
            this.toolStripSaveChart.Text = "Lưu";
            this.toolStripSaveChart.Click += new System.EventHandler(this.toolStripSaveChart_Click);
            // 
            // toolStripCopyChart
            // 
            this.toolStripCopyChart.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCopyChart.Image")));
            this.toolStripCopyChart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCopyChart.Name = "toolStripCopyChart";
            this.toolStripCopyChart.Size = new System.Drawing.Size(94, 24);
            this.toolStripCopyChart.Text = "Sao chép";
            this.toolStripCopyChart.Click += new System.EventHandler(this.toolStripCopyChart_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkCondensate);
            this.panel2.Controls.Add(this.chkGor);
            this.panel2.Controls.Add(this.chkCondensateYear);
            this.panel2.Controls.Add(this.chkWaterCut);
            this.panel2.Controls.Add(this.chkCondensateAcc);
            this.panel2.Controls.Add(this.chkGasAcc);
            this.panel2.Controls.Add(this.chkWater);
            this.panel2.Controls.Add(this.chkGasYear);
            this.panel2.Controls.Add(this.chkWaterYear);
            this.panel2.Controls.Add(this.chkGas);
            this.panel2.Controls.Add(this.chkWaterAcc);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(907, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(220, 391);
            this.panel2.TabIndex = 14;
            // 
            // chkCondensate
            // 
            this.chkCondensate.AutoSize = true;
            this.chkCondensate.Location = new System.Drawing.Point(5, 12);
            this.chkCondensate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCondensate.Name = "chkCondensate";
            this.chkCondensate.Size = new System.Drawing.Size(125, 21);
            this.chkCondensate.TabIndex = 3;
            this.chkCondensate.Text = "CONDENSATE";
            this.chkCondensate.UseVisualStyleBackColor = true;
            this.chkCondensate.CheckedChanged += new System.EventHandler(this.chkOil_CheckedChanged);
            // 
            // chkGor
            // 
            this.chkGor.AutoSize = true;
            this.chkGor.Location = new System.Drawing.Point(5, 295);
            this.chkGor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkGor.Name = "chkGor";
            this.chkGor.Size = new System.Drawing.Size(62, 21);
            this.chkGor.TabIndex = 13;
            this.chkGor.Text = "GOR";
            this.chkGor.UseVisualStyleBackColor = true;
            this.chkGor.CheckedChanged += new System.EventHandler(this.chkGor_CheckedChanged);
            // 
            // chkCondensateYear
            // 
            this.chkCondensateYear.AutoSize = true;
            this.chkCondensateYear.Location = new System.Drawing.Point(5, 41);
            this.chkCondensateYear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCondensateYear.Name = "chkCondensateYear";
            this.chkCondensateYear.Size = new System.Drawing.Size(170, 21);
            this.chkCondensateYear.TabIndex = 4;
            this.chkCondensateYear.Text = "CONDENSATE_YEAR";
            this.chkCondensateYear.UseVisualStyleBackColor = true;
            this.chkCondensateYear.CheckedChanged += new System.EventHandler(this.chkOilYear_CheckedChanged);
            // 
            // chkWaterCut
            // 
            this.chkWaterCut.AutoSize = true;
            this.chkWaterCut.Location = new System.Drawing.Point(5, 267);
            this.chkWaterCut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkWaterCut.Name = "chkWaterCut";
            this.chkWaterCut.Size = new System.Drawing.Size(116, 21);
            this.chkWaterCut.TabIndex = 12;
            this.chkWaterCut.Text = "WATER_CUT";
            this.chkWaterCut.UseVisualStyleBackColor = true;
            this.chkWaterCut.CheckedChanged += new System.EventHandler(this.chkWaterCut_CheckedChanged);
            // 
            // chkCondensateAcc
            // 
            this.chkCondensateAcc.AutoSize = true;
            this.chkCondensateAcc.Location = new System.Drawing.Point(5, 69);
            this.chkCondensateAcc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCondensateAcc.Name = "chkCondensateAcc";
            this.chkCondensateAcc.Size = new System.Drawing.Size(160, 21);
            this.chkCondensateAcc.TabIndex = 5;
            this.chkCondensateAcc.Text = "CONDENSATE_ACC";
            this.chkCondensateAcc.UseVisualStyleBackColor = true;
            this.chkCondensateAcc.CheckedChanged += new System.EventHandler(this.chkOilAcc_CheckedChanged);
            // 
            // chkGasAcc
            // 
            this.chkGasAcc.AutoSize = true;
            this.chkGasAcc.Location = new System.Drawing.Point(5, 239);
            this.chkGasAcc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkGasAcc.Name = "chkGasAcc";
            this.chkGasAcc.Size = new System.Drawing.Size(94, 21);
            this.chkGasAcc.TabIndex = 11;
            this.chkGasAcc.Text = "GAS_ACC";
            this.chkGasAcc.UseVisualStyleBackColor = true;
            this.chkGasAcc.CheckedChanged += new System.EventHandler(this.chkGasAcc_CheckedChanged);
            // 
            // chkWater
            // 
            this.chkWater.AutoSize = true;
            this.chkWater.Location = new System.Drawing.Point(5, 97);
            this.chkWater.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkWater.Name = "chkWater";
            this.chkWater.Size = new System.Drawing.Size(80, 21);
            this.chkWater.TabIndex = 6;
            this.chkWater.Text = "WATER";
            this.chkWater.UseVisualStyleBackColor = true;
            this.chkWater.CheckedChanged += new System.EventHandler(this.chkWater_CheckedChanged);
            // 
            // chkGasYear
            // 
            this.chkGasYear.AutoSize = true;
            this.chkGasYear.Location = new System.Drawing.Point(5, 210);
            this.chkGasYear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkGasYear.Name = "chkGasYear";
            this.chkGasYear.Size = new System.Drawing.Size(104, 21);
            this.chkGasYear.TabIndex = 10;
            this.chkGasYear.Text = "GAS_YEAR";
            this.chkGasYear.UseVisualStyleBackColor = true;
            this.chkGasYear.CheckedChanged += new System.EventHandler(this.chkGasYear_CheckedChanged);
            // 
            // chkWaterYear
            // 
            this.chkWaterYear.AutoSize = true;
            this.chkWaterYear.Location = new System.Drawing.Point(5, 126);
            this.chkWaterYear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkWaterYear.Name = "chkWaterYear";
            this.chkWaterYear.Size = new System.Drawing.Size(125, 21);
            this.chkWaterYear.TabIndex = 7;
            this.chkWaterYear.Text = "WATER_YEAR";
            this.chkWaterYear.UseVisualStyleBackColor = true;
            this.chkWaterYear.CheckedChanged += new System.EventHandler(this.chkWaterYear_CheckedChanged);
            // 
            // chkGas
            // 
            this.chkGas.AutoSize = true;
            this.chkGas.Location = new System.Drawing.Point(5, 182);
            this.chkGas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkGas.Name = "chkGas";
            this.chkGas.Size = new System.Drawing.Size(59, 21);
            this.chkGas.TabIndex = 9;
            this.chkGas.Text = "GAS";
            this.chkGas.UseVisualStyleBackColor = true;
            this.chkGas.CheckedChanged += new System.EventHandler(this.chkGas_CheckedChanged);
            // 
            // chkWaterAcc
            // 
            this.chkWaterAcc.AutoSize = true;
            this.chkWaterAcc.Location = new System.Drawing.Point(5, 154);
            this.chkWaterAcc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkWaterAcc.Name = "chkWaterAcc";
            this.chkWaterAcc.Size = new System.Drawing.Size(115, 21);
            this.chkWaterAcc.TabIndex = 8;
            this.chkWaterAcc.Text = "WATER_ACC";
            this.chkWaterAcc.UseVisualStyleBackColor = true;
            this.chkWaterAcc.CheckedChanged += new System.EventHandler(this.chkWaterAcc_CheckedChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl1.Location = new System.Drawing.Point(0, 33);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1127, 276);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.DetailHeight = 431;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // toolStrip2
            // 
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSetting,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.txtFromDate,
            this.toolStripLabel1,
            this.txtToDate,
            this.btnFilter,
            this.toolStripSeparator2,
            this.toolStripExport,
            this.toolStripProgressBar1});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1127, 33);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripSetting
            // 
            this.toolStripSetting.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSetting.Image")));
            this.toolStripSetting.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSetting.Name = "toolStripSetting";
            this.toolStripSetting.Size = new System.Drawing.Size(91, 30);
            this.toolStripSetting.Text = "Thiết lập";
            this.toolStripSetting.Click += new System.EventHandler(this.toolStripSetting_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 33);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(62, 30);
            this.toolStripTextBox1.Text = "Từ ngày";
            // 
            // txtFromDate
            // 
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Size = new System.Drawing.Size(132, 33);
            this.txtFromDate.ToolTipText = "Từ ngày";
            this.txtFromDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFromDate_KeyDown);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(72, 30);
            this.toolStripLabel1.Text = "Đến ngày";
            // 
            // txtToDate
            // 
            this.txtToDate.Name = "txtToDate";
            this.txtToDate.Size = new System.Drawing.Size(132, 33);
            this.txtToDate.ToolTipText = "Đến ngày";
            this.txtToDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtToDate_KeyDown);
            // 
            // btnFilter
            // 
            this.btnFilter.Image = ((System.Drawing.Image)(resources.GetObject("btnFilter.Image")));
            this.btnFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(56, 30);
            this.btnFilter.Text = "Lọc";
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 33);
            // 
            // toolStripExport
            // 
            this.toolStripExport.Image = ((System.Drawing.Image)(resources.GetObject("toolStripExport.Image")));
            this.toolStripExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripExport.Name = "toolStripExport";
            this.toolStripExport.Size = new System.Drawing.Size(63, 30);
            this.toolStripExport.Text = "Xuất";
            this.toolStripExport.Click += new System.EventHandler(this.toolStripExport_Click);
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(200, 30);
            // 
            // frmWellData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1389, 705);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmWellData";
            this.Text = "Dữ liệu khai thác theo giếng";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmWellData_FormClosed);
            this.Load += new System.EventHandler(this.frmWellData_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Data.OleDb.OleDbConnection oleDbConnection1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripSaveChart;
        private System.Windows.Forms.ToolStripButton toolStripCopyChart;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkCondensate;
        private System.Windows.Forms.CheckBox chkGor;
        private System.Windows.Forms.CheckBox chkCondensateYear;
        private System.Windows.Forms.CheckBox chkWaterCut;
        private System.Windows.Forms.CheckBox chkCondensateAcc;
        private System.Windows.Forms.CheckBox chkGasAcc;
        private System.Windows.Forms.CheckBox chkWater;
        private System.Windows.Forms.CheckBox chkGasYear;
        private System.Windows.Forms.CheckBox chkWaterYear;
        private System.Windows.Forms.CheckBox chkGas;
        private System.Windows.Forms.CheckBox chkWaterAcc;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripSetting;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripTextBox1;
        private System.Windows.Forms.ToolStripTextBox txtFromDate;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox txtToDate;
        private System.Windows.Forms.ToolStripButton btnFilter;
        private System.Windows.Forms.ToolStripButton toolStripExport;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTreeList.TreeList treeList1;
    }
}