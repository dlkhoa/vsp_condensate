using System;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace TrueTech.VSP.VSPCondensate
{
    static class Program {
        [STAThread]
        static void Main() {
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            frmLogin frm = new frmLogin();
            Application.Run(frm);
            if (!frm.stateLogin)
            {
                Application.Exit();
                return;
            }

            SplashScreenManager.ShowForm((Form)null, typeof(frmSplashScreen), true, true);
            Application.Run(new frmMain());
        }
    }
}
