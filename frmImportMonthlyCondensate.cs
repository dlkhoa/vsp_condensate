﻿using Spire.Xls;
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;


//using Microsoft.Office.Interop.Excel;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmImportMonthlyCondensate : Form
    {
        public string FileName; //file name bao gom ca duong dan
        public DataTable dataTable; //bang chua du lieu doc tu file Excel
        public string SafeFileName; //file name khong bao gom duong dan
        private string month;
        private string year;
        string namMax = null; //format dang mm/dd/yyyy
        DateTime dateMonth;

        clsDatabases database = new clsDatabases();
        clsDbProdQuery dbProd = new clsDbProdQuery();
        public frmImportMonthlyCondensate()
        {
            InitializeComponent();
        }

        private void frmProductionMonthlyExcel_Load(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            string error = null;
            if (!ImportXLS(FileName, ref table, ref error))
                MessageBox.Show(error);
            else
            {
                dataGridView1.DataSource = table;
                month = SafeFileName.Substring(2, 2);
                year = SafeFileName.Substring(4, 4);
                this.Text = "Nhập dữ liệu tháng " + month + " năm " + year;
            }
        }

        

        private string DefineWellNumber(string well_number)
        {
            int n = well_number.Length;
            int n_stop = 0;
            string s_int = ""; //gia tri so cua well_number
            string s_var = ""; //ky tu phia sau well_number
            for (int i = 0; i < n; i++)
            {
                if ((well_number[i] >= '0') && (well_number[i] <= '9'))
                    s_int = s_int + well_number[i].ToString();
                else
                {
                    n_stop = i;
                    break;
                }
            }
            if (n_stop > 0)
                s_var = well_number.Substring(n_stop);
            
            if (s_var.ToLower() == "б") s_var = "S1";
            else if (s_var == "B") s_var = "S1";
            else if (s_var == "БТ") s_var = "";

            if (s_int.Length == 1) s_int = "000" + s_int;
            else if (s_int.Length == 2) s_int = "00" + s_int;
            else if (s_int.Length == 3) s_int = "0" + s_int;
            return s_int + s_var;
        }

        private void SeparateChokeSize(string choke, ref string choke1, ref string choke2, string sep_char)
        {
            int n = choke.Length;
            int idx = choke.IndexOf(sep_char);
            choke1 = choke.Substring(0, idx);
            choke2 = choke.Substring(idx + 1);
        }

        private void SeparatePressure(string press, ref string press1, ref string press2, string sep_char)
        {
            int n = press.Length;
            int idx = press.IndexOf(sep_char);
            press1 = press.Substring(0, idx);
            press2 = press.Substring(idx + 1);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool ImportXLS(string str_FILE_NAME, ref DataTable DT, ref string str_RESPONSE)
        {
            try
            {
                Workbook workbook = new Workbook();

                workbook.LoadFromFile(str_FILE_NAME);
                //Initailize worksheet
                Worksheet sheet = workbook.Worksheets[0];

                DT = sheet.ExportDataTable();
                return true;
            }
            catch (Exception Ex)
            {
                str_RESPONSE = Ex.Message.ToString();
                return false;
            }
        }

        ///// <summary>
        ///// Input data from .XLS file, using SpireX control
        ///// </summary>
        ///// <param name="str_FILE_NAME">File's path</param>
        ///// <param name="DT">DataTable contains data</param>
        ///// <param name="str_RESPONSE">Response when error occurs</param>
        ///// <returns>true: if there is no error, false: if there is error</returns>
        //public bool ImportXLS(string str_FILE_NAME, ref DataTable DT, ref string str_RESPONSE)
        //{
        //    try
        //    {
        //        //ExcelEngine excelEngine = new ExcelEngine();
        //        //IApplication application = excelEngine.Excel;
        //        //application.UseNativeStorage = false;

        //        Spire.Xls.Workbook SpireWorkBook = new Spire.Xls.Workbook();
        //        SpireWorkBook.LoadFromFile(str_FILE_NAME);

        //        Spire.Xls.Worksheet SpireWorkSheet = SpireWorkBook.Worksheets[0];
        //        DT = SpireWorkSheet.ExportDataTable();

        //        int int_TOTAL_ERROR;
        //        int int_TOTAL_ROWS = DT.Rows.Count;
        //        int int_TOTAL_COLUMNS = DT.Columns.Count;
        //        string[] arr_ROW_ERROR = new string[int_TOTAL_ROWS];

        //        for (int int_ROW = 0; int_ROW < int_TOTAL_ROWS; int_ROW++)
        //        {
        //            int_TOTAL_ERROR = 0;
        //            for (int int_COL = 0; int_COL < int_TOTAL_COLUMNS; int_COL++)
        //            {
        //                if (DT.Rows[int_ROW][int_COL].ToString().Trim().Length == 0) int_TOTAL_ERROR++;
        //            }
        //            if (int_TOTAL_ERROR >= int_TOTAL_COLUMNS)
        //            {
        //                DT.Rows[int_ROW].Delete();
        //                int_ROW--;
        //                int_TOTAL_ROWS--;
        //            }
        //        }

        //        return true;
        //    }
        //    catch (Exception Ex)
        //    {
        //        str_RESPONSE = Ex.Message.ToString();
        //        return false;
        //    }
        //}

        private void btnLoadData_Click(object sender, EventArgs e)
        {
            if (!dataGridView1.Columns.Contains("Log message"))
                dataGridView1.Columns.Add("Log message", "Log message");

            //thiet lap format ngay thang
            CultureInfo oldCI = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            //xoa toan bo du lieu cua thang can nhap
            database.ExecuteNonQuery("DELETE FROM VSP_CONDENSATE WHERE MONTH = CONVERT(DATETIME,'" + month + "/01/" + year + "', 101)", CommandType.Text);

            //chon thoi gian gan nhat co so du lieu
            DataTable tableTime = database.FillDataTable("SELECT TOP 1 CONVERT(VARCHAR, MONTH, 101)"
                + " FROM VSP_CONDENSATE"
                + " WHERE MONTH < CONVERT(DATETIME, '" + month + "/01/" + year + "', 101)"
                + " ORDER BY MONTH DESC", CommandType.Text);
            foreach (DataRow dRow in tableTime.Rows)
            {
                namMax = dRow[0].ToString();
            }

            dateMonth = DateTime.Now;
            try
            {
                dateMonth = Convert.ToDateTime(month + "/01/" + year);
            }
            catch
            {

            }

            //truoc het load toan bo du lieu cua thang truoc
            LoadDataMonthBefore();

            //sau do load cac du lieu cua thang hien tai replace gia tri thang truoc

            //---thiet lap thanh trang thai-------
            int row_idx = 0;
            progressBar1.Maximum = dataGridView1.RowCount;
            progressBar1.Value = 0;
            int numError = 0;

            //--------------BAT DAU LOAD DU LIEU CUA THANG-------------------
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                dataGridView1[0, row_idx].Value = row_idx + 1;

                string msgReport = "";

                //field take from Column 3
                string field_id = dr.Cells[3].Value.ToString();
                field_id = field_id.Trim();

                //platfrom take from Column 5 --> need change for correct format
                string platform = dr.Cells[5].Value.ToString();

                //uwi take from Column 3, 4 and 5
                string well_number = dr.Cells[4].Value.ToString();
                well_number = well_number.Trim();
                well_number = DefineWellNumber(well_number);
                string uwi = field_id + platform + well_number;

                //reservoir take from Column 6
                string reservoir_short_name = dr.Cells[6].Value.ToString().Trim().ToUpper();
                string reservoir_id = "";
                DataTable dtReservoirID = database.FillDataTable
                    ("SELECT RESERVOIR_ID FROM VSP_EPOCH WHERE RESERVOIR_SHORT_NAME = N'"
                    + reservoir_short_name + "'", CommandType.Text);
                foreach (DataRow drReservoirID in dtReservoirID.Rows)
                {
                    reservoir_id = drReservoirID[0].ToString();
                }

                //dome take from Column 7
                string dome_name_id = dr.Cells[7].Value.ToString().Trim().ToUpper();
                string dome_id = "";
                DataTable dtDomeID = database.FillDataTable
                    ("SELECT DOME_ID FROM VSP_DOME WHERE DOME_NAME_ID = N'"
                    + dome_name_id + "'", CommandType.Text);
                foreach (DataRow drDomeID in dtDomeID.Rows)
                {
                    dome_id = drDomeID[0].ToString();
                }

                //zone take from Column 8
                string zone_id = dr.Cells[8].Value.ToString();
                zone_id = zone_id.Trim();

                //method take from Column 9

                string method_id = dr.Cells[9].Value.ToString();
                method_id = method_id.Trim().ToUpper();

                //neu Cot 10 khong co gia tri thi status = C; neu co thi lay gia tri do
                string status = "C";
                if (dr.Cells[10].Value.ToString().Trim().Equals("M"))
                    status = "M";

                //status_id lay tu method_id neu Column 11 khong co gia tri
                string status_id = "null";

                switch (method_id)
                {
                    case "ФОН": method_id = "1"; status_id = "1"; break;
                    case "КГ": method_id = "3"; status_id = "2"; break;
                    case "ЭЦН": method_id = "2"; status_id = "3"; break;
                }
                if (dr.Cells[11].Value.ToString().Trim().Length > 0)
                {
                    switch (dr.Cells[11].Value.ToString().Trim())
                    {
                        case "*": status_id = "4"; break;
                        case "**": status_id = "5"; break;
                        case "***": status_id = "6"; break;
                        case "НАГ.": status_id = "8"; break;
                    }
                }

                //condensate take from Column 12
                string sCondensate = dr.Cells[12].Value.ToString().Trim();
                if (sCondensate.Trim().Length == 0) sCondensate = "0";

                //gas take from Column 13
                string sGas = dr.Cells[13].Value.ToString().Trim();
                if (sGas.Trim().Length == 0) sGas = "0";

                //water cut take from Column 14
                string sWater_cut = dr.Cells[14].Value.ToString().Trim();
                if (sWater_cut.Trim().Length == 0) sWater_cut = "0";

                //choke size take from Column 15
                string s_choke_size = dr.Cells[15].Value.ToString().Trim().ToLower();
                string s_choke_size_1 = "null";
                string s_choke_size_2 = "null";
                if (s_choke_size.Contains("бш") || s_choke_size.Contains("без")) s_choke_size = s_choke_size.Replace("бш", "500").Replace("без", "500");
                if (s_choke_size.Contains("/"))
                    SeparateChokeSize(s_choke_size, ref s_choke_size_1, ref s_choke_size_2, "/");
                else if (s_choke_size.Contains("-"))
                    SeparateChokeSize(s_choke_size, ref s_choke_size_1, ref s_choke_size_2, "-");
                else if (s_choke_size.Contains("+"))
                    SeparateChokeSize(s_choke_size, ref s_choke_size_1, ref s_choke_size_2, "+");
                else if (s_choke_size == "0")
                {
                    s_choke_size_1 = "null";
                    s_choke_size_2 = "null";
                }
                else s_choke_size_1 = s_choke_size;

                object choke_size_1 = s_choke_size_1;
                if (s_choke_size_1 == "null") choke_size_1 = DBNull.Value;

                object choke_size_2 = s_choke_size_2;
                if (s_choke_size_2 == "null") choke_size_2 = DBNull.Value;

                //prod_hours take from Column 16
                string sProd_hours = dr.Cells[16].Value.ToString().Trim();
                if (sProd_hours.Length == 0) sProd_hours = "null";

                //fill_up_hours take from Column 17
                string sFill_up_hours = dr.Cells[17].Value.ToString().Trim();
                if (sFill_up_hours.Length == 0) sFill_up_hours = "null";

                //pressure take from Column 18
                string s_pressure = dr.Cells[18].Value.ToString().Trim();
                string s_pressure_1 = "null";
                string s_pressure_2 = "null";
                if (s_pressure.Contains("/"))
                    SeparatePressure(s_pressure, ref s_pressure_1, ref s_pressure_2, "/");
                else if (s_pressure.Contains("-"))
                    SeparatePressure(s_pressure, ref s_pressure_1, ref s_pressure_2, "-");
                else if (s_pressure.Contains("+"))
                    SeparatePressure(s_pressure, ref s_pressure_1, ref s_pressure_2, "+");
                else if (s_pressure == "0")
                {
                    s_pressure_1 = "null";
                    s_pressure_2 = "null";
                }
                else s_pressure_1 = s_pressure;

                object pressure_1 = s_pressure_1;
                if (s_pressure_1 == "null") pressure_1 = DBNull.Value;

                object pressure_2 = s_pressure_2;
                if (s_pressure_2 == "null") pressure_2 = DBNull.Value;


                //remarks take from Column 19
                string remarks = dr.Cells[19].Value.ToString().Trim();


                //tinh cac gia tri Condensate, Water, Gas va cac gia tri cong don
                double condensate = Math.Round(Convert.ToDouble(sCondensate), 0, MidpointRounding.AwayFromZero);
                double gas = Math.Round(Convert.ToDouble(sGas), 3, MidpointRounding.AwayFromZero);
                double water_cut = Math.Round(Convert.ToDouble(sWater_cut), 1, MidpointRounding.AwayFromZero);
                double prod_hours = Math.Round(Convert.ToDouble(sProd_hours), 1, MidpointRounding.AwayFromZero);
                double fill_up_hours = Math.Round(Convert.ToDouble(sFill_up_hours), 1, MidpointRounding.AwayFromZero);
                var monthHours = System.DateTime.DaysInMonth(int.Parse(year), int.Parse(month)) * 24;
                if (prod_hours > monthHours) prod_hours = monthHours;
                if (fill_up_hours > monthHours) fill_up_hours = monthHours;
                double suspended_hours = monthHours - prod_hours - fill_up_hours;
                if (suspended_hours < 0) suspended_hours = 0;
                double water_1 = Math.Round(condensate * water_cut / (100.0 - water_cut), 1, MidpointRounding.AwayFromZero);
                double water = Math.Round(water_1, 0, MidpointRounding.AwayFromZero);
                double gor = (prod_hours + fill_up_hours) == 0 ? 0 : Math.Round(gas * 24 / (prod_hours + fill_up_hours), 1, MidpointRounding.AwayFromZero);

                double condensate_year = 0;
                double condensate_acc = 0;
                double water_year = 0;
                double water_acc = 0;
                double gas_year = 0;
                double gas_acc = 0;
                double workday_year = Math.Round((prod_hours + fill_up_hours) / 24, 2, MidpointRounding.AwayFromZero);

                //kiem tra neu du lieu chua co thi insert
                if (!dbProd.CheckIfRowExistInCondensate(field_id, uwi,
                    reservoir_id, dome_id, zone_id, method_id, status, dateMonth))
                {
                    int i = dbProd.InsertToCondensate(field_id, platform, uwi, reservoir_id,
                        dome_id, zone_id, method_id, status, dateMonth, status_id, water_cut,
                        condensate, condensate_year, condensate_acc, water, water_year, water_acc,
                        gas, gas_year, gas_acc, choke_size_1, choke_size_2,
                        prod_hours, fill_up_hours, suspended_hours, workday_year,
                        pressure_1, pressure_2, gor, remarks, ref msgReport);
                    if (i == 0)
                    {
                        numError++;
                        dataGridView1[20, row_idx].Style.BackColor = Color.Red;
                    }
                }
                else
                {
                    int i = dbProd.UpdateToCondensate(field_id, platform, uwi, reservoir_id,
                        dome_id, zone_id, method_id, status, dateMonth, status_id, water_cut,
                        condensate, condensate_year, condensate_acc, water, water_year, water_acc,
                        gas, gas_year, gas_acc, choke_size_1, choke_size_2,
                        prod_hours, fill_up_hours, suspended_hours, workday_year,
                        pressure_1, pressure_2, gor, remarks, ref msgReport);
                    if (i == 0)
                    {
                        numError++;
                        dataGridView1[20, row_idx].Style.BackColor = Color.Red;
                    }
                }

                //chay ham cong don
                dbProd.RunAccumulateCondensate(uwi, reservoir_id,
                        dome_id, zone_id, method_id, dateMonth, ref msgReport);

                //Show message at column 21 of DataGridView
                dataGridView1[20, row_idx].Value = msgReport;

                //Increase value in Progress Bar
                progressBar1.Value++;
                row_idx++;
            }

            //-------------UPDATE DATA IN VSP_FLUID_TOTAL---------------
            /*if (dateMonth >= Convert.ToDateTime("02/01/2012"))
                dbProd.UpdateTableVSP_FLUID_TOTAL_FromProduction(namMax);*/

            System.Threading.Thread.CurrentThread.CurrentCulture = oldCI; //give old culture setting back
            MessageBox.Show("Hoàn thành!");
        }

        private void LoadDataMonthBefore()
        {
            string sql;
            if (Convert.ToDateTime(namMax).Year.ToString() == year)
                sql = "SELECT FIELD_ID, PLATFORM, UWI, RESERVOIR_ID, DOME_ID, "
                        + "ZONE_ID, METHOD_ID, STATUS,CONVERT(VARCHAR, MONTH, 101), "
                        + " 7 AS STATUS_ID, 0.0 AS WATER_CUT, "
                        + "0 AS CONDENSATE, CONDENSATE_YEAR, CONDENSATE_ACC, 0 AS WATER, WATER_YEAR, WATER_ACC, 0 AS GAS, GAS_YEAR, GAS_ACC, "
                        + "'' AS CHOKE_SIZE_1,'' AS CHOKE_SIZE_2, 0 AS PROD_HOURS, 0 AS FILL_UP_HOURS, "
                        + "0 AS SUSPENDED_HOURS, WORKDAY_YEAR, '' AS PRESSURE_1, '' AS PRESSURE_2, "
                        + "0 AS GOR, '' AS REMARKS FROM VSP_CONDENSATE WHERE MONTH = CONVERT(DATETIME,'" + namMax + "', 101)";
            else
                sql = "SELECT FIELD_ID, PLATFORM, UWI, RESERVOIR_ID, DOME_ID, "
                        + "ZONE_ID, METHOD_ID, 'C' AS STATUS, CONVERT(VARCHAR, MONTH, 101), "
                        + " 7 AS STATUS_ID, 0.0 AS WATER_CUT, "
                        + "0 AS CONDENSATE, 0 AS CONDENSATE_YEAR, CONDENSATE_ACC, 0 AS WATER, 0 AS WATER_YEAR, WATER_ACC, 0 AS GAS, 0 AS GAS_YEAR, GAS_ACC, "
                        + "'' AS CHOKE_SIZE_1,'' AS CHOKE_SIZE_2, 0 AS PROD_HOURS, 0 AS FILL_UP_HOURS, "
                        + "0 AS SUSPENDED_HOURS, 0 AS WORKDAY_YEAR, '' AS PRESSURE_1, '' AS PRESSURE_2, "
                        + "0 AS GOR, '' AS REMARKS FROM VSP_CONDENSATE WHERE MONTH = CONVERT(DATETIME,'" + namMax + "', 101)";

            DataTable dt = database.FillDataTable(sql, CommandType.Text);
            progressBar1.Maximum = dt.Rows.Count;
            progressBar1.Value = 0;

            foreach (DataRow dr in dt.Rows)
            {
                string field_id = dr[0].ToString();
                string platform = dr[1].ToString();
                string uwi = dr[2].ToString();
                string reservoir_id = dr[3].ToString();
                string dome_id = dr[4].ToString();
                string zone_id = dr[5].ToString();
                string method_id = dr[6].ToString();
                string status = dr[7].ToString();
                string status_id = dr[9].ToString();

                object choke_size_1 = DBNull.Value;
                if (dr[20].ToString().Length != 0) choke_size_1 = dr[20].ToString();
                object choke_size_2 = DBNull.Value;
                if (dr[21].ToString().Length != 0) choke_size_2 = dr[21].ToString();
                object pressure_1 = DBNull.Value;
                if (dr[26].ToString().Length != 0) pressure_1 = dr[26].ToString();
                object pressure_2 = DBNull.Value;
                if (dr[27].ToString().Length != 0) pressure_2 = dr[27].ToString();
                string remarks = dr[29].ToString();

                double water_cut = 0;
                try { Convert.ToDouble(dr[10].ToString()); }
                catch { }

                double condensate = 0;
                try { condensate = Convert.ToDouble(dr[11].ToString()); }
                catch { }

                double condensate_year = 0;
                try { condensate_year = Convert.ToDouble(dr[12].ToString()); }
                catch { }

                double condensate_acc = 0;
                try { condensate_acc = Convert.ToDouble(dr[13].ToString()); }
                catch { }

                double water = 0;
                try { water = Convert.ToDouble(dr[14].ToString()); }
                catch { }

                double water_year = 0;
                try { water_year = Convert.ToDouble(dr[15].ToString()); }
                catch { }

                double water_acc = 0;
                try { water_acc = Convert.ToDouble(dr[16].ToString()); }
                catch { }

                double gas = 0;
                try { gas = Convert.ToDouble(dr[17].ToString()); }
                catch { }

                double gas_year = 0;
                try { gas_year = Convert.ToDouble(dr[18].ToString()); }
                catch { }

                double gas_acc = 0;
                try { gas_acc = Convert.ToDouble(dr[19].ToString()); }
                catch { }

                double prod_hours = 0;
                try { prod_hours = Convert.ToDouble(dr[22].ToString()); }
                catch { }

                double fill_up_hours = 0;
                try { fill_up_hours = Convert.ToDouble(dr[23].ToString()); }
                catch { }

                double suspended_hours = 0;
                try { suspended_hours = Convert.ToDouble(dr[24].ToString()); }
                catch { }

                double workday_year = 0;
                try { workday_year = Convert.ToDouble(dr[25].ToString()); }
                catch { }

                double gor = 0;
                try { gor = Convert.ToDouble(dr[28].ToString()); }
                catch { }

                if (!dbProd.CheckIfRowExistInCondensate(field_id, uwi,
                    reservoir_id, dome_id, zone_id, method_id, status, dateMonth))
                {
                    string msgReport = "";
                    int i = dbProd.InsertToCondensate(field_id, platform, uwi, reservoir_id,
                        dome_id, zone_id, method_id, status, dateMonth, status_id, water_cut,
                        condensate, condensate_year, condensate_acc, water, water_year, water_acc,
                        gas, gas_year, gas_acc, choke_size_1, choke_size_2,
                        prod_hours, fill_up_hours, suspended_hours, workday_year,
                        pressure_1, pressure_2, gor, remarks, ref msgReport);
                }
                progressBar1.Value++;
            }
        }

        private void btnCheckData_Click(object sender, EventArgs e)
        {
            int numRow = dataGridView1.Rows.Count;
            
            DataTable dtFieldID = database.FillDataTable("SELECT FIELD_ID FROM VSP_FIELD_HDR", CommandType.Text);
            DataTable dtWell = database.FillDataTable("SELECT UWI FROM VSP_WELL_HDR", CommandType.Text);
            DataTable dtPlatform = database.FillDataTable("SELECT PLATFORM FROM VSP_PLATFORM", CommandType.Text);
            DataTable dtReservoir = database.FillDataTable("SELECT RESERVOIR_SHORT_NAME FROM VSP_EPOCH", CommandType.Text);
            DataTable dtDome = database.FillDataTable("SELECT DOME_NAME_ID FROM VSP_DOME", CommandType.Text);
            DataTable dtZone = database.FillDataTable("SELECT ZONE_ID FROM VSP_ZONE", CommandType.Text);
            DataTable dtMethod = database.FillDataTable("SELECT METHOD_SHORT_NAME FROM VSP_METHOD", CommandType.Text);

            progressBar1.Maximum = numRow;
            progressBar1.Value = 0;

            int numErr = 0;

            for (int i = 0; i < numRow; i++)
            {
                //field column
                DataGridViewCell cell = dataGridView1[3, i];
                bool bIsExist = false;
                cell.Style.BackColor = Color.White;
                foreach (DataRow drFieldID in dtFieldID.Rows)
                {
                    if (cell.Value.ToString() == drFieldID[0].ToString()) bIsExist = true;
                }
                if (!bIsExist)
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //well column
                cell = dataGridView1[4, i];
                string well = cell.Value.ToString();
                string num_well = "";
                int num_index = 0;
                int length = well.Length;
                for (int j = 0; j < length; j++)
                {
                    if (well[j].Equals('0') || well[j].Equals('1') || well[j].Equals('2')
                         || well[j].Equals('3') || well[j].Equals('4') || well[j].Equals('5')
                         || well[j].Equals('6') || well[j].Equals('7') || well[j].Equals('8')
                         || well[j].Equals('9'))
                    {
                        num_well = num_well + well[j].ToString();
                        num_index = j;
                    }
                    else
                        break;
                }
                string text_well = "";
                if (num_index + 1 < well.Length)
                    text_well = well.Substring(num_index + 1);
                text_well = text_well.Replace("Б", "B");

                if (num_well.Length == 3) num_well = "0" + num_well;
                else if (num_well.Length == 2) num_well = "00" + num_well;
                else if (num_well.Length == 1) num_well = "000" + num_well;

                string well_number = well;
                well_number = well_number.Trim();
                well_number = DefineWellNumber(well_number);
                string uwi = dataGridView1[3, i].Value.ToString() + dataGridView1[5, i].Value.ToString() + well_number;
                //string uwi = dataGridView1[3, i].Value.ToString() + dataGridView1[5, i].Value.ToString()
                //    + num_well + text_well;
                cell.Style.BackColor = Color.White;
                bIsExist = false;
                foreach (DataRow drWell in dtWell.Rows)
                {
                    if (uwi == drWell[0].ToString()) bIsExist = true;
                }
                if (!bIsExist)
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //platform column
                cell = dataGridView1[5, i];
                cell.Style.BackColor = Color.White;
                bIsExist = false;
                foreach (DataRow drPlatform in dtPlatform.Rows)
                {
                    if (cell.Value.ToString() == drPlatform[0].ToString()) bIsExist = true;
                }
                if (!bIsExist)
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //reservoir column
                cell = dataGridView1[6, i];
                cell.Style.BackColor = Color.White;
                bIsExist = false;
                foreach (DataRow drReservoir in dtReservoir.Rows)
                {
                    if (cell.Value.ToString() == drReservoir[0].ToString()) bIsExist = true;
                }
                if (!bIsExist)
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //dome column
                cell = dataGridView1[7, i];
                cell.Style.BackColor = Color.White;
                bIsExist = false;
                foreach (DataRow drDome in dtDome.Rows)
                {
                    if (cell.Value.ToString() == drDome[0].ToString()) bIsExist = true;
                }
                if (!bIsExist)
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //zone column
                cell = dataGridView1[8, i];
                cell.Style.BackColor = Color.White;
                bIsExist = false;
                foreach (DataRow drZone in dtZone.Rows)
                {
                    if (cell.Value.ToString() == drZone[0].ToString()) bIsExist = true;
                }
                if (!bIsExist)
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //method column
                cell = dataGridView1[9, i];
                cell.Style.BackColor = Color.White;
                bIsExist = false;
                foreach (DataRow drMethod in dtMethod.Rows)
                {
                    if (cell.Value.ToString() == drMethod[0].ToString()) bIsExist = true;
                }
                if (!bIsExist)
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //CuMoi column
                cell = dataGridView1[10, i];
                cell.Style.BackColor = Color.White;
                if ((cell.Value.ToString().Trim().Length > 0) && (cell.Value.ToString() != "M"))
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //Ki hieu column
                cell = dataGridView1[11, i];
                cell.Style.BackColor = Color.White;
                if ((cell.Value.ToString().Trim().Length > 0)
                    && !((cell.Value.ToString() == "*") || (cell.Value.ToString() == "**")
                    || (cell.Value.ToString() == "***") || (cell.Value.ToString() == "НАГ.")))
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //condensate column
                cell = dataGridView1[12, i];
                cell.Style.BackColor = Color.White;
                try
                {
                    Convert.ToDouble(cell.Value.ToString());
                }
                catch 
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //gas column
                cell = dataGridView1[13, i];
                cell.Style.BackColor = Color.White;
                try
                {
                    Convert.ToDouble(cell.Value.ToString());
                }
                catch
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //water_cut column
                cell = dataGridView1[14, i];
                cell.Style.BackColor = Color.White;
                try
                {
                    Convert.ToDouble(cell.Value.ToString());
                }
                catch
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //choke_size 
                cell = dataGridView1[15, i];
                cell.Style.BackColor = Color.White;
                string choke_size = cell.Value.ToString();
                bool bValid = true;
                for (int j = 0; j < choke_size.Length; j++)
                {

                    if (!(((choke_size[j] >= '0') && (choke_size[j] <= '9'))
                        || (choke_size[j] == '/') || (choke_size[j] == 'б') 
                        || (choke_size[j] == 'ш') || (choke_size[j] == '.')
                        || (choke_size[j] == 'е') || (choke_size[j] == 'з'))) 

                         bValid = false;
                }
                if (!bValid)
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }
                    
                //prod hours
                cell = dataGridView1[16, i];
                cell.Style.BackColor = Color.White;
                try
                {
                    Convert.ToDouble(cell.Value.ToString());
                }
                catch
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //fill up hours
                cell = dataGridView1[17, i];
                cell.Style.BackColor = Color.White;
                try
                {
                    Convert.ToDouble(cell.Value.ToString());
                }
                catch
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                //pressure 
                cell = dataGridView1[18, i];
                cell.Style.BackColor = Color.White;
                string pressure = cell.Value.ToString();
                bValid = true;
                for (int j = 0; j < pressure.Length; j++)
                {

                    if (!(((pressure[j] >= '0') && (pressure[j] <= '9'))
                        || (pressure[j] == '/') || (pressure[j] == '.')))

                        bValid = false;
                }
                if (!bValid)
                {
                    cell.Style.BackColor = Color.Red;
                    numErr++;
                }

                progressBar1.Value++;
            }
            MessageBox.Show("Số lỗi: " + numErr.ToString());
            
        }

        private void frmImportMonthlyCondensate_FormClosed(object sender, FormClosedEventArgs e)
        {
            Session.frmImportMonthlyCondensate = null;
        }
    }
}
