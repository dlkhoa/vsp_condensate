using System;
using System.Collections ;
using System.Data;
using System.Windows.Forms;

namespace TrueTech.VSP.VSPCondensate
{
    class clsControls
    {
        clsDatabases cls;
        internal clsControls()
        {
           cls = new clsDatabases();
        }
        internal void FillControls(ComboBox cb, string SQL)
        {
            DataTableReader dataTableReader =
                cls.FillDataTableReader(SQL, CommandType.Text);
            while (dataTableReader.Read())
            {
                cb.Items.Add(dataTableReader.GetString(0));
            }
        }
        internal void FillControls(ComboBox cb, 
            string SQL,string selectedValue,bool firstValue )
        {
            int i = 1,j=0;
            DataTableReader dataTableReader = 
             cls.FillDataTableReader(SQL, CommandType.Text);
             ListItem listItem  ;
            if (firstValue)
            {
                listItem = new ListItem();
                listItem.Name = "";
                listItem.Value = "";
                cb.Items.Add(listItem);
            }
            while (dataTableReader.Read())
            {
                listItem =new ListItem();
                listItem.Name = dataTableReader.GetValue(0).ToString();
                listItem.Value = dataTableReader.GetString(1);
                cb.Items.Add(listItem);
                if (selectedValue.Equals(listItem.Value))
                    j = i;
                i++;
            }
            cb.DisplayMember = "Name";
            cb.ValueMember = "Value";
            cb.SelectedIndex = j;
            
        }
        internal void FillControls(ComboBox[] cb,
            string[] SQL, string[] selectedValue, bool firstValue, int[] nextIndex )
        {

            int i = 0, j = 0,indexCombo=0;
            ArrayList arrayListObject =
               cls.GetArrayLists(SQL,
               CommandType.Text,  nextIndex);
            ListItem listItem;
            for (i = 0; i < cb.Length; i++)
            {
                if (firstValue)
                {
                    listItem = new ListItem();
                    listItem.Name = "";
                    listItem.Value = "";
                    cb[i].Items.Add(listItem);
                }               
            }
            i = 0;
            foreach (object arrayList in arrayListObject)
            {
                object[] objs = (object[])arrayList;
                listItem = new ListItem();
                listItem.Name = Convert.ToString(objs[1]);
                listItem.Value  = Convert.ToString(objs[0]);
                cb[indexCombo].Items.Add(listItem);                
                i++;
                if (i == nextIndex[j])
                {
                    j++;
                    indexCombo++;
                }
            }

            
            
            for (i = 0; i < cb.Length; i++)
            {
                 
                cb[i].DisplayMember = "Name";
                cb[i].ValueMember = "Value";
            }
        }
        internal void FillControls(ComboBox cb, 
            string SQL, string ValueMember, 
            string DisplayMember, string FirstValue, 
            string FirstDisplay)
        {

            DataTable dt = cls.FillDataTable(SQL,CommandType.Text);
           if (dt != null)
           {
               if (!FirstDisplay.Equals(""))
               {
                   DataRow dr = dt.NewRow();
                   dr[0] = FirstValue;
                   dr[1] = FirstDisplay;
                   dt.Rows.InsertAt(dr, 0);
               }
               if (dt != null)
               {
                   cb.DataSource = dt;
                   cb.DisplayMember = DisplayMember;
                   cb.ValueMember = ValueMember;
               }
           }
        }
        internal DataTable  FillControls(TreeView tv, 
            string SQL,string FirstNode, 
            string ValueMember, string DisplayMember, 
            bool SelectFirstNode)
        {
            DataTable dt = cls.FillDataTable(SQL, CommandType.Text);
            
                tv.Nodes.Add("", FirstNode);
                if (dt != null)
                {
                    TreeNode tn;
                    foreach (DataRow dr in dt.Rows)
                    {
                        tn = new TreeNode();
                        tn.Tag = dr[0].ToString();
                        DateTime t = Convert.ToDateTime(dr[1].ToString());
                        tn.Text = t.ToShortDateString();
                        tv.Nodes[0].Nodes.Add(tn);
                    }
                }
                tv.ExpandAll();
                if (SelectFirstNode)
                    tv.SelectedNode = tv.Nodes[0].Nodes[0];
            return dt;

        }

        internal DataTable  FillControls(ListView lv, string SQL )
        {
            DataTable dt = cls.FillDataTable(SQL, CommandType.Text);
            
            if (dt != null)
            {
                ListViewItem lvt;
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    lvt = new ListViewItem(i.ToString());
                    foreach(string str in dr.ItemArray)
                        {
                            lvt.SubItems.Add(str .ToString());                    
                        }
                    lv.Items.Add(lvt);
                }
            }
            return dt;
        }

        internal DataTable FillControls(ListBox lb, string SQL)
        {
            DataTable dt = cls.FillDataTable(SQL, CommandType.Text);

            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    lb.Items.Add(dr[0].ToString());
                }
            }
            return dt;
        }
        internal DataTable  FillControls(ListView lv, 
            string SQL, string[] headerText)
        {
            DataTable dt = cls.FillDataTable(SQL, CommandType.Text);
            foreach (string str in headerText)
            {
                lv.Columns.Add(str);
            }
            if (dt != null)
            {
                ListViewItem lvt;
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    i++;
                    lvt = new ListViewItem(i.ToString());
                    foreach (string str in dr.ItemArray)
                    {
                        lvt.SubItems.Add(str.ToString());
                    }
                    lv.Items.Add(lvt);
                }
            }
            return dt;
        }
        internal DataTable  FillControls(ListView lv, 
            string SQL, string[] headerText, int[] headerWidth)
        {
            DataTable dt = cls.FillDataTable(SQL, CommandType.Text);
            lv.Clear();
            for (int i = 0; i < headerText.Length;i++ )
            {
                lv.Columns.Add(headerText[i], headerWidth[i]);
            }
            if (dt != null)
            {
                ListViewItem lvt;
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    i++;
                    lvt = new ListViewItem(i.ToString());
                    foreach (object obj in dr.ItemArray)
                    {
                        lvt.SubItems.Add(Convert.ToString(obj));
                    }
                    lv.Items.Add(lvt);
                }
            }
            return dt;
        }
        internal DataTable  FillControls(ListView lv, 
            string SQL, string[] headerText, 
            int[] headerWidth, 
            HorizontalAlignment[] headerAlign, 
            string strFormat, short startPosition, 
            short endPosition)
        {
            DataTable dt = cls.FillDataTable(SQL, CommandType.Text);
            lv.Clear();
            for (int i = 0; i < headerText.Length; i++)
            {
                lv.Columns.Add(headerText[i], headerWidth[i], headerAlign[i]);
            }
            if (dt != null)
            {
                ListViewItem lvt;
                int i = 0;
                int j = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    i++;
                    lvt = new ListViewItem(i.ToString());
                    j = 0;
                    foreach (object obj in dr.ItemArray)
                    {
                        j++;
                        if (j > startPosition && j < endPosition)
                        {
                            if (Convert.ToString(obj) != "")
                                lvt.SubItems.Add(Convert.ToDouble(obj).ToString(strFormat));
                        }
                        else
                        {
                            lvt.SubItems.Add(Convert.ToString(obj));
                        }
                    }
                    lv.Items.Add(lvt);
                }
            }
            return dt;
        }
        internal DataTable  FillControls(ListView lv, 
            string SQL, string[] headerText, 
            int[] headerWidth, 
            HorizontalAlignment[] headerAlign)
        {
            DataTable dt = cls.FillDataTable(SQL, CommandType.Text);
            lv.Clear();
            for (int i = 0; i < headerText.Length; i++)
            {
                lv.Columns.Add(headerText[i], headerWidth[i], headerAlign[i]);
            }
            if (dt != null)
            {
                ListViewItem lvt;
                int i = 0;                
                foreach (DataRow dr in dt.Rows)
                {
                    i++;
                    lvt = new ListViewItem(i.ToString());
                  
                    foreach (object obj in dr.ItemArray)
                    {
                        lvt.SubItems.Add(Convert.ToString(obj));
                    }
                    lv.Items.Add(lvt);
                }
            }
            return dt;
        }
        internal DataTable  FillControls(DataGridView  dgv,string SQL)
        {
            DataTable dataTable = cls.FillDataTable(SQL, CommandType.Text);
            dgv.DataSource = dataTable;
            return dataTable;
        }
        internal DataTable FillControls(DataGridView dgv, string SQL, string[] headerText, int[] width)
        {
            DataTable dataTable = cls.FillDataTable(SQL, CommandType.Text);
            dgv.DataSource = dataTable;
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                dgv.Columns[i].HeaderText = headerText[i];
                dgv.Columns[i].Width = width[i];
            }
            return dataTable;
        }
        internal DataTable  FillControls(DataGridView dgv, string SP,string[] parameters,string[] values)
        {
            DataTable dataTable = cls.FillDataTable(SP, CommandType.StoredProcedure, parameters, values);
            dgv.DataSource = dataTable;
            return dataTable;
        }

        ~ clsControls()
        {
              
        }
    }
}
