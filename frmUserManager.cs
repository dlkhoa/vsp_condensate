using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmUserManager : Form
    {
        clsDatabases database = new clsDatabases();
        private string[] usersSelected;
        public frmUserManager()
        {
            InitializeComponent();
        }

        private void frmUserManager_Load(object sender, EventArgs e)
        {
            ShowUserList();
        }

        private void ShowUserList()
        {
            clsControls control = new clsControls();
            control.FillControls(lvUser, "SELECT UserName, "
                + "FullName, Department, IsAdminGroup, AccessProduction, AccessInjection, Can_Change_Data FROM SYS_USERS",
                new string[8] {"STT", "Tài khoản ", "Họ tên", "Phòng", "Quản trị", "Nhập dữ liệu khai thác", "Nhập dữ liệu bơm ép", "Cho phép sửa dữ liệu"},
                new int[8] {40, 100, 120, 120, 80, 120, 120, 120 });
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ShowMessageCannotDelete()
        {
            MessageBox.Show("Bạn không thể xóa tài khoản này!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        private void lvUser_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (!e.IsSelected) return;
            txtUserName.Text = e.Item.SubItems[1].Text;

            string sqlQuery = "SELECT USERNAME, CONVERT(VARCHAR(10), DECRYPTBYPASSPHRASE('DBProd', PASSWORD)),"
                + " FULLNAME, DEPARTMENT, ISADMINGROUP, ACCESSPRODUCTION, ACCESSINJECTION, CAN_CHANGE_DATA"
                + " FROM SYS_USERS WHERE USERNAME = '" + txtUserName.Text + "'";
            DataTable dt = new DataTable();
            dt = database.FillDataTable(sqlQuery, CommandType.Text);
            foreach (DataRow dr in dt.Rows)
            {
                txtPassword.Text = dr[1].ToString();
                txtRePassword.Text = dr[1].ToString();
                txtFullName.Text = dr[2].ToString();
                txtDepartment.Text = dr[3].ToString();
                if (dr[4].ToString() == "True") chkAdminFunction.Checked = true;
                else chkAdminFunction.Checked = false;

                if (dr[5].ToString() == "True") chkProducitonFunction.Checked = true;
                else chkProducitonFunction.Checked = false;

                if (dr[6].ToString() == "True") chkInjectionFunction.Checked = true;
                else chkInjectionFunction.Checked = false;

                if (dr[7].ToString() == "True") chkCanChangeData.Checked = true;
                else chkCanChangeData.Checked = false;
            }

            ListView.SelectedListViewItemCollection userCol = lvUser.SelectedItems;
            usersSelected = new string[userCol.Count];
            int i = 0;
            foreach (ListViewItem item in userCol)
            {
                usersSelected.SetValue(item.SubItems[1].Text, i);
                i++;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text.Length != 0)
            {
                if (txtPassword.Text != txtRePassword.Text)
                {
                    MessageBox.Show("Mật khẩu không trùng nhau!");
                    return;
                }
                string sql = "SELECT USERNAME, ISADMINGROUP FROM SYS_USERS WHERE USERNAME = '" 
                    + txtUserName.Text + "'";
                
                //biến lưu giá trị của User đang được sửa:
                //User thuộc nhóm Admin: strIsAdminGroup = True
                //User không thuộc nhóm Admin: strIsAdminGroup = False
                string strIsAdminGroup = "False";
                
                DataTable dtUser = database.FillDataTable(sql, CommandType.Text);

                foreach (DataRow dr in dtUser.Rows)
                {
                    strIsAdminGroup = dr[1].ToString();
                }

                if (dtUser.Rows.Count > 0)
                {
                    //User đang đăng nhập là ADMIN thì mới sửa được thông tin của họ
                    if ((txtUserName.Text.ToUpper() == "ADMIN") && (clsStatic.strUser == "ADMIN"))
                        UpdateUser(txtUserName.Text);
                    //User đang đăng nhập là ADMIN thì được sửa thông tin của mọi thành viên
                    else if (clsStatic.strUser == "ADMIN")
                        UpdateUser(txtUserName.Text);
                    //trường hợp User đang đăng nhập không phải là ADMIN 
                    //thì được quyền sửa thông tin của chính họ
                    else if (txtUserName.Text.ToUpper() == clsStatic.strUser)
                        UpdateUser(txtUserName.Text);
                    //nếu User đang đăng nhập thuộc nhóm Admin (clsStatic.bIsAdminGroup = true)
                    //thì có quyền sửa thông tin của User không thuộc nhóm Admin (strIsAdminGroup = False)
                    else if ((clsStatic.bIsAdminGroup) && (strIsAdminGroup == "False"))
                        UpdateUser(txtUserName.Text);
                }
                else
                {
                    //User đang đăng nhập phải thuộc nhóm quản trị thì mới được phép tạo User mới
                    if (clsStatic.bIsAdminGroup)
                        InsertUser(txtUserName.Text);
                }
            }
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (usersSelected == null) return;

            foreach (string userID in usersSelected)
            {
                string sql = "SELECT USERNAME, ISADMINGROUP FROM SYS_USERS WHERE USERNAME = '"
                    + userID + "'";

                //biến lưu giá trị của User đang được sửa:
                //User thuộc nhóm Admin: strIsAdminGroup = True
                //User không thuộc nhóm Admin: strIsAdminGroup = False
                string strIsAdminGroup = "False";

                DataTable dtUser = database.FillDataTable(sql, CommandType.Text);

                foreach (DataRow dr in dtUser.Rows)
                {
                    strIsAdminGroup = dr[1].ToString();
                }
                if ((clsStatic.strUser == "ADMIN") && (userID != "ADMIN"))
                    DeleteUser(userID);
                else if ((strIsAdminGroup == "False") && (clsStatic.bIsAdminGroup))
                    DeleteUser(userID);
                else
                    MessageBox.Show("Không thể xóa user: " + userID);
            }
        }

        private void UpdateUser(string userID)
        {
            string strAdminFunction;
            string strProductionFunction;
            string strInjectionFunction;
            string strCanChangeData;
            
            if (chkAdminFunction.Checked) strAdminFunction = "True";
            else strAdminFunction = "False";

            if (chkProducitonFunction.Checked) strProductionFunction = "True";
            else strProductionFunction = "False";

            if (chkInjectionFunction.Checked) strInjectionFunction = "True";
            else strInjectionFunction = "False";

            if (chkCanChangeData.Checked) strCanChangeData = "True";
            else strCanChangeData = "False";

            string sqlCmd = "UPDATE SYS_Users SET FullName = N'" + txtFullName.Text
                        + "', Password = EncryptByPassPhrase('DBProd', Convert(varchar(MAX), '"
                        + txtPassword.Text + "')), Department = N'" + txtDepartment.Text
                        + "', IsAdminGroup = '" + strAdminFunction 
                        + "', AccessProduction = '" + strProductionFunction
                        + "', AccessInjection = '" + strInjectionFunction
                        + "', Can_Change_Data = '" + strCanChangeData
                        + "' WHERE userName = '" + userID + "'";
            int i = database.ExecuteNonQuery(sqlCmd, CommandType.Text);
            if (i > 0)
            {
                ShowUserList();
                MessageBox.Show("Cập nhật tài khoản thành công!");
            }
        }

        private void InsertUser(string userID)
        {
            string strAdminFunction;
            string strProductionFunction;
            string strInjectionFunction;
            string strCanChangeData;

            if (chkAdminFunction.Checked) strAdminFunction = "True";
            else strAdminFunction = "False";

            if (chkProducitonFunction.Checked) strProductionFunction = "True";
            else strProductionFunction = "False";

            if (chkInjectionFunction.Checked) strInjectionFunction = "True";
            else strInjectionFunction = "False";

            if (chkCanChangeData.Checked) strCanChangeData = "True";
            else strCanChangeData = "False";

            string sqlCmd = "INSERT INTO SYS_Users(UserName, Password, FullName, "
                + "Department, IsAdminGroup, AccessProduction, AccessInjection, Can_Change_Data) VALUES('"
                + userID + "', EncryptByPassPhrase('DBProd', '" + txtPassword.Text + "'),"
                + "N'" + txtFullName.Text
                + "', N'" + txtDepartment.Text
                + "', '" + strAdminFunction + "', '"
                + strProductionFunction + "', '"
                + strInjectionFunction + "', '"
                + strCanChangeData + "')";
            int i = database.ExecuteNonQuery(sqlCmd, CommandType.Text);
            if (i > 0)
            {
                ShowUserList();
                MessageBox.Show("Thêm tài khoản thành công!");
            }
        }

        private void DeleteUser(string sUserID)
        {
            string sqlCmd = "DELETE FROM SYS_Users WHERE UserName = @userID";
            int i = database.ExecuteNonQuery(sqlCmd, CommandType.Text,
                new string[1] { "userID" }, new string[1] { sUserID },
                new SqlDbType[1] { SqlDbType.NVarChar });
            if (i > 0)
            {
                ShowUserList();
                MessageBox.Show("Đã xóa thành công user: " + sUserID);
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmUserManager_FormClosed(object sender, FormClosedEventArgs e)
        {
            Session.frmUserManager = null;
        }
    }
}