﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TrueTech.VSP.VSPCondensate {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
    internal sealed partial class WellDataSettingDisplay : global::System.Configuration.ApplicationSettingsBase {
        
        private static WellDataSettingDisplay defaultInstance = ((WellDataSettingDisplay)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new WellDataSettingDisplay())));
        
        public static WellDataSettingDisplay Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Attributes {
            get {
                return ((string)(this["Attributes"]));
            }
            set {
                this["Attributes"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CondensateShow {
            get {
                return ((bool)(this["CondensateShow"]));
            }
            set {
                this["CondensateShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CondensateYearShow {
            get {
                return ((bool)(this["CondensateYearShow"]));
            }
            set {
                this["CondensateYearShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CondensateAccShow {
            get {
                return ((bool)(this["CondensateAccShow"]));
            }
            set {
                this["CondensateAccShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool WaterShow {
            get {
                return ((bool)(this["WaterShow"]));
            }
            set {
                this["WaterShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool WaterYearShow {
            get {
                return ((bool)(this["WaterYearShow"]));
            }
            set {
                this["WaterYearShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool WaterAccShow {
            get {
                return ((bool)(this["WaterAccShow"]));
            }
            set {
                this["WaterAccShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool GasShow {
            get {
                return ((bool)(this["GasShow"]));
            }
            set {
                this["GasShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool GasYearShow {
            get {
                return ((bool)(this["GasYearShow"]));
            }
            set {
                this["GasYearShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool GasAccShow {
            get {
                return ((bool)(this["GasAccShow"]));
            }
            set {
                this["GasAccShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool WaterCutShow {
            get {
                return ((bool)(this["WaterCutShow"]));
            }
            set {
                this["WaterCutShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Gor {
            get {
                return ((bool)(this["Gor"]));
            }
            set {
                this["Gor"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool FluidShow {
            get {
                return ((bool)(this["FluidShow"]));
            }
            set {
                this["FluidShow"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool FluidAccShow {
            get {
                return ((bool)(this["FluidAccShow"]));
            }
            set {
                this["FluidAccShow"] = value;
            }
        }
    }
}
