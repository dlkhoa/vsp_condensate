﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmWellDataSettings : Form
    {
        clsControls control = new clsControls();
        WellDataSettingDisplay setting = new WellDataSettingDisplay();
        private char[] delimiters = new char[] { ',' };
        public bool bClosed = false;

        public frmWellDataSettings()
        {
            InitializeComponent();
        }

        private void frmWellDataSettings_Load(object sender, EventArgs e)
        {
            //load cac thuoc tinh tu View vao CheckListBox
            string sqlText = "SELECT COLUMN_NAME FROM information_schema.columns WHERE table_name = 'WELL_CONDENSATE_MONTH'";
            control.FillControls(checkedListBox1, sqlText);

            //danh dau cac thuoc tinh co trong chuoi Setting
            string ss = setting.Attributes;
            string[] sArray = ss.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in sArray)
            {
                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    if (checkedListBox1.Items[i].ToString() == s)
                        checkedListBox1.SetItemChecked(i, true);
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string ss = "";
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                    ss = ss + "," + checkedListBox1.Items[i].ToString();
            }
            setting.Attributes = ss;
            setting.Save();
            bClosed = true;
            this.Close();
            
        }
    }
}
