﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("VSP Condensate Production Management System - Hệ thống quản lý dữ liệu khai thác condensate")]
[assembly: AssemblyDescription("VSP Condensate Production Management System - Hệ thống quản lý dữ liệu khai thác condensate")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TrueTech")]
[assembly: AssemblyProduct("VSP Condensate Production Management System")]
[assembly: AssemblyCopyright("Copyright © 2020 TrueTech")]
[assembly: AssemblyTrademark("VSP Condensate Production Management System")]
[assembly: AssemblyCulture("")]
// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0")]
[assembly: AssemblyFileVersion("1.0")]
