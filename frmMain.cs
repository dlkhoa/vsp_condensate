﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraSplashScreen;
using System;
using System.Windows.Forms;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm {
        public frmMain() {
            InitializeComponent();
            Icon = DevExpress.Utils.ResourceImageHelper.CreateIconFromResourcesEx("TrueTech.VSP.VSPCondensate.AppIcon.ico", typeof(frmMain).Assembly);
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            SplashScreenManager.CloseForm(false);
        }
        protected override void OnShown(EventArgs e) {
            base.OnShown(e);
        }

        #region Init
        public void UpdateText() {
            ribbonControlMain.ApplicationCaption = "VSP Condensate Production Management System - Hệ thống quản lý dữ liệu khai thác condensate";
        }

        void ChangeActiveForm() {
            UpdateText();
        }

        private void frmMain_MdiChildActivate(object sender, System.EventArgs e) {
            ChangeActiveForm();
        }

        #endregion

        #region File

        private void iExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
            Application.Exit();
        }

        #endregion

        private void frmMain_Load(object sender, System.EventArgs e) {
            ribbonControlMain.ForceInitialize();
            //ribbonControlMain.RibbonStyle = RibbonControlStyle.Office2013;

            if (!clsStatic.bIsAdminGroup)
            {
                ribbonPageGroupAdmin.Visible = false;
                ribbonPageGroupCondensate.Visible = (clsStatic.bAccessProduction || clsStatic.bCanChangeData);
                iCondensateImport.Enabled = clsStatic.bCanChangeData;
                iCondensateReportMonthly.Enabled = clsStatic.bAccessProduction;
                iCondensateReportChart.Enabled = clsStatic.bAccessProduction;
            }
        }

        private void openForm<T>(ref T frm) where T : Form
        {
            if (frm == null)
            {
                frm = (T)Activator.CreateInstance(typeof(T));
                frm.MdiParent = this;
                frm.Show();
            }
            frm.WindowState = FormWindowState.Maximized;
            frm.BringToFront();
        }

        private void iAdminUser_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            openForm(ref Session.frmUserManager);
        }

        private void iAdminSettings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            openForm(ref Session.frmGenneralInformation);
        }

        private void iAdminWells_ItemClick(object sender, ItemClickEventArgs e)
        {
            openForm(ref Session.frmWells);
        }

        private void iPassword_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmChangePass fr = new frmChangePass();
            fr.ShowDialog();
        }

        private void iFileAbout_ItemClick(object sender, ItemClickEventArgs e)
        {
            AboutBox1 frm = new AboutBox1();
            frm.ShowDialog();
        }

        private void iCondensateImport_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select Excel file";
            //dlg.Filter = "Excel Workbook (*.xlsx)| *.xlsx |Excel 97-2003 Workbook (*.xls)| *.xls";
            dlg.Filter = "Excel 97-2003 Workbook (*.xls)| *.xls";
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (Session.frmImportMonthlyCondensate != null)
                {
                    Session.frmImportMonthlyCondensate.Close();
                }
                Session.frmImportMonthlyCondensate = new frmImportMonthlyCondensate();
                Session.frmImportMonthlyCondensate.MdiParent = this;
                Session.frmImportMonthlyCondensate.FileName = dlg.FileName;
                Session.frmImportMonthlyCondensate.SafeFileName = dlg.SafeFileName;
                Session.frmImportMonthlyCondensate.WindowState = FormWindowState.Maximized;
                Session.frmImportMonthlyCondensate.Show();
                Session.frmImportMonthlyCondensate.BringToFront();
            }
        }

        private void iCondensateReportMonthly_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmMonthExport frmSelect = new frmMonthExport();
            frmSelect.DataType = "CONDENSATE";
            DialogResult result = frmSelect.ShowDialog();
            if (result == DialogResult.OK)
            {
                frmExportMonthlyCondensate frm = new frmExportMonthlyCondensate();
                frm.month = frmSelect.month;
                frm.year = frmSelect.year;
                frm.ShowDialog();
            }
        }

        private void iCondensateReportChart_ItemClick(object sender, ItemClickEventArgs e)
        {
            openForm(ref Session.frmWellData);

        }
    }
}
