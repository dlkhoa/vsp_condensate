﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace TrueTech.VSP.VSPCondensate
{
    class clsDbProdQuery
    {
        clsDatabases database = new clsDatabases();

        public void UpdateTableVSP_FLUID_TOTAL_FromProduction(string minBeforeMonth) //dinh dang mm/dd/yyyy
        {
            string sql = "SELECT DISTINCT CONVERT(VARCHAR, MONTH, 101), MONTH FROM VSP_PRODUCTION"
                + " WHERE MONTH > CONVERT(DATETIME, '" + minBeforeMonth + "', 101) "
                + " ORDER BY MONTH";
            DataTable dtMonth = database.FillDataTable(sql, CommandType.Text);
            foreach (DataRow drMonth in dtMonth.Rows)
            {
                string curMonth = drMonth[0].ToString();

                //--lay ID tu bang VSP_DENSITY---
                sql = "SELECT DISTINCT D.ID, D.FIELD_ID, D.RESERVOIR_ID, D.DOME_ID,"
                    + " D.OIL_VOLUME, D.WATER_VOLUME FROM VSP_DENSITY D";
                DataTable dtID = database.FillDataTable(sql, CommandType.Text);

                //--duyet cac dong trong bang dtID
                foreach (DataRow drID in dtID.Rows)
                {
                    string sDID = drID[0].ToString();
                    string field_id = drID[1].ToString();
                    string reservoir_id = drID[2].ToString();
                    string dome_id = drID[3].ToString();
                    string oil_volume = drID[4].ToString();
                    string water_volume = drID[5].ToString();

                    //tim thang ngay truoc thang curMonth
                    sql = "SELECT DISTINCT TOP 1 CONVERT(VARCHAR, MONTH, 101), MONTH"
                        + " FROM VSP_PRODUCTION "
                        + " WHERE MONTH < CONVERT(DATETIME, '" + curMonth + "', 101) "
                        + " ORDER BY MONTH DESC";

                    string beforeMonth = "";
                    foreach (DataRow drBeforeMonth in database.FillDataTable(sql, CommandType.Text).Rows)
                    {
                        beforeMonth = drBeforeMonth[0].ToString();
                    }

                    //-- query tong cong don cua chat long khai thac THANG TRUOC curMonth---
                    DataTable dtFluidBefore = database.FillDataTable("SELECT FT.FLUID_TOTAL "
                        + " FROM VSP_FLUID_TOTAL FT, VSP_DENSITY D"
                        + " WHERE FT.DID = D.ID AND D.FIELD_ID = '" + field_id + "'"
                        + " AND D.RESERVOIR_ID = " + reservoir_id
                        + " AND D.DOME_ID = " + dome_id
                        + " AND FT.MONTH = CONVERT(DATETIME, '" + beforeMonth + "', 101)", CommandType.Text);

                    string fluidBefore = "0";
                    foreach (DataRow drFluidBefore in dtFluidBefore.Rows)
                    {
                        fluidBefore = drFluidBefore[0].ToString();
                    }

                    //--tinh tong chat long thang nay--
                    DataTable dtFluidCurrMonth = database.FillDataTable("SELECT SUM(P.OIL), SUM(P.WATER), F.VSP_PERCENT"
                        + " FROM VSP_PRODUCTION P, VSP_FIELD_HDR F"
                        + " WHERE P.FIELD_ID = F.FIELD_ID AND P.FIELD_ID = '" + field_id
                        + "' AND P.RESERVOIR_ID = " + reservoir_id
                        + " AND P.DOME_ID = " + dome_id
                        + " AND P.MONTH = CONVERT(DATETIME,'" + curMonth + "', 101)"
                        + " GROUP BY F.VSP_PERCENT"
                        , CommandType.Text);
                    string oilCurrMonth = "0";
                    string waterCurrMonth = "0";
                    string vsp_percent = "0";
                    foreach (DataRow drFluidCurrMonth in dtFluidCurrMonth.Rows)
                    {
                        oilCurrMonth = drFluidCurrMonth[0].ToString();
                        waterCurrMonth = drFluidCurrMonth[1].ToString();
                        vsp_percent = drFluidCurrMonth[2].ToString();
                    }
                    if (oilCurrMonth.Trim().Length == 0) oilCurrMonth = "0";
                    if (waterCurrMonth.Trim().Length == 0) waterCurrMonth = "0";

                    //--tinh tong chat long cong don den thang nay
                    string fluid_acc = (Convert.ToDouble(fluidBefore)
                        + Math.Round(Convert.ToDouble(oilCurrMonth) * Convert.ToDouble(vsp_percent) * Convert.ToDouble(oil_volume) / 100
                        + Convert.ToDouble(waterCurrMonth) * Convert.ToDouble(vsp_percent) * Convert.ToDouble(water_volume) / 100,
                        0, MidpointRounding.AwayFromZero)).ToString();

                    //--kiem tra su ton tai trong bang VSP_FLUID_TOTAL--
                    string s = "SELECT * FROM VSP_FLUID_TOTAL WHERE DID = " + sDID
                        + " AND MONTH = CONVERT(DATETIME,'" + curMonth + "', 101)";
                    DataTable dtFluidTotal = database.FillDataTable(s, CommandType.Text);
                    string sqlQuery;
                    if (dtFluidTotal.Rows.Count == 0)
                    {
                        //--neu chua co du lieu thi INSERT 
                        sqlQuery = "INSERT INTO VSP_FLUID_TOTAL(DID, MONTH, FLUID_TOTAL, FLUID_INJ_TOTAL)"
                            + " VALUES(" + sDID
                            + " , CONVERT(DATETIME,'" + curMonth + "', 101)"
                            + " , " + fluid_acc + ", 0)";
                    }
                    else
                    {
                        //--neu co du lieu roi thi UPDATE
                        sqlQuery = "UPDATE VSP_FLUID_TOTAL SET"
                            + " FLUID_TOTAL = " + fluid_acc
                            + " WHERE DID = " + sDID
                        + " AND MONTH = CONVERT(DATETIME,'" + curMonth + "', 101)";
                    }

                    int i = database.ExecuteNonQuery(sqlQuery, CommandType.Text);
                }
            }
        }

        public void UpdateTableVSP_FLUID_TOTAL_FromInjection(string minBeforeMonth)
        {
            string sql = "SELECT DISTINCT CONVERT(VARCHAR, MONTH, 101), MONTH FROM VSP_INJECTION WHERE MONTH > CONVERT(DATETIME, '"
                + minBeforeMonth + "', 101) ORDER BY MONTH";
            DataTable dtMonth = database.FillDataTable(sql, CommandType.Text);
            foreach (DataRow drMonth in dtMonth.Rows)
            {
                string curMonth = drMonth[0].ToString();

                //--lay ID tu bang VSP_DENSITY---
                sql = "SELECT DISTINCT D.ID, D.FIELD_ID, D.RESERVOIR_ID, D.DOME_ID, D.WATER_INJ_VOLUME FROM VSP_DENSITY D";
                DataTable dtID = database.FillDataTable(sql, CommandType.Text);

                //--duyet cac dong trong bang dtID
                foreach (DataRow drID in dtID.Rows)
                {
                    string sDID = drID[0].ToString();
                    string field_id = drID[1].ToString();
                    string reservoir_id = drID[2].ToString();
                    string dome_id = drID[3].ToString();
                    string water_inj_volume = drID[4].ToString();

                    //tim thang ngay truoc thang curMonth
                    sql = "SELECT DISTINCT TOP 1 CONVERT(VARCHAR, MONTH, 101), MONTH FROM VSP_INJECTION WHERE MONTH < CONVERT(DATETIME, '"
                                + curMonth + "', 101) ORDER BY MONTH DESC";

                    string beforeMonth = "";
                    foreach (DataRow drBeforeMonth in database.FillDataTable(sql, CommandType.Text).Rows)
                    {
                        beforeMonth = drBeforeMonth[0].ToString();
                    }

                    //-- tinh tong cong don cua chat long khai thac THANG TRUOC ---
                    DataTable dtFluidBefore = database.FillDataTable("SELECT FT.FLUID_INJ_TOTAL "
                        + " FROM VSP_FLUID_TOTAL FT, VSP_DENSITY D"
                        + " WHERE FT.DID = D.ID AND D.FIELD_ID = '" + field_id + "'"
                        + " AND D.RESERVOIR_ID = " + reservoir_id
                        + " AND D.DOME_ID = " + dome_id
                        + " AND FT.MONTH = CONVERT(DATETIME, '" + beforeMonth + "', 101)", CommandType.Text);

                    string fluidInjBefore = "0";
                    foreach (DataRow drFluidBefore in dtFluidBefore.Rows)
                    {
                        fluidInjBefore = drFluidBefore[0].ToString();
                    }

                    //--tinh tong chat long thang nay--
                    DataTable dtFluidCurrMonth = database.FillDataTable("SELECT SUM(WATER)"
                        + " FROM VSP_INJECTION"
                        + " WHERE FIELD_ID = '" + field_id
                        + "' AND RESERVOIR_ID = " + reservoir_id
                        + " AND DOME_ID = " + dome_id
                        + " AND MONTH = CONVERT(DATETIME,'" + curMonth + "', 101)"
                        , CommandType.Text);
                    string waterCurrMonth = "0";
                    foreach (DataRow drFluidCurrMonth in dtFluidCurrMonth.Rows)
                    {
                        waterCurrMonth = drFluidCurrMonth[0].ToString();
                    }
                    if (waterCurrMonth.Trim().Length == 0) waterCurrMonth = "0";

                    //--tinh tong chat long cong don den thang nay
                    string fluid_inj_acc = (Convert.ToDouble(fluidInjBefore)
                        + Math.Round(Convert.ToDouble(waterCurrMonth) * Convert.ToDouble(water_inj_volume),
                        0, MidpointRounding.AwayFromZero)).ToString();

                    //--kiem tra su ton tai trong bang VSP_FLUID_TOTAL--
                    string s = "SELECT * FROM VSP_FLUID_TOTAL WHERE DID = " + sDID
                        + " AND MONTH = CONVERT(DATETIME,'" + curMonth + "', 101)";
                    DataTable dtFluidTotal = database.FillDataTable(s, CommandType.Text);
                    string sqlQuery;
                    if (dtFluidTotal.Rows.Count == 0)
                    {
                        //--neu chua co du lieu thi INSERT 
                        sqlQuery = "INSERT INTO VSP_FLUID_TOTAL(DID, MONTH, FLUID_TOTAL, FLUID_INJ_TOTAL)"
                            + " VALUES(" + sDID
                            + " , CONVERT(DATETIME,'" + curMonth + "', 101)"
                            + " , 0, " + fluid_inj_acc + ")";
                    }
                    else
                    {
                        //--neu co du lieu roi thi UPDATE
                        sqlQuery = "UPDATE VSP_FLUID_TOTAL SET"
                            + " FLUID_INJ_TOTAL = " + fluid_inj_acc
                            + " WHERE DID = " + sDID
                        + " AND MONTH = CONVERT(DATETIME,'" + curMonth + "', 101)";
                    }

                    int i = database.ExecuteNonQuery(sqlQuery, CommandType.Text);
                }
            }
        }

        public bool CheckIfRowExistInProduction(string field_id, string uwi, string reservoir_id,
            string dome_id, string zone_id, string method_id, string status, DateTime month)
        {
            clsDatabases database = new clsDatabases();
            string sql = "SELECT * FROM VSP_PRODUCTION WHERE "
                + "FIELD_ID = @FIELD_ID AND UWI = @UWI AND "
                + "RESERVOIR_ID = @RESERVOIR_ID AND DOME_ID = @DOME_ID AND "
                + "ZONE_ID = @ZONE_ID AND METHOD_ID = @METHOD_ID AND "
                + "STATUS = @STATUS AND MONTH = @MONTH";
            string[] paraCollections = new string[8] {"FIELD_ID",  "UWI", "RESERVOIR_ID", "DOME_ID",
                "ZONE_ID", "METHOD_ID", "STATUS", "MONTH"};

            object[] objCollections = new object[8] {field_id, uwi, reservoir_id, dome_id, 
                zone_id, method_id, status, month};

            SqlDbType[] sqlType = new SqlDbType[8] {SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.DateTime};

            DataTable table = database.FillDataTable(sql, CommandType.Text, paraCollections, objCollections, sqlType);
            if (table.Rows.Count > 0) return true;
            else return false;
        }


        public bool CheckIfRowExistInInjection(string field_id, string uwi, string reservoir_id,
            string dome_id, string zone_id, DateTime month)
        {
            clsDatabases database = new clsDatabases();
            string sql = "SELECT * FROM VSP_INJECTION WHERE "
                + "FIELD_ID = @FIELD_ID AND UWI = @UWI AND "
                + "RESERVOIR_ID = @RESERVOIR_ID AND DOME_ID = @DOME_ID AND "
                + "ZONE_ID = @ZONE_ID AND MONTH = @MONTH";
            string[] paraCollections = new string[6] {"FIELD_ID",  "UWI", "RESERVOIR_ID", "DOME_ID",
                "ZONE_ID", "MONTH"};

            object[] objCollections = new object[6] {field_id, uwi, reservoir_id, dome_id, 
                zone_id, month};

            SqlDbType[] sqlType = new SqlDbType[6] {SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.DateTime};

            DataTable table = database.FillDataTable(sql, CommandType.Text, paraCollections, objCollections, sqlType);
            if (table.Rows.Count > 0) return true;
            else return false;
        }


        public bool CheckIfRowExistInCondensate(string field_id, string uwi, string reservoir_id,
            string dome_id, string zone_id, string method_id, string status, DateTime month)
        {
            clsDatabases database = new clsDatabases();
            string sql = "SELECT * FROM VSP_CONDENSATE WHERE "
                + "FIELD_ID = @FIELD_ID AND UWI = @UWI AND "
                + "RESERVOIR_ID = @RESERVOIR_ID AND DOME_ID = @DOME_ID AND "
                + "ZONE_ID = @ZONE_ID AND METHOD_ID = @METHOD_ID AND "
                + "MONTH = @MONTH";
                //+"STATUS = @STATUS AND MONTH = @MONTH";
            string[] paraCollections = new string[7] {"FIELD_ID",  "UWI", "RESERVOIR_ID", "DOME_ID",
                "ZONE_ID", "METHOD_ID", "MONTH"};

            object[] objCollections = new object[7] {field_id, uwi, reservoir_id, dome_id,
                zone_id, method_id, month};

            SqlDbType[] sqlType = new SqlDbType[7] {SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.DateTime};

            DataTable table = database.FillDataTable(sql, CommandType.Text, paraCollections, objCollections, sqlType);
            if (table.Rows.Count > 0) return true;
            else return false;
        }

        public void RunAccumulateProduction(string uwi, string reservoir_id, string dome_id,
            string zone_id, string method_id, DateTime dateMonth, ref string msgReport)
        {
            SqlCommand cmd = new SqlCommand("VSP_ACC_SP_NEW");
            cmd.Connection = clsConnection.sqlConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "VSP_ACC_SP_NEW";

            try
            {
                cmd.Parameters.Add(new SqlParameter("@i_uwi", uwi));
                cmd.Parameters.Add(new SqlParameter("@i_reservoir_id", reservoir_id));
                cmd.Parameters.Add(new SqlParameter("@i_dome_id", dome_id));
                cmd.Parameters.Add(new SqlParameter("@i_zone_id", zone_id));
                cmd.Parameters.Add(new SqlParameter("@i_method_id", method_id));
                cmd.Parameters.Add(new SqlParameter("@i_month", dateMonth));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                msgReport += "\r\n" + ex.Message;
            }
        }

        public void RunAccumulateInjection(string uwi, string reservoir_id, string dome_id, 
            string zone_id, DateTime dateMonth, ref string msgReport)
        {
            SqlCommand cmd = new SqlCommand("VSP_INJ_ACC_SP");
            cmd.Connection = clsConnection.sqlConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "VSP_INJ_ACC_SP";

            try
            {
                cmd.Parameters.Add(new SqlParameter("@i_uwi", uwi));
                cmd.Parameters.Add(new SqlParameter("@i_reservoir_id", reservoir_id));
                cmd.Parameters.Add(new SqlParameter("@i_dome_id", dome_id));
                cmd.Parameters.Add(new SqlParameter("@i_zone_id", zone_id));
                cmd.Parameters.Add(new SqlParameter("@i_month", dateMonth));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                msgReport += "\r\n" + ex.Message;
            }
        }

        public void RunAccumulateCondensate(string uwi, string reservoir_id, string dome_id,
            string zone_id, string method_id, DateTime dateMonth, ref string msgReport)
        {
            SqlCommand cmd = new SqlCommand("VSP_CONDENSATE_ACC_SP");
            cmd.Connection = clsConnection.sqlConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "VSP_CONDENSATE_ACC_SP";

            try
            {
                cmd.Parameters.Add(new SqlParameter("@i_uwi", uwi));
                cmd.Parameters.Add(new SqlParameter("@i_reservoir_id", reservoir_id));
                cmd.Parameters.Add(new SqlParameter("@i_dome_id", dome_id));
                cmd.Parameters.Add(new SqlParameter("@i_zone_id", zone_id));
                cmd.Parameters.Add(new SqlParameter("@i_method_id", method_id));
                cmd.Parameters.Add(new SqlParameter("@i_month", dateMonth));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                msgReport += "\r\n" + ex.Message;
            }
        }

        public int InsertToProduction(string field_id, string platform, string uwi, string reservoir_id,
            string dome_id, string zone_id, string method_id, string status, DateTime dateMonth,
            string status_id, double water_cut, double oil, double oil_year, double oil_acc,
            double water, double water_year, double water_acc, double gas, double gas_year, double gas_acc,
            object choke_size_1, object choke_size_2, double prod_hours, double fill_up_hours, double suspended_hours,
            double workday_year, object pressure_1, object pressure_2, double gor, string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[34] {"FIELD_ID", "PLATFORM", "UWI", "RESERVOIR_ID", 
                "DOME_ID", "ZONE_ID", "METHOD_ID", "STATUS", "MONTH", "STATUS_ID", "WATER_CUT", 
                "OIL", "OIL_YEAR", "OIL_ACC", "WATER", "WATER_YEAR", "WATER_ACC", 
                "GAS", "GAS_YEAR", "GAS_ACC", "CHOKE_SIZE_1", "CHOKE_SIZE_2", 
                "PROD_HOURS", "FILL_UP_HOURS", "SUSPENDED_HOURS", "WORKDAY_YEAR", 
                "PRESSURE_1", "PRESSURE_2", "GOR", "REMARKS",
                "INSERTED_DATE", "INSERTED_BY", "LAST_UPDATE", "UPDATED_BY"};

            object[] objCollections = new object[34] {field_id, platform, uwi, reservoir_id, 
                dome_id, zone_id, method_id, status, dateMonth, status_id, water_cut, 
                oil, oil_year, oil_acc, water, water_year, water_acc, 
                gas, gas_year, gas_acc, choke_size_1, choke_size_2 ,
                prod_hours, fill_up_hours, suspended_hours, workday_year, 
                pressure_1, pressure_2, gor, remarks,
                DateTime.Now, clsStatic.strUser,
                DateTime.Now, clsStatic.strUser};

            SqlDbType[] sqlType = new SqlDbType[34] {SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.DateTime, SqlDbType.Int, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar, 
                SqlDbType.DateTime, SqlDbType.NVarChar, SqlDbType.DateTime, SqlDbType.NVarChar};

            string sql = "INSERT INTO VSP_PRODUCTION (FIELD_ID, PLATFORM, UWI, RESERVOIR_ID, DOME_ID, "
                + "ZONE_ID, METHOD_ID, STATUS, MONTH, STATUS_ID, WATER_CUT, "
                + "OIL, OIL_YEAR, OIL_ACC, WATER, WATER_YEAR, WATER_ACC, GAS, GAS_YEAR, GAS_ACC, "
                + "CHOKE_SIZE_1, CHOKE_SIZE_2, PROD_HOURS, FILL_UP_HOURS, "
                + "SUSPENDED_HOURS, WORKDAY_YEAR, PRESSURE_1, PRESSURE_2, "
                + "GOR, REMARKS, INSERTED_DATE, INSERTED_BY, LAST_UPDATE, UPDATED_BY) VALUES( "
                + "@FIELD_ID, @PLATFORM, @UWI, @RESERVOIR_ID, @DOME_ID, "
                + "@ZONE_ID, @METHOD_ID, "
                + "@STATUS, @MONTH, @STATUS_ID, @WATER_CUT, "
                + "@OIL, @OIL_YEAR, @OIL_ACC, @WATER, @WATER_YEAR, @WATER_ACC, @GAS, @GAS_YEAR, @GAS_ACC, "
                + "@CHOKE_SIZE_1, @CHOKE_SIZE_2, @PROD_HOURS, @FILL_UP_HOURS, "
                + "@SUSPENDED_HOURS, @WORKDAY_YEAR, @PRESSURE_1, @PRESSURE_2, "
                + "@GOR, @REMARKS, @INSERTED_DATE, @INSERTED_BY, @LAST_UPDATE, @UPDATED_BY)";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }

        public int UpdateToProduction(string field_id, string platform, string uwi, string reservoir_id,
            string dome_id, string zone_id, string method_id, string status, DateTime dateMonth,
            string status_id, double water_cut, double oil, double oil_year, double oil_acc,
            double water, double water_year, double water_acc, double gas, double gas_year, double gas_acc,
            object choke_size_1, object choke_size_2, double prod_hours, double fill_up_hours, double suspended_hours,
            double workday_year, object pressure_1, object pressure_2, double gor, string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[34] {"FIELD_ID", "PLATFORM", "UWI", "RESERVOIR_ID", 
                "DOME_ID", "ZONE_ID", "METHOD_ID", "STATUS", "MONTH", "STATUS_ID", "WATER_CUT", 
                "OIL", "OIL_YEAR", "OIL_ACC", "WATER", "WATER_YEAR", "WATER_ACC", 
                "GAS", "GAS_YEAR", "GAS_ACC", "CHOKE_SIZE_1", "CHOKE_SIZE_2", 
                "PROD_HOURS", "FILL_UP_HOURS", "SUSPENDED_HOURS", "WORKDAY_YEAR", 
                "PRESSURE_1", "PRESSURE_2", "GOR", "REMARKS",
                "INSERTED_DATE", "INSERTED_BY", "LAST_UPDATE", "UPDATED_BY"};

            object[] objCollections = new object[34] {field_id, platform, uwi, reservoir_id, 
                dome_id, zone_id, method_id, status, dateMonth, status_id, water_cut, 
                oil, oil_year, oil_acc, water, water_year, water_acc, 
                gas, gas_year, gas_acc, choke_size_1, choke_size_2 ,
                prod_hours, fill_up_hours, suspended_hours, workday_year, 
                pressure_1, pressure_2, gor, remarks,
                DateTime.Now, clsStatic.strUser,
                DateTime.Now, clsStatic.strUser};

            SqlDbType[] sqlType = new SqlDbType[34] {SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.DateTime, SqlDbType.Int, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar, 
                SqlDbType.DateTime, SqlDbType.NVarChar, SqlDbType.DateTime, SqlDbType.NVarChar};

            string sql = "UPDATE VSP_PRODUCTION SET PLATFORM = @PLATFORM, STATUS_ID = @STATUS_ID, WATER_CUT = @WATER_CUT, "
                + "OIL = @OIL, OIL_YEAR = @OIL_YEAR, OIL_ACC = @OIL_ACC, "
                + "WATER = @WATER, WATER_YEAR = @WATER_YEAR, WATER_ACC = @WATER_ACC, "
                + "GAS = @GAS, GAS_YEAR = @GAS_YEAR, GAS_ACC = @GAS_ACC, "
                + "CHOKE_SIZE_1 = @CHOKE_SIZE_1, CHOKE_SIZE_2 = @CHOKE_SIZE_2, "
                + "PROD_HOURS = @PROD_HOURS, FILL_UP_HOURS = @FILL_UP_HOURS, "
                + "SUSPENDED_HOURS = @SUSPENDED_HOURS, WORKDAY_YEAR = @WORKDAY_YEAR, "
                + "PRESSURE_1 = @PRESSURE_1, PRESSURE_2 = @PRESSURE_2, "
                + "GOR = @GOR, REMARKS = @REMARKS, "
                + "INSERTED_DATE = @INSERTED_DATE, INSERTED_BY = @INSERTED_BY, "
                + "LAST_UPDATE = @LAST_UPDATE, UPDATED_BY = @UPDATED_BY "
                + "WHERE FIELD_ID = @FIELD_ID AND UWI = @UWI AND RESERVOIR_ID = @RESERVOIR_ID "
                + "AND DOME_ID = @DOME_ID AND ZONE_ID = @ZONE_ID AND METHOD_ID = @METHOD_ID  "
                + "AND STATUS = @STATUS AND MONTH = @MONTH";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }


        public int InsertToInjection(string field_id, string platform, string uwi, string reservoir_id,
            string dome_id, string zone_id, DateTime dateMonth,
            double water, double water_year, double water_acc, 
            double prod_hours, double suspended_hours,
            double workday_year, object pressure_1, object pressure_2, 
            string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[20] {"FIELD_ID", "PLATFORM", "UWI", "RESERVOIR_ID", 
                "DOME_ID", "ZONE_ID", "MONTH", 
                "WATER", "WATER_YEAR", "WATER_ACC",
                "PROD_HOURS", "SUSPENDED_HOURS", "WORKDAY_YEAR", 
                "PRESSURE_1", "PRESSURE_2", "REMARKS",
                "INSERTED_DATE", "INSERTED_BY", "LAST_UPDATE", "UPDATED_BY"};

            object[] objCollections = new object[20] {field_id, platform, uwi, reservoir_id, 
                dome_id, zone_id, dateMonth,
                water, water_year, water_acc,  
                prod_hours, suspended_hours, workday_year, 
                pressure_1, pressure_2, remarks,
                DateTime.Now, clsStatic.strUser,
                DateTime.Now, clsStatic.strUser};

            SqlDbType[] sqlType = new SqlDbType[20] {SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.DateTime,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar, 
                SqlDbType.DateTime, SqlDbType.NVarChar, SqlDbType.DateTime, SqlDbType.NVarChar};

            string sql = "INSERT INTO VSP_INJECTION (FIELD_ID, PLATFORM, UWI, RESERVOIR_ID, DOME_ID, "
                + "ZONE_ID, MONTH, WATER, WATER_YEAR, WATER_ACC, PROD_HOURS, SUSPENDED_HOURS, WORKDAY_YEAR, "
                + "PRESSURE_1, PRESSURE_2, REMARKS, INSERTED_DATE, INSERTED_BY, LAST_UPDATE, UPDATED_BY) VALUES( "
                + "@FIELD_ID, @PLATFORM, @UWI, @RESERVOIR_ID, @DOME_ID, @ZONE_ID, @MONTH, "
                + "@WATER, @WATER_YEAR, @WATER_ACC, "
                + "@PROD_HOURS, @SUSPENDED_HOURS, @WORKDAY_YEAR, @PRESSURE_1, @PRESSURE_2, "
                + "@REMARKS, @INSERTED_DATE, @INSERTED_BY, @LAST_UPDATE, @UPDATED_BY)";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }

        public int UpdateToInjection(string field_id, string platform, string uwi, string reservoir_id,
            string dome_id, string zone_id, DateTime dateMonth,
            double water, double water_year, double water_acc,
            double prod_hours, double suspended_hours,
            double workday_year, object pressure_1, object pressure_2, 
            string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[20] {"FIELD_ID", "PLATFORM", "UWI", "RESERVOIR_ID", 
                "DOME_ID", "ZONE_ID", "MONTH", 
                "WATER", "WATER_YEAR", "WATER_ACC",
                "PROD_HOURS", "SUSPENDED_HOURS", "WORKDAY_YEAR", 
                "PRESSURE_1", "PRESSURE_2", "REMARKS",
                "INSERTED_DATE", "INSERTED_BY", "LAST_UPDATE", "UPDATED_BY"};

            object[] objCollections = new object[20] {field_id, platform, uwi, reservoir_id, 
                dome_id, zone_id, dateMonth,
                water, water_year, water_acc,  
                prod_hours, suspended_hours, workday_year, 
                pressure_1, pressure_2, remarks,
                DateTime.Now, clsStatic.strUser,
                DateTime.Now, clsStatic.strUser};

            SqlDbType[] sqlType = new SqlDbType[20] {SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.DateTime,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar, 
                SqlDbType.DateTime, SqlDbType.NVarChar, SqlDbType.DateTime, SqlDbType.NVarChar};

            string sql = "UPDATE VSP_INJECTION SET PLATFORM = @PLATFORM, "
                + "WATER = @WATER, WATER_YEAR = @WATER_YEAR, WATER_ACC = @WATER_ACC,"
                + "PROD_HOURS = @PROD_HOURS, SUSPENDED_HOURS = @SUSPENDED_HOURS, WORKDAY_YEAR = @WORKDAY_YEAR, "
                + "PRESSURE_1 = @PRESSURE_1, PRESSURE_2 = @PRESSURE_2, REMARKS = @REMARKS, "
                + "UPDATED_BY = @UPDATED_BY, LAST_UPDATE = @LAST_UPDATE"
                + " WHERE FIELD_ID = @FIELD_ID AND UWI = @UWI "
                + " AND RESERVOIR_ID = @RESERVOIR_ID AND DOME_ID = @DOME_ID "
                + " AND ZONE_ID = @ZONE_ID AND MONTH = @MONTH";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }

        public int InsertToCondensate(string field_id, string platform, string uwi, string reservoir_id,
            string dome_id, string zone_id, string method_id, string status, DateTime dateMonth,
            string status_id, double water_cut, double condensate, double condensate_year, double condensate_acc,
            double water, double water_year, double water_acc, double gas, double gas_year, double gas_acc,
            object choke_size_1, object choke_size_2, double prod_hours, double fill_up_hours, double suspended_hours,
            double workday_year, object pressure_1, object pressure_2, double gor, string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[34] {"FIELD_ID", "PLATFORM", "UWI", "RESERVOIR_ID",
                "DOME_ID", "ZONE_ID", "METHOD_ID", "STATUS", "MONTH", "STATUS_ID", "WATER_CUT",
                "CONDENSATE", "CONDENSATE_YEAR", "CONDENSATE_ACC", "WATER", "WATER_YEAR", "WATER_ACC",
                "GAS", "GAS_YEAR", "GAS_ACC", "CHOKE_SIZE_1", "CHOKE_SIZE_2",
                "PROD_HOURS", "FILL_UP_HOURS", "SUSPENDED_HOURS", "WORKDAY_YEAR",
                "PRESSURE_1", "PRESSURE_2", "GOR", "REMARKS",
                "INSERTED_DATE", "INSERTED_BY", "LAST_UPDATE", "UPDATED_BY"};

            object[] objCollections = new object[34] {field_id, platform, uwi, reservoir_id,
                dome_id, zone_id, method_id, status, dateMonth, status_id, water_cut,
                condensate, condensate_year, condensate_acc, water, water_year, water_acc,
                gas, gas_year, gas_acc, choke_size_1, choke_size_2 ,
                prod_hours, fill_up_hours, suspended_hours, workday_year,
                pressure_1, pressure_2, gor, remarks,
                DateTime.Now, clsStatic.strUser,
                DateTime.Now, clsStatic.strUser};

            SqlDbType[] sqlType = new SqlDbType[34] {SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.DateTime, SqlDbType.Int, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar,
                SqlDbType.DateTime, SqlDbType.NVarChar, SqlDbType.DateTime, SqlDbType.NVarChar};

            string sql = "INSERT INTO VSP_CONDENSATE (FIELD_ID, PLATFORM, UWI, RESERVOIR_ID, DOME_ID, "
                + "ZONE_ID, METHOD_ID, STATUS, MONTH, STATUS_ID, WATER_CUT, "
                + "CONDENSATE, CONDENSATE_YEAR, CONDENSATE_ACC, WATER, WATER_YEAR, WATER_ACC, GAS, GAS_YEAR, GAS_ACC, "
                + "CHOKE_SIZE_1, CHOKE_SIZE_2, PROD_HOURS, FILL_UP_HOURS, "
                + "SUSPENDED_HOURS, WORKDAY_YEAR, PRESSURE_1, PRESSURE_2, "
                + "GOR, REMARKS, INSERTED_DATE, INSERTED_BY, LAST_UPDATE, UPDATED_BY) VALUES( "
                + "@FIELD_ID, @PLATFORM, @UWI, @RESERVOIR_ID, @DOME_ID, "
                + "@ZONE_ID, @METHOD_ID, "
                + "@STATUS, @MONTH, @STATUS_ID, @WATER_CUT, "
                + "@CONDENSATE, @CONDENSATE_YEAR, @CONDENSATE_ACC, @WATER, @WATER_YEAR, @WATER_ACC, @GAS, @GAS_YEAR, @GAS_ACC, "
                + "@CHOKE_SIZE_1, @CHOKE_SIZE_2, @PROD_HOURS, @FILL_UP_HOURS, "
                + "@SUSPENDED_HOURS, @WORKDAY_YEAR, @PRESSURE_1, @PRESSURE_2, "
                + "@GOR, @REMARKS, @INSERTED_DATE, @INSERTED_BY, @LAST_UPDATE, @UPDATED_BY)";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }

        public int UpdateToCondensate(string field_id, string platform, string uwi, string reservoir_id,
            string dome_id, string zone_id, string method_id, string status, DateTime dateMonth,
            string status_id, double water_cut, double condensate, double condensate_year, double condensate_acc,
            double water, double water_year, double water_acc, double gas, double gas_year, double gas_acc,
            object choke_size_1, object choke_size_2, double prod_hours, double fill_up_hours, double suspended_hours,
            double workday_year, object pressure_1, object pressure_2, double gor, string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[34] {"FIELD_ID", "PLATFORM", "UWI", "RESERVOIR_ID",
                "DOME_ID", "ZONE_ID", "METHOD_ID", "STATUS", "MONTH", "STATUS_ID", "WATER_CUT",
                "CONDENSATE", "CONDENSATE_YEAR", "CONDENSATE_ACC", "WATER", "WATER_YEAR", "WATER_ACC",
                "GAS", "GAS_YEAR", "GAS_ACC", "CHOKE_SIZE_1", "CHOKE_SIZE_2",
                "PROD_HOURS", "FILL_UP_HOURS", "SUSPENDED_HOURS", "WORKDAY_YEAR",
                "PRESSURE_1", "PRESSURE_2", "GOR", "REMARKS",
                "INSERTED_DATE", "INSERTED_BY", "LAST_UPDATE", "UPDATED_BY"};

            object[] objCollections = new object[34] {field_id, platform, uwi, reservoir_id,
                dome_id, zone_id, method_id, status, dateMonth, status_id, water_cut,
                condensate, condensate_year, condensate_acc, water, water_year, water_acc,
                gas, gas_year, gas_acc, choke_size_1, choke_size_2 ,
                prod_hours, fill_up_hours, suspended_hours, workday_year,
                pressure_1, pressure_2, gor, remarks,
                DateTime.Now, clsStatic.strUser,
                DateTime.Now, clsStatic.strUser};

            SqlDbType[] sqlType = new SqlDbType[34] {SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.DateTime, SqlDbType.Int, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar,
                SqlDbType.DateTime, SqlDbType.NVarChar, SqlDbType.DateTime, SqlDbType.NVarChar};

            string sql = "UPDATE VSP_CONDENSATE SET PLATFORM = @PLATFORM, STATUS = @STATUS, STATUS_ID = @STATUS_ID, WATER_CUT = @WATER_CUT, "
                + "CONDENSATE = @CONDENSATE, CONDENSATE_YEAR = @CONDENSATE_YEAR, CONDENSATE_ACC = @CONDENSATE_ACC, "
                + "WATER = @WATER, WATER_YEAR = @WATER_YEAR, WATER_ACC = @WATER_ACC, "
                + "GAS = @GAS, GAS_YEAR = @GAS_YEAR, GAS_ACC = @GAS_ACC, "
                + "CHOKE_SIZE_1 = @CHOKE_SIZE_1, CHOKE_SIZE_2 = @CHOKE_SIZE_2, "
                + "PROD_HOURS = @PROD_HOURS, FILL_UP_HOURS = @FILL_UP_HOURS, "
                + "SUSPENDED_HOURS = @SUSPENDED_HOURS, WORKDAY_YEAR = @WORKDAY_YEAR, "
                + "PRESSURE_1 = @PRESSURE_1, PRESSURE_2 = @PRESSURE_2, "
                + "GOR = @GOR, REMARKS = @REMARKS, "
                + "INSERTED_DATE = @INSERTED_DATE, INSERTED_BY = @INSERTED_BY, "
                + "LAST_UPDATE = @LAST_UPDATE, UPDATED_BY = @UPDATED_BY "
                + "WHERE FIELD_ID = @FIELD_ID AND UWI = @UWI AND RESERVOIR_ID = @RESERVOIR_ID "
                + "AND DOME_ID = @DOME_ID AND ZONE_ID = @ZONE_ID AND METHOD_ID = @METHOD_ID  "
                + "AND MONTH = @MONTH";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }

        public int InsertToVSPOGPMSuction(DateTime date, string data_date, string field_id, string platform, string uwi, 
            string dome_id, string method_id, string choke_size, string whp, string bhp, string rp,
            string cp_1, string cp_2, string cp_3,
            string prod_hours, string temp, string water_cut, string oil, string fluid, string gas,
            string gaslift, string gaslift_plan, string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[23] {"DATE", "DATA_DATE", "FIELD_ID", "PLATFORM", "UWI", 
                "DOME_ID", "METHOD_ID", "CHOKE_SIZE", "WHP", "BHP", "RP",
                "CP_1", "CP_2", "CP_3",
                "PROD_HOURS", "TEMP", "WATER_CUT", "OIL", "FLUID", "GAS",
                "GASLIFT", "GASLIFT_PLAN", "REMARKS"};

            object[] objCollections = new object[23] {date, data_date, field_id, platform, uwi,  
                dome_id, method_id, choke_size, whp, bhp, rp, 
                cp_1, cp_2, cp_3,
                prod_hours, temp, water_cut, oil, fluid, gas,
                gaslift, gaslift_plan, remarks};

            SqlDbType[] sqlType = new SqlDbType[23] {SqlDbType.Date, SqlDbType.NVarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, 
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar};

            string sql = "INSERT INTO VSP_DAILY_PRODUCTION (PRODUCTION_DATE, DATA_DATE, FIELD_ID, PLATFORM_ID, UWI, DOME_ID, "
                + "METHOD_ID, CHOKE_SIZE, "
                + "WELL_HEAD_PRESSURE, BOTTOM_HOLE_PRESSURE,  RESERVOIR_PRESSURE, CASING_PRESSURE_1, CASING_PRESSURE_2, CASING_PRESSURE_3, "
                + "PRODUCTION_HOURS, TEMPERATURE, WATER_CUT, OIL, OIL_CAL, FLUID, GAS, GASLIFT, GASLIFT_PLAN, REMARKS) VALUES("
                + "@DATE, @DATA_DATE, @FIELD_ID, @PLATFORM, @UWI, @DOME_ID, "
                + "@METHOD_ID, @CHOKE_SIZE, "
                + "@WHP, @BHP, @RP, @CP_1, @CP_2, @CP_3, "
                + "@PROD_HOURS, @TEMP, @WATER_CUT, @OIL, @OIL, @FLUID, @GAS, @GASLIFT, @GASLIFT_PLAN, @REMARKS)";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }

        public int UpdateToVSPOGPMSuction(DateTime date, string data_date, string field_id, string platform, string uwi, 
            string dome_id, string method_id, string choke_size, string whp, string bhp, string rp,
            string cp_1, string cp_2, string cp_3,
            string prod_hours, string temp, string water_cut, string oil, string fluid, string gas,
            string gaslift, string gaslift_plan, string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[23] {"DATE", "DATA_DATE", "FIELD_ID", "PLATFORM", "UWI", 
                "DOME_ID", "METHOD_ID", "CHOKE_SIZE", "WHP", "BHP", "RP",
                "CP_1", "CP_2", "CP_3",
                "PROD_HOURS", "TEMP", "WATER_CUT", "OIL", "FLUID", "GAS",
                "GASLIFT", "GASLIFT_PLAN", "REMARKS"};

            object[] objCollections = new object[23] {date, data_date, field_id, platform, uwi, 
                dome_id, method_id, choke_size, 
                whp, bhp, rp, cp_1, cp_2, cp_3,
                prod_hours, temp, water_cut, oil, fluid, gas,
                gaslift, gaslift_plan, remarks};

            SqlDbType[] sqlType = new SqlDbType[23] {SqlDbType.Date, SqlDbType.NVarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, 
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar};

            string sql = "UPDATE VSP_DAILY_PRODUCTION SET DATA_DATE = @DATA_DATE, CHOKE_SIZE = @CHOKE_SIZE, WELL_HEAD_PRESSURE = @WHP, "
                + "BOTTOM_HOLE_PRESSURE = @BHP, RESERVOIR_PRESSURE = @RP, "
                + "CASING_PRESSURE_1 = @CP_1, CASING_PRESSURE_2 = @CP_2, CASING_PRESSURE_3 = @CP_3,"
                + "PRODUCTION_HOURS = @PROD_HOURS, TEMPERATURE = @TEMP, "
                + "WATER_CUT = @WATER_CUT, OIL = @OIL, OIL_CAL = @OIL, FLUID = @FLUID, GAS = @GAS, "
                + "GASLIFT = @GASLIFT, GASLIFT_PLAN = @GASLIFT_PLAN, REMARKS = @REMARKS "
                + "WHERE UWI = @UWI "
                + "AND DOME_ID = @DOME_ID AND METHOD_ID = @METHOD_ID  "
                + "AND PRODUCTION_DATE = @DATE";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }


        public bool CheckIfRowExistInProductionDaily(string uwi, 
        string dome_id, string method_id, DateTime date)
        {
            clsDatabases database = new clsDatabases();
            string sql = "SELECT * FROM VSP_DAILY_PRODUCTION WHERE "
                + "UWI = @UWI AND "
                + "DOME_ID = @DOME_ID AND "
                + "METHOD_ID = @METHOD_ID AND "
                + "PRODUCTION_DATE = @DATE";
            string[] paraCollections = new string[4] {"UWI", "DOME_ID",
               "METHOD_ID", "DATE"};

            object[] objCollections = new object[4] {uwi, dome_id, 
                method_id, date};

            SqlDbType[] sqlType = new SqlDbType[4] {SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Date};

            DataTable table = database.FillDataTable(sql, CommandType.Text, paraCollections, objCollections, sqlType);
            if (table.Rows.Count > 0) return true;
            else return false;
        }

        public bool CheckIfRowExistInInjectionDaily(string uwi,
            string dome_id, DateTime date)
        {
            clsDatabases database = new clsDatabases();
            string sql = "SELECT * FROM VSP_DAILY_INJECTION WHERE "
                + "UWI = @UWI AND "
                + "DOME_ID = @DOME_ID AND "
                + "PRODUCTION_DATE = @DATE";
            string[] paraCollections = new string[3] {"UWI", "DOME_ID", "DATE"};

            object[] objCollections = new object[3] {uwi, dome_id, date};

            SqlDbType[] sqlType = new SqlDbType[3] {SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Date};

            DataTable table = database.FillDataTable(sql, CommandType.Text, paraCollections, objCollections, sqlType);
            if (table.Rows.Count > 0) return true;
            else return false;
        }

        public int InsertToDailyInjection(DateTime date, string data_date, string field_id, string platform, string uwi,
           string dome_id, string whp, string bhp,
            string cp_1, string cp_2, string cp_3, string prod_hours, 
           string water, string water_plan, string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[15] {"DATE", "DATA_DATE", "FIELD_ID", "PLATFORM", "UWI", 
                "DOME_ID", "WHP", "BHP", 
                "CP_1", "CP_2", "CP_3", "PROD_HOURS", 
                "WATER", "WATER_PLAN", "REMARKS"};

            object[] objCollections = new object[15] {date, data_date, field_id, platform, uwi,  
                dome_id, whp, bhp, cp_1, cp_2, cp_3, prod_hours,
                water, water_plan, remarks};

            SqlDbType[] sqlType = new SqlDbType[15] {SqlDbType.Date, SqlDbType.NVarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, 
                SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar};

            string sql = "INSERT INTO VSP_DAILY_INJECTION (PRODUCTION_DATE, DATA_DATE, FIELD_ID, PLATFORM_ID, UWI, DOME_ID, "
                + "WELL_HEAD_PRESSURE, BOTTOM_HOLE_PRESSURE, CASING_PRESSURE_1, CASING_PRESSURE_2, CASING_PRESSURE_3, "
                + "PRODUCTION_HOURS, WATER, WATER_PLAN, REMARKS) VALUES("
                + "@DATE, @DATA_DATE, @FIELD_ID, @PLATFORM, @UWI, @DOME_ID, "
                + "@WHP, @BHP, @CP_1, @CP_2, @CP_3, "
                + "@PROD_HOURS, @WATER, @WATER_PLAN, @REMARKS)";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }

        public int UpdateToDailyInjection(DateTime date, string data_date, string field_id, string platform, string uwi,
           string dome_id, string whp, string bhp, string cp_1, string cp_2, string cp_3, string prod_hours,
           string water, string water_plan, string remarks, ref string msgReport)
        {
            string[] paraCollections = new string[15] {"DATE", "DATA_DATE", "FIELD_ID", "PLATFORM", "UWI", 
                "DOME_ID", "WHP", "BHP", "CP_1", "CP_2", "CP_3", "PROD_HOURS", 
                "WATER", "WATER_PLAN", "REMARKS"};

            object[] objCollections = new object[15] {date, data_date, field_id, platform, uwi,  
                dome_id, whp, bhp, cp_1, cp_2, cp_3, prod_hours,
                water, water_plan, remarks};

            SqlDbType[] sqlType = new SqlDbType[15] {SqlDbType.Date, SqlDbType.NVarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, 
                SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, 
                SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.NVarChar};

            string sql = "UPDATE VSP_DAILY_INJECTION SET DATA_DATE = @DATA_DATE, WELL_HEAD_PRESSURE = @WHP, "
                + "BOTTOM_HOLE_PRESSURE = @BHP, CASING_PRESSURE_1 = @CP_1, CASING_PRESSURE_2 = @CP_2, CASING_PRESSURE_3 = @CP_3, PRODUCTION_HOURS = @PROD_HOURS, "
                + "WATER = @WATER, WATER_PLAN = @WATER_PLAN, REMARKS = @REMARKS "
                + "WHERE UWI = @UWI "
                + "AND DOME_ID = @DOME_ID  "
                + "AND PRODUCTION_DATE = @DATE";
            int i = database.ExecuteNonQuery(sql, CommandType.Text, paraCollections, objCollections, sqlType, ref msgReport);
            return i;
        }
    }

}
