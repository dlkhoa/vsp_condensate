using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;


namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmLogin : Form
    {
        public bool stateLogin = false;
        public bool IsAdminLogin = false;
        public bool bProductionLogin;
        public bool bInjectionLogin;
        public bool bManagerLogin;
        clsConnection cls = new clsConnection();
        LoginSettings settings = new LoginSettings();
 

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.Text == "Login")
                Application.Exit();
            else
                this.Close();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            if (this.Text == "Log Off")
                txtUserName.Text = clsStatic.strUser;
            txtUserName.Text = settings.User;
            txtUserName.SelectAll();
            txtUserName.Focus();
        }

        public void LoginAuthentication(
            string User, string Pwd)
        {
            if (User.Equals(""))
            {
                MessageBox.Show("Please enter your Username");
                txtUserName.Focus();
            }
            else
            {
                if (cls.Authentication(User, Pwd))
                {
                    //Kiểm tra User Name và password trong bảng tblUser
                    string sql = "SELECT UserName, IsAdminGroup, AccessProduction, AccessInjection, Can_Change_Data" 
                        + " FROM SYS_USERS WHERE UserName = '" + User 
                        + "' AND  Convert(varchar(100), DecryptByPassPhrase('DBProd', PassWord)) = '" + Pwd + "'";
                    clsDatabases cl = new clsDatabases();
                    DataTable table = cl.FillDataTable(sql, CommandType.Text);
                    
                    //nếu nhập đúng userName và Password
                    if (table.Rows.Count > 0)
                    {
                        stateLogin = true;
                        clsStatic.strUser = txtUserName.Text;
                        clsStatic.strServerName = cls.ServerName;
                        clsStatic.bIsAdminGroup = false;
                        if (table.Rows[0]["IsAdminGroup"] != null)
                        {
                            bool.TryParse(table.Rows[0]["IsAdminGroup"].ToString(), out clsStatic.bIsAdminGroup);
                        }
                        clsStatic.bAccessProduction = false;
                        if (table.Rows[0]["IsAdminGroup"] != null)
                        {
                            bool.TryParse(table.Rows[0]["AccessProduction"].ToString(), out clsStatic.bAccessProduction);
                        }
                        clsStatic.bCanChangeData = false;
                        if (table.Rows[0]["IsAdminGroup"] != null)
                        {
                            bool.TryParse(table.Rows[0]["Can_Change_Data"].ToString(), out clsStatic.bCanChangeData);
                        }

                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Login fail. Please try again!");
                        txtPassword.SelectAll();
                        txtPassword.Focus();
                    }
                }
                else
                {
                    MessageBox.Show(cls.Error);
                    if (this.Text != "Login")
                        this.Text = "Login";
                }

            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bProductionLogin = false;
            bInjectionLogin = false;
            bManagerLogin = false;
            LoginAuthentication(txtUserName.Text, txtPassword.Text);
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                LoginAuthentication(txtUserName.Text, txtPassword.Text);
        }

        private void chkAdmin_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAdmin.Checked)
            {
                txtUserName.Text = "ADMIN";
                txtUserName.ReadOnly = true;
                txtPassword.Focus();
            }
            else
            {
                txtUserName.ReadOnly = false;
                txtUserName.Focus();
            }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            if (txtUserName.Text.ToUpper() == "ADMIN")
            {
                txtUserName.ReadOnly = true;
                chkAdmin.Checked = true;
                txtPassword.Focus();
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            frmConfigServer frm = new frmConfigServer();
            frm.ShowDialog();
        }
    }
}