using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace TrueTech.VSP.VSPCondensate
{
    class clsConnection
    {
        private string serverName;
        private string databaseName;
        private string userName;
        private string password;
        public static SqlConnection sqlConnection;
        public string ServerName
        {
            get { return serverName; }
            set { serverName = value; }
        }
        internal clsConnection()
        {
            sqlConnection = new SqlConnection();
        }
        private string strError = "";
        public string Error
        {
            get { return strError; }
        }

        public SqlConnection SqlConnection
        {
            get { return sqlConnection; }
        }

        internal static void CloseConnection()
        {
            if (sqlConnection.State != ConnectionState.Closed)
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }
        internal bool Authentication(string user, string pwd)
        {
            strError = "";
            if (sqlConnection.State == ConnectionState.Open)
                sqlConnection.Close();
            bool IsExist = false;
            string path = Directory.GetCurrentDirectory() + "\\config.dat";
            if (!File.Exists(path))
            {
                strError += "File:\t" + path + " is not found!!!";
                return false;
            }
            StreamReader sr = File.OpenText(path);
            serverName = sr.ReadLine();
            databaseName = sr.ReadLine();
            userName = sr.ReadLine();
            password = sr.ReadLine();
            try
            {
                if (ConnectToSQLServer(serverName, databaseName, userName, password))
                    clsStatic.strUser = user;
                IsExist = true;
            }
            catch (Exception ex)
            {
                strError = "Error: " + ex.Message;
            }
            return IsExist;
        }

        public bool ConnectToSQLServer(string ServerName, string DatabaseName, string UserName, string Password)
        {
            sqlConnection = new SqlConnection();
            if (ServerName != "")
            {
                int pos = 0;
                char cfind = '\\';
                string sfind = "\\";
                string sreplace = "\\\\";

                if (ServerName.StartsWith(sfind))
                {
                    pos = ServerName.IndexOf(cfind);
                    if (ServerName[pos + 1] != cfind)
                    {
                        ServerName.Replace(sfind, sreplace);
                    }
                }
            }
            clsStatic.connectionString = "Data Source=" + ServerName + ";Initial Catalog=" + DatabaseName + ";User ID=" + UserName + ";password=" + Password;
            sqlConnection.ConnectionString = "Data Source=" + ServerName + ";Initial Catalog=" + DatabaseName + ";User ID=" + UserName + ";password=" + Password;

            try
            {
                sqlConnection.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
