﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Collections;
using System.IO;
using System.Data.OleDb;
using System.Data.SqlClient;
using Spire.Xls;

namespace TrueTech.VSP.VSPCondensate
{
    public partial class frmExportMonthlyCondensate : Form
    {
        public int year;
        public int month;
        string strMonth;

        public string strExportDate; //Date you choose to export data for example: 01/07/2009
        private int col_title = 14; //cot chua title: field, reservoir, dome, zone...
        private DataTable rowsTable; //table include all rows will be exported to report

        clsDatabases database = new clsDatabases();
        VSP_Setting setting = new VSP_Setting();

        public frmExportMonthlyCondensate()
        {
            InitializeComponent();
        }

        private void frmExportMonthlyGasProduction_Load(object sender, EventArgs e)
        {
            this.Text = "Đang xuất dữ liệu...";
            if (month.ToString().Length == 1)
                strExportDate = "0" + month.ToString() + "/01/" + year.ToString();
            else
                strExportDate = month.ToString() + "/01/" + year.ToString();
        }

        private void StartExcelFile()
        {
            var fileName = "Condensate_";
            if (month.ToString().Length == 1)
                fileName += "0" + month.ToString() + "_" + year.ToString();
            else
                fileName += month.ToString() + "_" + year.ToString();
            fileName += "_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Excel 97-2003 Workbook (*.xls)|*.xls";
            dlg.FileName = fileName;
            dlg.RestoreDirectory = true;
            DialogResult result = dlg.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    rowsTable = new DataTable();
                    rowsTable.Columns.Add("FIELD_ID", Type.GetType("System.String"));
                    rowsTable.Columns.Add("PLATFORM", Type.GetType("System.String"));
                    rowsTable.Columns.Add("UWI", Type.GetType("System.String"));
                    rowsTable.Columns.Add("WELL_NUMBER", Type.GetType("System.String"));
                    rowsTable.Columns.Add("RESERVOIR_ID", Type.GetType("System.String"));
                    rowsTable.Columns.Add("DOME_ID", Type.GetType("System.String"));
                    rowsTable.Columns.Add("ZONE_ID", Type.GetType("System.String"));
                    rowsTable.Columns.Add("METHOD_ID", Type.GetType("System.String"));
                    rowsTable.Columns.Add("STATUS_NAME", Type.GetType("System.String"));
                    rowsTable.Columns.Add("STATUS", Type.GetType("System.String"));
                    rowsTable.Columns.Add("CONDENSATE", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("CONDENSATE_YEAR", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("CONDENSATE_ACC", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("WATER_CUT", Type.GetType("System.String"));
                    rowsTable.Columns.Add("WATER", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("WATER_YEAR", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("WATER_ACC", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("CHOKE_SIZE_1", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("CHOKE_SIZE_2", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("PROD_HOURS", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("FILL_UP_HOURS", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("SUSPENDED_HOURS", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("WORKDAY_YEAR", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("PRESSURE_1", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("PRESSURE_2", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("GAS", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("GAS_YEAR", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("GAS_ACC", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("GOR", Type.GetType("System.Double"));
                    rowsTable.Columns.Add("REMARKS", Type.GetType("System.String"));
                    rowsTable.Columns.Add("WELL_NUM", Type.GetType("System.Int32"));

                    GetAllDataToTable(strExportDate);
                    progressBar1.Maximum = rowsTable.Rows.Count;
                    progressBar1.Value = 0;

                    ExportToExcel(strExportDate, dlg.FileName);
                    MessageBox.Show("Hoàn thành");
                    System.Diagnostics.Process.Start(dlg.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                this.Close();
            }
            else this.Close();
        }

        //Firstly we collect all of data row will be exported to rowsTable by functions: GetAllDataToTable, GetAllDataOfField
        //GetAllDataOfReservoir, GetAllDataOfDome, GetAllDataOfZone, GetAllDataOfMethod.

        private void GetAllDataToTable(string strExportDate)
        {
            string sql_Query = "SELECT p.FIELD_ID, p.PLATFORM, p.UWI, wh.WELL_NUMBER,"
                    + " p.RESERVOIR_ID, p.DOME_ID, p.ZONE_ID, p.METHOD_ID, ms.STATUS_NAME, p.STATUS, "
                    + " ROUND(p.CONDENSATE, 0) AS CONDENSATE, ROUND(p.CONDENSATE_YEAR, 0) AS CONDENSATE_YEAR, ROUND(p.CONDENSATE_ACC, 0) AS CONDENSATE_ACC, "
                    + " ROUND(P.WATER_CUT, 1) AS WATER_CUT, ROUND(P.WATER, 0) AS WATER, ROUND(P.WATER_YEAR, 0) AS WATER_YEAR, ROUND(p.WATER_ACC, 0) AS WATER_ACC, "
                    + " P.CHOKE_SIZE_1, p.CHOKE_SIZE_2, "
                    + " p.PROD_HOURS, p.FILL_UP_HOURS, p.SUSPENDED_HOURS, P.WORKDAY_YEAR, "
                    + " P.PRESSURE_1, P.PRESSURE_2, "
                    + " ROUND(P.GAS, 1) AS GAS, ROUND(p.GAS_YEAR, 1) AS GAS_YEAR, ROUND(P.GAS_ACC, 1) AS GAS_ACC, "
                    + " P.GOR, P.REMARKS, "
                    + " WH.WELL_NUMBER_SORT AS WELL_NUM FROM "
                    + " VSP_CONDENSATE p, VSP_WELL_HDR wh, VSP_METHOD_STATUS ms "
                    + " WHERE p.UWI = wh.UWI AND ms.STATUS_ID = p.STATUS_ID AND p.MONTH = CONVERT(DATETIME, '" + strExportDate + "', 101) ORDER BY reservoir_id, dome_id, zone_id, method_id, well_num";
            rowsTable = database.FillDataTable(sql_Query, CommandType.Text);
        }

        
        private void ExportToExcel(string strExportDate, string str_FILE_NAME)
        {
            Spire.Xls.Workbook SpireWorkBook = new Spire.Xls.Workbook();
            SpireWorkBook.SaveToFile(str_FILE_NAME);
            Spire.Xls.Worksheet worksheet = SpireWorkBook.Worksheets[0];

            worksheet.Pictures.Add(1, 1, Directory.GetCurrentDirectory() + "\\logo_vsp.png", 80, 60);


            //Set column width, align, text format and font
            //Set column width, align, text format and font
            worksheet.SetColumnWidth(1, 3.14);
            worksheet.SetColumnWidth(2, 3.86);
            worksheet.SetColumnWidth(3, 4.14);
            worksheet.SetColumnWidth(4, 3.43);
            worksheet.SetColumnWidth(5, 4.71);
            worksheet.SetColumnWidth(6, 5.71);
            worksheet.SetColumnWidth(7, 7.29);
            worksheet.SetColumnWidth(8, 3.29);
            worksheet.SetColumnWidth(9, 4.57);
            worksheet.SetColumnWidth(10, 5.71);
            worksheet.SetColumnWidth(11, 6.14);
            worksheet.SetColumnWidth(12, 4.43);
            worksheet.SetColumnWidth(13, 5.00);
            worksheet.SetColumnWidth(14, 4.86);
            worksheet.SetColumnWidth(15, 5.29);
            worksheet.SetColumnWidth(16, 4.29);
            worksheet.SetColumnWidth(17, 4.00);
            worksheet.SetColumnWidth(18, 3.86);
            worksheet.SetColumnWidth(19, 3.86);
            worksheet.SetColumnWidth(20, 4.43);
            worksheet.SetColumnWidth(21, 5.43);
            worksheet.SetColumnWidth(22, 6.86);
            worksheet.SetColumnWidth(23, 6.86);
            worksheet.SetColumnWidth(24, 5.00);
            worksheet.SetColumnWidth(25, 18.86);

            //Set header
            DataTable field_table = new DataTable();
            string sqlGetField = "SELECT distinct P.field_id, F.ORDER_NUM from VSP_CONDENSATE P, VSP_FIELD_HDR F "
                + " WHERE P.FIELD_ID = F.FIELD_ID AND P.month <= CONVERT(DATETIME, '" 
                + strExportDate + "', 101) ORDER BY F.ORDER_NUM";
            string all_field = "";
            //---------choose field-----------
            foreach (DataRow dr_field in database.FillDataTable(sqlGetField, CommandType.Text).Rows)
            {
                foreach (DataRow dr in database.FillDataTable("SELECT FIELD_NAME_R FROM VSP_FIELD_HDR WHERE FIELD_ID = '" 
                    + dr_field[0].ToString() + "'", CommandType.Text).Rows)
                    all_field += "\"" + dr[0].ToString() + "\", ";
            }
            if (all_field.Length > 4) all_field = all_field.Substring(0, all_field.Length - 2); // bo bot 2 ki tu "," va "space"
            worksheet.SetText(1, col_title, "ЭКСПЛУАТАЦИОННЫЙ РАПОРТ");

            //chuyen thang tu so -> chu
            strMonth = "";
            switch (month)
            {
                case 1: strMonth = "ЯНВАРЬ"; break;
                case 2: strMonth = "ФЕВРАЛЬ"; break;
                case 3: strMonth = "МАРТ"; break;
                case 4: strMonth = "АПРЕЛЬ"; break;
                case 5: strMonth = "МАЙ"; break;
                case 6: strMonth = "ИЮНЬ"; break;
                case 7: strMonth = "ИЮЛЬ"; break;
                case 8: strMonth = "АВГУСТ"; break;
                case 9: strMonth = "СЕНТЯБРЬ"; break;
                case 10: strMonth = "ОКТЯБРЬ"; break;
                case 11: strMonth = "НОЯБРЬ"; break;
                case 12: strMonth = "ДЕКАБРЬ"; break;
            }
            worksheet.SetText(2, col_title, "ДОБЫЧА ГАЗОВОГО КОНДЕНСАТА М/Р " + all_field);
            worksheet.SetText(3, col_title, "ПДНГ – СП «ВЬЕТСОВПЕТРО» - ЗА " + strMonth + " " + year.ToString() + "г.");
            worksheet.Range[1, col_title, 3, col_title].Style.Font.FontName = "Times New Roman";
            worksheet.Range[1, col_title, 3, col_title].Style.Font.Size = 12;
            worksheet.Range[1, col_title, 3, col_title].Style.Font.IsBold = true;
            worksheet.Range[1, col_title, 3, col_title].Style.HorizontalAlignment = HorizontalAlignType.Center;
            worksheet.Range[1, col_title, 3, col_title].Style.VerticalAlignment = VerticalAlignType.Center;
            
            //Set Column header
            worksheet.Range[6, 1, 7, 1].Merge();
            worksheet.SetText(6, 1, "No\nП/П");

            worksheet.Range[6, 2, 7, 3].Merge();
            worksheet.SetText(6, 2, "No\nСКВ. МСП.");

            worksheet.Range[6, 4, 7, 4].Merge();
            worksheet.SetText(6, 4, "СП-Б ЭКСП.");

            worksheet.Range[6, 5, 6, 7].Merge();
            worksheet.SetText(6, 5, "ДОБЫЧА КОНДЕНСАТА, Т");
            worksheet.SetText(7, 5, "ЗА МЕСЯЦ");
            worksheet.SetText(7, 6, "С НАЧ. ГОДА");
            worksheet.SetText(7, 7, "С НАЧ. ЭКСП.");

            worksheet.Range[6, 8, 7, 8].Merge();
            worksheet.SetText(6, 8, "%ВОДЫ");

            worksheet.Range[6, 9, 6, 11].Merge();
            worksheet.SetText(6, 9, "ДОБЫЧА ВОДЫ, Т");
            worksheet.SetText(7, 9, "ЗА МЕСЯЦ");
            worksheet.SetText(7, 10, "С НАЧ. ГОДА");
            worksheet.SetText(7, 11, "С НАЧ. ЭКСП.");

            worksheet.Range[6, 12, 7, 12].Merge();
            worksheet.SetText(6, 12, "ДИАМ ШТ-РА (ММ)");

            worksheet.Range[6, 13, 7, 13].Merge();
            worksheet.SetText(6, 13, "УПЛОТ СР.СУТ ДЕБИТ ПО КОНД., Т/СУТ.");

            worksheet.Range[6, 14, 6, 16].Merge();
            worksheet.SetText(6, 14, "УПЛОТН. СКВ.-ДНИ");
            worksheet.SetText(7, 14, "ЗА МЕСЯЦ");
            worksheet.SetText(7, 15, "С НАЧ. ГОДА");
            worksheet.SetText(7, 16, "ПРОСТ. ЗА МЕС.");

            worksheet.Range[6, 17, 6, 19].Merge();
            worksheet.SetText(6, 17, "СКВ. ЧАСЫ ЗА МЕСЯЦ");
            worksheet.SetText(7, 17, "РАБ.");
            worksheet.SetText(7, 18, "НАК.");
            worksheet.SetText(7, 19, "ПРОСТ");

            worksheet.Range[6, 20, 7, 20].Merge();
            worksheet.SetText(6, 20, "РБ (АТМ)");

            worksheet.Range[6, 21, 6, 23].Merge();
            worksheet.SetText(6, 21, "ДОБЫЧА ГАЗА, Тыс. М3");
            worksheet.SetText(7, 21, "ЗА МЕСЯЦ");
            worksheet.SetText(7, 22, "С НАЧ. ГОДА");
            worksheet.SetText(7, 23, "С НАЧ. ЭКСП.");

            worksheet.Range[6, 24, 7, 24].Merge();
            worksheet.SetText(6, 24, "СР.СУТ ДЕБИТ ПО ГАЗУ, ТЫС М3/СУТ.");

            worksheet.Range[6, 25, 7, 25].Merge();
            worksheet.SetText(6, 25, "ПРИМЕЧАНИЕ");

            //so thu tu cot
            worksheet.Range[8, 2, 8, 3].Merge();
            worksheet.SetCellValue(8, 1, "1");
            worksheet.SetCellValue(8, 2, "2");
            for (int i = 4; i <= 25; i++)
            {
                worksheet.SetCellValue(8, i, (i - 1).ToString()); //so thu tu cua cot
            }

            worksheet.Range[6, 1, 8, 25].Style.WrapText = true;
            worksheet.Range[6, 1, 8, 25].Style.Font.FontName = "Times New Roman";
            worksheet.Range[6, 1, 8, 25].Style.Font.Size = 7;
            worksheet.Range[6, 1, 8, 25].Style.HorizontalAlignment = HorizontalAlignType.Center;
            worksheet.Range[6, 1, 8, 25].Style.VerticalAlignment = VerticalAlignType.Center;
            worksheet.Range[6, 1, 8, 25].Borders.LineStyle = LineStyleType.Thin;
            worksheet.Range[6, 1, 8, 25].Borders[BordersLineType.DiagonalDown].LineStyle = LineStyleType.None;
            worksheet.Range[6, 1, 8, 25].Borders[BordersLineType.DiagonalUp].LineStyle = LineStyleType.None;
            worksheet.Range[6, 1, 8, 25].Borders.Color = Color.Black;
            worksheet.Range[6, 1, 6, 25].RowHeight = 22D;
            worksheet.Range[7, 1, 7, 25].RowHeight = 45D;

            ////Fill data
            int totalRow = 0;
            FillData(worksheet, strExportDate, ref totalRow);
            worksheet.Range[9, 1, totalRow, 25].RowHeight = 9D;
            worksheet.Range[9, 1, totalRow, 25].VerticalAlignment = VerticalAlignType.Center;
            
            //page setup for this file
            worksheet.PageSetup.LeftMargin = 0.0D;
            worksheet.PageSetup.RightMargin = 0.0D;
            worksheet.PageSetup.TopMargin = 0.1D;
            worksheet.PageSetup.BottomMargin = 0.28D;
            worksheet.PageSetup.HeaderMarginInch = 0.1D;
            worksheet.PageSetup.FooterMarginInch = 0.1D;
            worksheet.PageSetup.Orientation = PageOrientationType.Landscape;
            worksheet.PageSetup.PrintTitleRows = "$8:$8";

            //create footer
            StringBuilder str = new StringBuilder();
            str.Append(@"&""Times New Roman,Regular""&6" + setting.LeftFoot);
            worksheet.PageSetup.LeftFooter = str.ToString();
            str = new StringBuilder();
            str.Append(@"&""Times New Roman,Regular""&6" + setting.CenterFoot);
            worksheet.PageSetup.CenterFooter = str.ToString();

            str = new StringBuilder();
            str.Append(@"&""Times New Roman,Regular""&6TRANG &P/&N");
            worksheet.PageSetup.RightFooter = str.ToString();

            //save file
            SpireWorkBook.Save();
            worksheet.Dispose();
        }

        private void FillData(Spire.Xls.Worksheet worksheet, string strExportDate, ref int totalRow)
        {
            int row_idx = 9; //start write to exel file at row number 9
            int count_well = 1; //start count working well at column 1 at 1

            //select all field name from vsp_production table
            DataTable field_table = new DataTable();
            string sqlGetField = "SELECT DISTINCT F.FIELD_ID, F.VSP_PERCENT, F.ORDER_NUM "
                + " FROM VSP_CONDENSATE P, VSP_FIELD_HDR F "
                + " WHERE F.FIELD_ID = P.FIELD_ID "
                + " AND P.MONTH = CONVERT(DATETIME, '" + strExportDate + "', 101) ORDER BY F.ORDER_NUM";

            object[] objNewWell = new object[13];
            object[] objOldWell = new object[13];
            object[] objTotalWell = new object[13];
            for (int i = 0; i < 13; i++)
            {
                objNewWell.SetValue(0, i);
                objOldWell.SetValue(0, i);
                objTotalWell.SetValue(0, i);
            }
            
            //---------chon lan luot tung mo
            foreach (DataRow dr_field in database.FillDataTable(sqlGetField, CommandType.Text).Rows)
            {
                string field_id = dr_field[0].ToString();
                row_idx++;

                FillDataForEachField(worksheet, strExportDate, field_id, ref row_idx,
                    ref count_well);
                
                //---tinh toan tong khai thac
                double dVsp_Percent = Convert.ToDouble(dr_field[1].ToString());
                string strFilter = "FIELD_ID = '" + field_id + "' AND STATUS = 'M'";

                object[] objSum = new object[13];

                if (rowsTable.Select(strFilter).Length == 0)
                    for (int i = 0; i < 13; i++)
                        objSum.SetValue(0, i);
                else
                {
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 0);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 1);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 2);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 3);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER_YEAR)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 4);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER_ACC)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 5);
                    objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                    objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                    objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                    objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 10);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS_YEAR)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 11);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS_ACC)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 12);
                }
                for (int i = 0; i < 13; i++)
                {
                    objNewWell.SetValue(Convert.ToDouble(objNewWell.GetValue(i).ToString())
                        + Convert.ToDouble(objSum[i].ToString()), i);
                }

                strFilter = "FIELD_ID = '" + field_id + "' AND STATUS = 'C'";
                objSum = new object[13];

                if (rowsTable.Select(strFilter).Length == 0)
                    for (int i = 0; i < 13; i++)
                        objSum.SetValue(0, i);
                else
                {
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 0);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 1);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 2);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 3);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER_YEAR)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 4);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER_ACC)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 5);
                    objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                    objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                    objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                    objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 10);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS_YEAR)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 11);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS_ACC)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 12);
                }

                for (int i = 0; i < 13; i++)
                {
                    objOldWell.SetValue(Convert.ToDouble(objOldWell.GetValue(i).ToString())
                        + Convert.ToDouble(objSum[i].ToString()), i);
                }


                strFilter = "FIELD_ID = '" + field_id + "'";
                objSum = new object[13];

                if (rowsTable.Select(strFilter).Length == 0)
                    for (int i = 0; i < 13; i++)
                        objSum.SetValue(0, i);
                else
                {
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 0);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 1);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 2);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 3);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER_YEAR)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 4);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER_ACC)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 5);
                    objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                    objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                    objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                    objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 10);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS_YEAR)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 11);
                    objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS_ACC)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 12);

                }

                for (int i = 0; i < 13; i++)
                {
                    objTotalWell.SetValue(Convert.ToDouble(objTotalWell.GetValue(i).ToString())
                        + Convert.ToDouble(objSum[i].ToString()), i);
                }
                
            }
            
            //--------------------END OF FILE --------------------------------------
            row_idx++;

            FillSummary(worksheet, "ИТОГО ПО ПДНГ:", ConvertObjectToSummaryParameter(objTotalWell, 100), ref row_idx);
            FillSummary(worksheet, "В Т.Ч. НОВ.СКВ.:", ConvertObjectToSummaryParameter(objNewWell, 100), ref row_idx);
            FillSummary(worksheet, "ПЕР.СКВ.:", ConvertObjectToSummaryParameter(objOldWell, 100), ref row_idx);
            
            row_idx++;
            row_idx++;
            string total_oil = ConvertObjectToSummaryParameter(objTotalWell, 100).GetValue(0).ToString();
            string total_gas = ConvertObjectToSummaryParameter(objTotalWell, 100).GetValue(14).ToString();
            worksheet.SetText(row_idx, 1, "ВСЕГО ДОБЫТО ГАЗОВОГО КОНДЕНСАТА ЗА " + strMonth + " " + year + "Г.");
            worksheet.SetCellValue(row_idx, 12, total_oil + " Т. И " + total_gas + " ТЫС.М3 ГАЗА");
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.Size = 9;
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.IsBold = true;
            row_idx++;
            worksheet.SetText(row_idx, 1, "В т.ч. поступление на УБН ");
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.Size = 9;
            row_idx++;
            worksheet.SetText(row_idx, 1, "Потери  при сборе и транспортировке ");
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.Size = 9;
            row_idx++;
            worksheet.SetText(row_idx, 1, "Расход нефти на нужды ПБиКРС ");
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.Size = 9;
            row_idx++;
            worksheet.SetText(row_idx, 1, "Изменение объема нефти в емкостях и нефтепроводах ");
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.Size = 9;
            row_idx++;
            worksheet.SetText(row_idx, 1, "Подача газа на берег за " + strMonth + " " + year + "Г.");
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 1, row_idx, 12].Style.Font.Size = 9;
            row_idx++;
            row_idx++;
            row_idx++;
            worksheet.SetText(row_idx, 10, "ГЛАВНЫЙ ГЕОЛОГ ПДНГ");
            worksheet.SetText(row_idx, 20, setting.MainSign);
            worksheet.Range[row_idx, 10, row_idx, 20].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 10, row_idx, 20].Style.Font.Size = 10;
            worksheet.Range[row_idx, 10, row_idx, 20].Style.Font.IsBold = true;
            row_idx++;
            row_idx++;
            worksheet.SetText(row_idx, 3, "СОСТАВИЛ:");
            worksheet.SetText(row_idx, 7, "ОРМ ПДНГ");
            worksheet.Range[row_idx, 3, row_idx, 7].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 3, row_idx, 7].Style.Font.Size = 7;
            worksheet.Range[row_idx, 3, row_idx, 7].Style.Font.IsBold = true;
            row_idx++;
            row_idx++;
            worksheet.SetText(row_idx, 4, "Исполнитель: "+ setting.ArtistSign);
            worksheet.Range[row_idx, 4].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 4].Style.Font.Size = 7;
            
            //------------------------------------------------------------------------

            totalRow = row_idx;

        }

        //This function will export all well in rowsTable that have the same: field_id
        //in this function we use filter criteria to select data in rowsTable
        //field_name get from VSP_FIELD table in DB
        private void FillDataForEachField(Spire.Xls.Worksheet worksheet, string strExportDate,
            string field_id, ref int row_idx, ref int count_well)
        {
            //---------Chuyen field_id sang field_name_r de xuat ra file excel---------------
            string field_name_r = null;
            string field_name_r_sum = null;
            string vsp_percent = null;
            foreach (DataRow dr in database.FillDataTable(
                "SELECT FIELD_NAME_R, FIELD_NAME_R_1, VSP_PERCENT FROM VSP_FIELD_HDR WHERE FIELD_ID = '" 
                + field_id + "'", CommandType.Text).Rows)
            {
                field_name_r = dr[0].ToString();
                field_name_r_sum = dr[1].ToString();
                vsp_percent = dr[2].ToString();
            }
            worksheet.SetCellValue(row_idx, col_title, field_name_r);
            worksheet.Range[row_idx, col_title].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, col_title].Style.Font.Size = 10;
            worksheet.Range[row_idx, col_title].Style.Font.IsBold = true;
            worksheet.Range[row_idx, col_title].Style.HorizontalAlignment = HorizontalAlignType.Center;
            //----------------------------------------------------------------------------------
            
            string sqlGetReservoir = "SELECT DISTINCT P.RESERVOIR_ID, E.ORDER_NUM "
                + " FROM VSP_CONDENSATE P, VSP_EPOCH E "
                + " WHERE P.RESERVOIR_ID = E.RESERVOIR_ID AND P.FIELD_ID = '" + field_id
                + "' AND P.MONTH = CONVERT(DATETIME, '" + strExportDate + "', 101) ORDER BY E.ORDER_NUM";

            //-----------chon lan luot tung` tang`
            foreach (DataRow dr_reservoir in database.FillDataTable(sqlGetReservoir, CommandType.Text).Rows)
            {
                string reservoir_id = dr_reservoir[0].ToString();
                row_idx++;
                FillDataForEachReservoir(worksheet, strExportDate, field_id, reservoir_id, ref row_idx, ref count_well);

            }
            
            //Fill summary FIELD

            string strFilter = "FIELD_ID = '" + field_id + "'";
            object[] objSum = new object[13];
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
            objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
            objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
            objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
            objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
            objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
            objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
            FillSummary(worksheet, "ИТОГО ПО " + field_name_r_sum, ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

            //Fill summary by NEW well
            strFilter = "FIELD_ID = '" + field_id + "' AND STATUS = 'M'";
            objSum = new object[13];
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
            objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
            objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
            objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
            objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
            objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
            objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
            FillSummary(worksheet, "В Т.Ч. НОВ.СКВ.:", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

            //Fill summary by OLD well
            strFilter = "FIELD_ID = '" + field_id + "' AND STATUS = 'C'";
            objSum = new object[13];
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
            objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
            objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
            objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
            objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
            objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
            objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
            FillSummary(worksheet, "ПЕР.СКВ.:", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

            //Fill summary by Platform
            //Get distinct Platform name in rowsTable
            string sqlPlatform = "SELECT DISTINCT P.PLATFORM, PL.ORDER_NUM FROM "
                + " VSP_CONDENSATE P, VSP_PLATFORM PL WHERE P.PLATFORM = PL.PLATFORM"
                + " AND P.MONTH = CONVERT(DATETIME, '" + strExportDate + "', 101)"
                + " AND P.FIELD_ID = '" + field_id + "'"
                + " ORDER BY PL.ORDER_NUM";
            DataTable dtPlatform = database.FillDataTable(sqlPlatform, CommandType.Text);

            //Fill to excel file
            foreach (DataRow drPlatform in dtPlatform.Rows)
            {
                strFilter = "FIELD_ID = '" + field_id + "' AND PLATFORM = '" + drPlatform[0].ToString() + "'";
                objSum = new object[13];
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
                objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
                objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
                string platform = drPlatform[0].ToString();
                string platformRus = platform;
                if (platform.Contains("MSP"))
                    platformRus = platform.Replace("MSP", "МСП");
                else if (platform.Contains("BK"))
                    platformRus = platform.Replace("BK", "БК");
                FillSummary(worksheet, platformRus, ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);
            }


            //neu mỏ khong phai 100% cua VSP thi hien 1 dong
            double dVsp_Percent = Convert.ToDouble(vsp_percent);
            if (dVsp_Percent != 100)
            {
                strFilter = "FIELD_ID = '" + field_id + "'";
                objSum = new object[13];
                objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 0);
                objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 1);
                objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 2);
                objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 3);
                objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER_YEAR)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 4);
                objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(WATER_ACC)", strFilter)) * dVsp_Percent / 100, 0, MidpointRounding.AwayFromZero), 5);
                objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 10);
                objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS_YEAR)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 11);
                objSum.SetValue(Math.Round(Convert.ToDouble(rowsTable.Compute("Sum(GAS_ACC)", strFilter)) * dVsp_Percent / 100, 1, MidpointRounding.AwayFromZero), 12);
                FillSummary(worksheet, "ИТОГО ПО " + field_name_r_sum + " ПО VSP", ConvertObjectToSummaryParameter(objSum, dVsp_Percent), ref row_idx);
            }
        }

        //This function will export all well in rowsTable that have the same: field_id, reservoir_id
        //in this function we use filter criteria to select data in rowsTable
        //reservoir_name get from VSP_EPOCH table in DB
        private void FillDataForEachReservoir(Spire.Xls.Worksheet worksheet, string strExportDate,
            string field_id, string reservoir_id, ref int row_idx, ref int count_well)
        {
            //---------Chuyen zone_id sang reservoir_name_r de xuat ra file excel---------------
            string reservoir_name_r = null;
            string reservoir_name_r_sum = null;
            foreach (DataRow dr in database.FillDataTable(
                "SELECT RESERVOIR_NAME_R, RESERVOIR_NAME_R_1 FROM VSP_EPOCH WHERE RESERVOIR_ID = " 
                + reservoir_id, CommandType.Text).Rows)
            {
                reservoir_name_r = dr[0].ToString();
                reservoir_name_r_sum = dr[1].ToString();
            }
            worksheet.SetCellValue(row_idx, col_title, reservoir_name_r);
            worksheet.Range[row_idx, col_title].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, col_title].Style.Font.Size = 10;
            worksheet.Range[row_idx, col_title].Style.Font.IsBold = true;
            worksheet.Range[row_idx, col_title].Style.HorizontalAlignment = HorizontalAlignType.Center;
            //----------------------------------------------------------------------------------
            
            string sqlGetDome = "SELECT DISTINCT P.DOME_ID, D.ORDER_NUM "
                + " FROM VSP_CONDENSATE P, VSP_DOME D "
                + " WHERE P.DOME_ID = D.DOME_ID AND "
                + " P.FIELD_ID = '" + field_id + "' AND P.RESERVOIR_ID = " + reservoir_id
                + " AND P.MONTH = CONVERT(DATETIME, '" + strExportDate + "', 101) ORDER BY D.ORDER_NUM";

            //chon lan luot tung dome
            foreach (DataRow dr_dome in database.FillDataTable(sqlGetDome, CommandType.Text).Rows)
            {
                string dome_id = dr_dome[0].ToString();
                FillDataForEachDome(worksheet, strExportDate, field_id, reservoir_id, dome_id, ref row_idx, ref count_well);
            }
            
            //Fill summary RESERVOIR
            string strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id + "'";
            object[] objSum = new object[13];
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
            objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
            objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
            objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
            objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
            objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
            objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
            FillSummary(worksheet, "ИТОГО ПО " + reservoir_name_r_sum, ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

            //Fill summary by METHOD - Tu phun
            strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                + "' AND METHOD_ID = '1'";
            objSum = new object[13];
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
            objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
            objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
            objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
            objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
            objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
            objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
            if (rowsTable.Compute("Sum(CONDENSATE)", strFilter).ToString().Length != 0)
                if (Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE)", strFilter)) != 0)
                    FillSummary(worksheet, "ПО ФОН.", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

            //Fill summary by METHOD - Bom
            strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                + "' AND METHOD_ID = '2'";
            objSum = new object[13];
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
            objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
            objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
            objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
            objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
            objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
            objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
            if (rowsTable.Compute("Sum(CONDENSATE)", strFilter).ToString().Length != 0)
                if (Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE)", strFilter)) != 0)
                    FillSummary(worksheet, "ПО ЭЦН.", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

            //Fill summary by METHOD - Gaslift
            strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                + "' AND METHOD_ID = '3'";
            objSum = new object[13];
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
            objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
            objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
            objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
            objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
            objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
            objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
            if (rowsTable.Compute("Sum(CONDENSATE)", strFilter).ToString().Length != 0)
                if (Convert.ToDouble(rowsTable.Compute("Sum(CONDENSATE)", strFilter)) != 0)
                    FillSummary(worksheet, "ПО КГ.", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

            //Fill summary by New well
            strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                + "' AND STATUS = 'M'";
            objSum = new object[13];
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
            objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
            objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
            objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
            objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
            objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
            objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
            FillSummary(worksheet, "В Т.Ч. НОВ.СКВ.:", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

            //Fill summary by New well
            strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                + "' AND STATUS = 'C'";
            objSum = new object[13];
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
            objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
            objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
            objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
            objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
            objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
            objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
            objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
            objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
            objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
            FillSummary(worksheet, "ПЕР.СКВ.:", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

            //Fill summary by Oligocene (White Tiger field only)
            if (field_id == "BH" & reservoir_id == "3")
            {
                strFilter = "FIELD_ID = 'BH' AND (RESERVOIR_ID = '2' OR RESERVOIR_ID = '3')";
                objSum = new object[13];
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
                objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
                objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
                FillSummary(worksheet, "ИТОГО ПО ОЛИГОЦЕНУ:", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);
            }
        }

        //This function will export all well in rowsTable that have the same: field_id, reservoir_id, dome_id
        //in this function we use filter criteria to select data in rowsTable
        //dome_name get from VSP_DOME table in DB
        private void FillDataForEachDome(Spire.Xls.Worksheet worksheet, string strExportDate,
            string field_id, string reservoir_id, string dome_id, ref int row_idx, ref int count_well)
        {
            //---------Chuyen dome_id sang dome_name de xuat ra file excel---------------
            string dome_name_r = null;
            string dome_name_r_sum = null;

            if (dome_id != "0")
            {
                foreach (DataRow dr in database.FillDataTable(
                    "SELECT DOME_NAME_R, DOME_NAME_R_1 FROM VSP_DOME WHERE DOME_ID = " 
                    + dome_id, CommandType.Text).Rows)
                {
                    dome_name_r = dr[0].ToString();
                    dome_name_r_sum = dr[1].ToString();
                }

                //Doi voi tang Miocen Rong them Khu vuc Trung tam o truoc СЕВЕРНАЯ ЧАСТЬ
                if (dome_id == "13")
                {
                    row_idx++;
                    worksheet.SetCellValue(row_idx, col_title, "ЦЕНТРАЛЬНЫЙ УЧАСТОК");
                    worksheet.Range[row_idx, col_title].Style.Font.FontName = "Times New Roman";
                    worksheet.Range[row_idx, col_title].Style.Font.Size = 10;
                    worksheet.Range[row_idx, col_title].Style.Font.IsBold = true;
                    worksheet.Range[row_idx, col_title].Style.HorizontalAlignment = HorizontalAlignType.Center;
                }

                row_idx++;
                worksheet.SetCellValue(row_idx, col_title, dome_name_r);
                worksheet.Range[row_idx, col_title].Style.Font.FontName = "Times New Roman";
                worksheet.Range[row_idx, col_title].Style.Font.Size = 10;
                worksheet.Range[row_idx, col_title].Style.Font.IsBold = true;
                worksheet.Range[row_idx, col_title].Style.HorizontalAlignment = HorizontalAlignType.Center;

                //Doi voi tang Mong Rong Khu vuc Trung tam chen them ЮЖНАЯ ЧАСТЬ
                if (dome_id == "22")
                {
                    row_idx++;
                    worksheet.SetCellValue(row_idx, col_title, "ЮЖНАЯ ЧАСТЬ");
                    worksheet.Range[row_idx, col_title].Style.Font.FontName = "Times New Roman";
                    worksheet.Range[row_idx, col_title].Style.Font.Size = 10;
                    worksheet.Range[row_idx, col_title].Style.Font.IsBold = true;
                    worksheet.Range[row_idx, col_title].Style.HorizontalAlignment = HorizontalAlignType.Center;
                }
                //----------------------------------------------------------------------------------
            }
            
            string sqlGetZone = "SELECT DISTINCT P.ZONE_ID, Z.ORDER_NUM "
                + " FROM VSP_CONDENSATE P, VSP_ZONE Z "
                + " WHERE P.ZONE_ID = Z.ZONE_ID AND P.FIELD_ID = '" + field_id 
                + "' AND P.RESERVOIR_ID = " + reservoir_id + " AND P.DOME_ID = " + dome_id
                + " AND P.MONTH = CONVERT(DATETIME, '" + strExportDate + "', 101) ORDER BY Z.ORDER_NUM";

            //chon lan luot tung zone
            foreach (DataRow dr_zone in database.FillDataTable(sqlGetZone, CommandType.Text).Rows)
            {
                string zone_id = dr_zone[0].ToString();
                FillDataForEachZone(worksheet, strExportDate, field_id, reservoir_id,
                    dome_id, zone_id, ref row_idx, ref count_well);
            }
            
            //Fill summary DOME
            if (dome_id != "0")
            {
                string strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                    + "' AND DOME_ID = '" + dome_id + "'";
                object[] objSum = new object[13];
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
                objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
                objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
                FillSummary(worksheet, "ИТОГО ПО " + dome_name_r_sum, ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

                //Fill summary for New wells:
                strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                    + "' AND DOME_ID = '" + dome_id + "' AND STATUS = 'M'"; //new well
                objSum = new object[13];
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
                objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
                objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
                FillSummary(worksheet, "В Т.Ч. НОВ.СКВ.:", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);

                //Fill summary for Old wells:
                strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                    + "' AND DOME_ID = '" + dome_id + "' AND STATUS = 'C'"; //old well
                objSum = new object[13];
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
                objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
                objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
                FillSummary(worksheet, "ПЕР.СКВ.:", ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);
            }
            

        }

        //This function will export all well in rowsTable that have the same: field_id, reservoir_id, dome_id, zone_id
        //in this function we use filter criteria to select data in rowsTable
        //zone_name get from VSP_ZONE table in DB
        private void FillDataForEachZone(Spire.Xls.Worksheet worksheet, string strExportDate, string field_id, string reservoir_id,
                    string dome_id, string zone_id, ref int row_idx, ref int count_well)
        {
            string sqlGetMethod = null;
            string zone_name_r = null;
            string zone_name_r_sum = null;
            if (zone_id != "0")
            {
                //---------Chuyen zone_id sang zone_name de xuat ra file excel---------------
                foreach (DataRow dr in database.FillDataTable(
                    "SELECT ZONE_NAME_R, ZONE_NAME_R_1 FROM VSP_ZONE WHERE ZONE_ID = " + zone_id, CommandType.Text).Rows)
                {
                    zone_name_r = dr[0].ToString();
                    zone_name_r_sum = dr[1].ToString();
                }
                row_idx++;
                worksheet.SetCellValue(row_idx, col_title, zone_name_r);
                worksheet.Range[row_idx, col_title].Style.Font.FontName = "Times New Roman";
                worksheet.Range[row_idx, col_title].Style.Font.Size = 10;
                worksheet.Range[row_idx, col_title].Style.Font.IsBold = true;
                worksheet.Range[row_idx, col_title].Style.HorizontalAlignment = HorizontalAlignType.Center;

                sqlGetMethod = "SELECT DISTINCT P.METHOD_ID, M.ORDER_NUM "
                + " FROM VSP_CONDENSATE P, VSP_METHOD M "
                + " WHERE P.METHOD_ID = M.METHOD_ID AND P.FIELD_ID = '" + field_id
                + "' AND P.RESERVOIR_ID = " + reservoir_id 
                + " AND P.DOME_ID = " + dome_id + " AND P.ZONE_ID = " + zone_id 
                + " AND P.MONTH = CONVERT(DATETIME, '" + strExportDate + "', 101) ORDER BY M.ORDER_NUM";
            }
            else
            {
                sqlGetMethod = "SELECT DISTINCT P.METHOD_ID, M.ORDER_NUM "
                    + " FROM VSP_CONDENSATE P, VSP_METHOD M "
                    + " WHERE P.METHOD_ID = M.METHOD_ID AND "
                    + " P.FIELD_ID = '" + field_id + "' AND P.RESERVOIR_ID = " + reservoir_id
                    + " AND P.DOME_ID = " + dome_id
                    + " AND P.MONTH = CONVERT(DATETIME, '" + strExportDate + "', 101) ORDER BY M.ORDER_NUM";
            }
            //chon lan luot tung phuong phap khai thac------------GIENG CU-------------
            foreach (DataRow dr_method in database.FillDataTable(sqlGetMethod, CommandType.Text).Rows)
            {
                string method_id = dr_method[0].ToString();
                FillDataForEachMethod(worksheet, strExportDate, field_id, reservoir_id, dome_id,
                    zone_id, method_id, ref row_idx, ref count_well, true);
            }

            //chon lan luot tung phuong phap khai thac------------GIENG MOI-------------
            foreach (DataRow dr_method in database.FillDataTable(sqlGetMethod, CommandType.Text).Rows)
            {
                string method_id = dr_method[0].ToString();
                FillDataForEachMethod(worksheet, strExportDate, field_id, reservoir_id, dome_id,
                    zone_id, method_id, ref row_idx, ref count_well, false);
            }
            
            if (zone_id != "0")
            {
                string strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                + "' AND DOME_ID = '" + dome_id + "' AND ZONE_ID = '" + zone_id + "'";
                //Fill summary ZONE
                object[] objSum = new object[13];
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
                objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
                objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
                objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
                objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
                objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
                FillSummary(worksheet, "ИТОГО ПО " + zone_name_r_sum, ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);
            }
        }

        //This function will export all well in rowsTable that have the same: field_id, reservoir_id, dome_id, zone_id, method_id
        //In this function: old well will be exported first, after that is new well
        //in this function we use filter criteria to select data in rowsTable
        //method_name get from VSP_METHOD table in DB
        private void FillDataForEachMethod(Spire.Xls.Worksheet worksheet, string strExportDate,
            string field_id, string reservoir_id, string dome_id, string zone_id, 
            string method_id, ref int row_idx, ref int count_well, bool isOldWell)
        {
            string method_name_r = null;
            string method_name_r_sum = null;
            foreach (DataRow dr in database.FillDataTable(
                "SELECT METHOD_NAME_R, METHOD_NAME_R_1 FROM VSP_METHOD WHERE METHOD_ID = " 
                + method_id, CommandType.Text).Rows)
            {
                method_name_r = dr[0].ToString();
                method_name_r_sum = dr[1].ToString();
            }

            if (isOldWell)
            {
                //--------------------Chon cac dong trong rowsTable -> Array DataRow : Gieng CU -------------
                string strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                    + "' AND DOME_ID = '" + dome_id + "' AND ZONE_ID = '" + zone_id + "' AND METHOD_ID = '" + method_id + "' AND STATUS = 'C'";
                DataRow[] arrDataRow = rowsTable.Select(strFilter, "WELL_NUM");

                //Neu trong phuong phap nay so gieng > 0 thi moi in title cua phuong phap
                if (arrDataRow.Length > 0)
                {
                    row_idx++;
                    worksheet.SetCellValue(row_idx, col_title, "ПЕРЕХОДЯЩИЕ " + method_name_r + " СКВАЖИНЫ");
                    worksheet.Range[row_idx, col_title].Style.Font.FontName = "Times New Roman";
                    worksheet.Range[row_idx, col_title].Style.Font.Size = 10;
                    worksheet.Range[row_idx, col_title].Style.Font.IsBold = true;
                    worksheet.Range[row_idx, col_title].Style.HorizontalAlignment = HorizontalAlignType.Center;
                    
                    foreach (DataRow dr in arrDataRow)
                    {
                        FillDataRow(worksheet, dr, ref row_idx, ref count_well);
                        progressBar1.Value++;

                    }
                    
                    //Fill summary METHOD: WELL OLD
                    object[] objSum = new object[13];
                    objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
                    objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
                    objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
                    objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
                    objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
                    objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
                    objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                    objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                    objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                    objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                    objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
                    objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
                    objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
                    FillSummary(worksheet, "ИТОГО ПО " + method_name_r_sum, ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);
                    
                }

            }
            else
            {
                //--------------------Chon cac dong trong rowsTable -> Array DataRow : Gieng MOI -------------
                string strFilter = "FIELD_ID = '" + field_id + "' AND RESERVOIR_ID = '" + reservoir_id
                    + "' AND DOME_ID = '" + dome_id + "' AND ZONE_ID = '" + zone_id + "' AND METHOD_ID = '" + method_id + "' AND STATUS = 'M'";
                DataRow[] arrDataRow = rowsTable.Select(strFilter);

                //Neu trong phuong phap nay so gieng > 0 thi moi in title cua phuong phap
                if (arrDataRow.Length > 0)
                {
                    row_idx++;
                    worksheet.SetCellValue(row_idx, col_title, "НОВЫЕ " + method_name_r + " СКВАЖИНЫ");
                    worksheet.Range[row_idx, col_title].Style.Font.FontName = "Times New Roman";
                    worksheet.Range[row_idx, col_title].Style.Font.Size = 10;
                    worksheet.Range[row_idx, col_title].Style.Font.IsBold = true;
                    worksheet.Range[row_idx, col_title].Style.HorizontalAlignment = HorizontalAlignType.Center;
                    
                    foreach (DataRow dr in arrDataRow)
                    {
                        FillDataRow(worksheet, dr, ref row_idx, ref count_well);
                    }
                    //Fill summary METHOD: WELL NEW
                    object[] objSum = new object[13];
                    objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE)", strFilter), 0);
                    objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_YEAR)", strFilter), 1);
                    objSum.SetValue(rowsTable.Compute("Sum(CONDENSATE_ACC)", strFilter), 2);
                    objSum.SetValue(rowsTable.Compute("Sum(WATER)", strFilter), 3);
                    objSum.SetValue(rowsTable.Compute("Sum(WATER_YEAR)", strFilter), 4);
                    objSum.SetValue(rowsTable.Compute("Sum(WATER_ACC)", strFilter), 5);
                    objSum.SetValue(rowsTable.Compute("Sum(PROD_HOURS)", strFilter), 6);
                    objSum.SetValue(rowsTable.Compute("Sum(FILL_UP_HOURS)", strFilter), 7);
                    objSum.SetValue(rowsTable.Compute("Sum(SUSPENDED_HOURS)", strFilter), 8);
                    objSum.SetValue(rowsTable.Compute("Sum(WORKDAY_YEAR)", strFilter), 9);
                    objSum.SetValue(rowsTable.Compute("Sum(GAS)", strFilter), 10);
                    objSum.SetValue(rowsTable.Compute("Sum(GAS_YEAR)", strFilter), 11);
                    objSum.SetValue(rowsTable.Compute("Sum(GAS_ACC)", strFilter), 12);
                    FillSummary(worksheet, "ИТОГО ПО НОВ. " + method_name_r_sum, ConvertObjectToSummaryParameter(objSum, 100), ref row_idx);
                    row_idx++;
                }
            }
        }

        //This function to write to excel file 1 row of data in rowsTable
        //worksheet: is excel file
        //dr: is one datarow of rowsTable
        //row_idx: row number in excel file
        private void FillDataRow(Spire.Xls.Worksheet worksheet, DataRow dr, ref int row_idx, ref int count_well)
        {
            row_idx++;
            if (dr["STATUS_NAME"].ToString() != "-")
            {
                worksheet.SetCellValue(row_idx, 1, count_well.ToString()); // cot 1
                count_well++;
            }
            worksheet.SetCellValue(row_idx, 2, dr["WELL_NUMBER"].ToString() + "  -" ); //well number cot 2
            worksheet.Range[row_idx, 2].Style.HorizontalAlignment = HorizontalAlignType.Right;
            worksheet.SetText(row_idx, 3, GetPlatformFromUwi(dr["PLATFORM"].ToString())); //cot 3
            worksheet.Range[row_idx, 3].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet.SetCellValue(row_idx, 4, dr["STATUS_NAME"].ToString()); // METHOD_STATUS cot 4
            worksheet.SetCellValue(row_idx, 5, dr["CONDENSATE"].ToString()); // CONDENSATE cot 5
            worksheet.SetCellValue(row_idx, 6, dr["CONDENSATE_YEAR"].ToString()); // CONDENSATE_YEAR cot 6
            worksheet.SetCellValue(row_idx, 7, dr["CONDENSATE_ACC"].ToString()); // CONDENSATE_ACC cot 7

            //--water_cut
            string water_cut;
            string ss = dr["CONDENSATE"].ToString();
            string sss = dr["WATER"].ToString();
            if ((ss == "0.000") &&
                (sss == "0.000")) water_cut = "-";
            else water_cut = dr["WATER_CUT"].ToString();

            worksheet.SetCellValue(row_idx, 8, water_cut); // WATER CUT cot 8
            worksheet.Range[row_idx, 8].Style.NumberFormat = "0.0";
            worksheet.Range[row_idx, 8].Style.HorizontalAlignment = HorizontalAlignType.Right;
            worksheet.SetCellValue(row_idx, 9, dr["WATER"].ToString()); // WATER cot 9
            worksheet.SetCellValue(row_idx, 10, dr["WATER_YEAR"].ToString()); // WATER_YEAR cot 10
            worksheet.SetCellValue(row_idx, 11, dr["WATER_ACC"].ToString()); // WATER_ACC cot 11

            //--choke size
            string choke_size = "-";
            if (dr["CHOKE_SIZE_1"].ToString() == "555" & dr["CHOKE_SIZE_2"].ToString() == "555")
                choke_size = "-";
            else if ((dr["CHOKE_SIZE_1"].ToString().Length > 0) && (dr["CHOKE_SIZE_2"].ToString().Length == 0))
            {
                choke_size = dr["CHOKE_SIZE_1"].ToString();
                if (dr["CHOKE_SIZE_1"].ToString() == "500") choke_size = "без";
            }
            else if ((dr["CHOKE_SIZE_1"].ToString().Length == 0) && (dr["CHOKE_SIZE_2"].ToString().Length > 0))
            {
                choke_size = dr["CHOKE_SIZE_2"].ToString();
                if (dr["CHOKE_SIZE_1"].ToString() == "500") choke_size = "без";
            }
            else if ((dr["CHOKE_SIZE_1"].ToString().Length > 0) && (dr["CHOKE_SIZE_2"].ToString().Length > 0))
            {
                choke_size = "'" + dr["CHOKE_SIZE_1"].ToString() + "/" + dr["CHOKE_SIZE_2"].ToString();
                if (dr["CHOKE_SIZE_1"].ToString() == "500")
                    choke_size = "без/" + dr["CHOKE_SIZE_2"].ToString();
                else if (dr["CHOKE_SIZE_2"].ToString() == "500")
                    choke_size = dr["CHOKE_SIZE_1"].ToString() + "/без";
            }
            worksheet.SetCellValue(row_idx, 12, choke_size); // CHOKE_SIZE cot 12
            worksheet.Range[row_idx, 12].Style.HorizontalAlignment = HorizontalAlignType.Right;
            
            //--work day
            string work_day = "-";
            try
            {
                if (Convert.ToDouble(dr["PROD_HOURS"].ToString()) + Convert.ToDouble(dr["FILL_UP_HOURS"].ToString()) >= 0) // prod_hours + fill_up_hours
                    work_day = Math.Round(((Convert.ToDouble(dr["PROD_HOURS"].ToString()) + Convert.ToDouble(dr["FILL_UP_HOURS"].ToString())) / 24.0), 2, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }
            worksheet.SetCellValue(row_idx, 14, work_day); // work day cot 14
            worksheet.Range[row_idx, 14].Style.NumberFormat = "0.00";
            
            //--fluid traffic
            string fluid_traffic = "-";
            try
            {
                if (Convert.ToDouble(work_day) > 0)
                    fluid_traffic = Math.Round((Convert.ToDouble(dr["CONDENSATE"].ToString()) / Convert.ToDouble(work_day)), 1, MidpointRounding.AwayFromZero).ToString(); //oil / workday
            }
            catch { }
            worksheet.SetCellValue(row_idx, 13, fluid_traffic); // fluid traffic cot 13
            worksheet.Range[row_idx, 13].Style.HorizontalAlignment = HorizontalAlignType.Right;
            worksheet.SetCellValue(row_idx, 15, dr["WORKDAY_YEAR"].ToString()); // work day year cot 15
            worksheet.Range[row_idx, 15].Style.NumberFormat = "0.00";
            
            //--suspended day
            string suspended_day = "-";
            try
            {
                suspended_day = Math.Round((Convert.ToDouble(dr["SUSPENDED_HOURS"].ToString()) / 24.0), 2, MidpointRounding.AwayFromZero).ToString(); // suspended day = suspended_hours /24.0 cot 15
            }
            catch { }

            worksheet.SetCellValue(row_idx, 16, suspended_day); // suspended day = suspended_hours /24.0 cot 16
            worksheet.Range[row_idx, 16].Style.NumberFormat = "0.00";
            worksheet.SetCellValue(row_idx, 17, dr["PROD_HOURS"].ToString()); // prod_hours cot 17
            worksheet.SetCellValue(row_idx, 18, dr["FILL_UP_HOURS"].ToString()); // fill up hours cot 18
            worksheet.SetCellValue(row_idx, 19, dr["SUSPENDED_HOURS"].ToString()); // suspended cot 19
            
            //--pressure
            string pressure = "-";
            if (dr["PRESSURE_1"].ToString() == "555" & dr["PRESSURE_2"].ToString() == "555")
                pressure = "-";
            else if ((dr["PRESSURE_1"].ToString().Length > 0) && (dr["PRESSURE_2"].ToString().Length > 0))
                pressure = dr["PRESSURE_1"].ToString() + "/" + dr["PRESSURE_2"].ToString();
            else if ((dr["PRESSURE_1"].ToString().Length > 0) && (dr["PRESSURE_2"].ToString().Length == 0))
                pressure = dr["PRESSURE_1"].ToString();
            else if ((dr["PRESSURE_1"].ToString().Length == 0) && (dr["PRESSURE_2"].ToString().Length > 0))
                pressure = dr["PRESSURE_2"].ToString();

            worksheet.SetCellValue(row_idx, 20, pressure); // pressure cot 20
            worksheet.Range[row_idx, 20].Style.HorizontalAlignment = HorizontalAlignType.Center;
            worksheet.SetCellValue(row_idx, 21, dr["GAS"].ToString()); // gas cot 21
            worksheet.SetCellValue(row_idx, 22, dr["GAS_YEAR"].ToString()); // gas year cot 22
            worksheet.SetCellValue(row_idx, 23, dr["GAS_ACC"].ToString()); // gas acc cot 23
            worksheet.Range[row_idx, 21, row_idx, 23].Style.NumberFormat = "0.0";
            worksheet.SetCellValue(row_idx, 24, dr["GOR"].ToString()); // gor cot 24
            worksheet.SetCellValue(row_idx, 25, dr["REMARKS"].ToString()); // remarks cot 25
            worksheet.Range[row_idx, 1, row_idx, 25].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 1, row_idx, 25].Style.Font.Size = 7;
            worksheet.SetRowHeight(row_idx, 11);
        }

        //objSum: collection of summary includes: oil, oil_year, oil_acc, water, water_year, water_acc,
        //prod_hours, fill_up_hours, suspended_hours, workday_year, gas, gas_year, gas_acc
        //This function will calculate other information as: water_cut, oil_traffic, gor...
        private object[] ConvertObjectToSummaryParameter(object[] objSum, double dPercent)
        {
            // oil
            string oil = "0";
            try
            {
                oil = Math.Round(Convert.ToDouble(objSum[0].ToString())
                    , 0, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // oil year
            string oil_year = "0";
            try
            {
                oil_year = Math.Round(Convert.ToDouble(objSum[1].ToString())
                    , 0, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // oil acc
            string oil_acc = "0";
            try
            {
                oil_acc = Math.Round(Convert.ToDouble(objSum[2].ToString())
                    , 0, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // water
            string water = "0";
            try
            {
                water = Math.Round(Convert.ToDouble(objSum[3].ToString())
                    , 0, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // water year
            string water_year = "0";
            try
            {
                water_year = Math.Round(Convert.ToDouble(objSum[4].ToString())
                    , 0, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // water acc
            string water_acc = "0";
            try
            {
                water_acc = Math.Round(Convert.ToDouble(objSum[5].ToString())
                    , 0, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            //work day year
            string work_day_year = "0";
            try
            {
                work_day_year = Math.Round(Convert.ToDouble(objSum[9].ToString())
                    , 2, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // prod hours
            string prod_hours = "0";
            try
            {
                prod_hours = Math.Round(Convert.ToDouble(objSum[6].ToString())
                    , 0, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // fill up hours
            string fill_up_hours = "0";
            try
            {
                fill_up_hours = Math.Round(Convert.ToDouble(objSum[7].ToString())
                    , 0, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // suspended hours
            string suspended_hours = "0";
            try
            {
                suspended_hours = Math.Round(Convert.ToDouble(objSum[8].ToString())
                    , 0, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // gas
            string gas = "0";
            try
            {
                gas = Math.Round(Convert.ToDouble(objSum[10].ToString())
                    , 1, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // gas year
            string gas_year = "0";
            try
            {
                gas_year = Math.Round(Convert.ToDouble(objSum[11].ToString())
                    , 1, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            // gas acc
            string gas_acc = "0";
            try
            {
                gas_acc = Math.Round(Convert.ToDouble(objSum[12].ToString())
                    , 1, MidpointRounding.AwayFromZero).ToString();
            }
            catch { }

            //--work day
            string work_day = "0.00";
            try
            {
                if (Convert.ToDouble(prod_hours) + Convert.ToDouble(fill_up_hours) >= 0) // prod_hours + fill_up_hours
                    work_day = Math.Round(((Convert.ToDouble(prod_hours) 
                        + Convert.ToDouble(fill_up_hours)) / 24.0), 2).ToString();
            }
            catch { }

            //suspended day
            string suspended_day = "0.00";
            if (objSum[10].ToString().Length != 0) // sum of suspended day
                suspended_day = Math.Round(Convert.ToDouble(objSum[8].ToString()) / 24.0, 
                    2, MidpointRounding.AwayFromZero).ToString();

            //---water cut
            string water_cut = "-";
            double total_fluid;
            try
            {
                total_fluid = Convert.ToDouble(water) 
                    + Convert.ToDouble(oil);
            }
            catch
            {
                total_fluid = 0;
            }
            if (total_fluid > 0)
            {
                water_cut = Math.Round(Convert.ToDouble(water) * 100.0 / total_fluid, 
                    1, MidpointRounding.AwayFromZero).ToString();
            }

            // fluid traffic 
            string fluid_traffic = "-";
            double d_work_day;
            try
            {
                d_work_day = Convert.ToDouble(work_day);
            }
            catch
            {
                d_work_day = 0;
            }
            if (d_work_day > 0)
            {
                fluid_traffic = Math.Round(Convert.ToDouble(oil) * 100/ (d_work_day * dPercent) , 1, MidpointRounding.AwayFromZero).ToString();
            }

            //---gor
            string gor = "-";
            double d_oil;
            try
            {
                d_oil = Convert.ToDouble(oil);
            }
            catch
            {
                d_oil = 0;
            }
            if (d_oil > 0)
            {
                gor = Math.Round(Convert.ToDouble(gas) / d_oil * 1000.0, 
                    0, MidpointRounding.AwayFromZero).ToString();
            }

            object[] objPara = new object[18];
            objPara.SetValue(oil, 0); //oil
            objPara.SetValue(oil_year, 1); //oil year
            objPara.SetValue(oil_acc, 2); //oil acc

            objPara.SetValue(water_cut, 3); //water cut

            objPara.SetValue(water, 4); //water
            objPara.SetValue(water_year, 5); //water year
            objPara.SetValue(water_acc, 6); //water acc

            objPara.SetValue(fluid_traffic, 7); //fluid traffic
            
            objPara.SetValue(work_day, 8); //work day
            objPara.SetValue(work_day_year, 9); //work day year
            objPara.SetValue(suspended_day, 10); //suspended day

            objPara.SetValue(prod_hours, 11); //prod hours
            objPara.SetValue(fill_up_hours, 12); //fill up hours
            objPara.SetValue(suspended_hours, 13); //suspended hours

            objPara.SetValue(gas, 14); //gas
            objPara.SetValue(gas_year, 15); //gas year
            objPara.SetValue(gas_acc, 16); //gas acc

            objPara.SetValue(gor, 17); //gor
            return objPara;
        }

        //This function to write to excel file 1 row summary when finish method, zone, dome, reservoir or field
        //worksheet: is excel file
        //cellText: text appearance in the first column.

        private void FillSummary(Spire.Xls.Worksheet worksheet, string cellText, object[] objPara, ref int row_idx)
        {
            row_idx++;
            worksheet.SetCellValue(row_idx, 1, cellText);
            worksheet.SetCellValue(row_idx, 5, objPara[0].ToString());
            worksheet.SetCellValue(row_idx, 6, objPara[1].ToString());
            worksheet.SetCellValue(row_idx, 7, objPara[2].ToString());
            worksheet.SetCellValue(row_idx, 8, objPara[3].ToString());
            worksheet.Range[row_idx, 8].Style.HorizontalAlignment = HorizontalAlignType.Right;
            worksheet.SetCellValue(row_idx, 9, objPara[4].ToString());
            worksheet.SetCellValue(row_idx, 10, objPara[5].ToString());
            worksheet.SetCellValue(row_idx, 11, objPara[6].ToString());
            worksheet.SetCellValue(row_idx, 13, objPara[7].ToString());
            worksheet.Range[row_idx, 13].Style.HorizontalAlignment = HorizontalAlignType.Right;
            worksheet.SetCellValue(row_idx, 14, objPara[8].ToString());
            worksheet.SetCellValue(row_idx, 15, objPara[9].ToString());
            worksheet.SetCellValue(row_idx, 16, objPara[10].ToString());
            worksheet.Range[row_idx, 14, row_idx, 16].Style.NumberFormat = "0.00";
            worksheet.SetCellValue(row_idx, 17, objPara[11].ToString());
            worksheet.SetCellValue(row_idx, 18, objPara[12].ToString());
            worksheet.SetCellValue(row_idx, 19, objPara[13].ToString());
            worksheet.SetCellValue(row_idx, 21, objPara[14].ToString());
            worksheet.SetCellValue(row_idx, 22, objPara[15].ToString());
            worksheet.SetCellValue(row_idx, 23, objPara[16].ToString());
            worksheet.SetCellValue(row_idx, 24, objPara[17].ToString());
            worksheet.Range[row_idx, 1, row_idx, 24].Style.Font.FontName = "Times New Roman";
            worksheet.Range[row_idx, 1, row_idx, 24].Style.Font.Size = 7;
            worksheet.SetRowHeight(row_idx, 11);
        }

        //This function to get platform number from uwi string
        //MSP01 --> 01; BK06 --> БК06; RC, RP no change.
        private string GetPlatformFromUwi(string platform)
        {
            string str = platform;
            if (platform.Contains("MSP"))
                str = platform.Substring(3, platform.Length - 3);
            else if (platform.Contains("BK"))
                str = platform.Replace("BK", "БК");
            return str;
        }

        private void frmExportMonthlyGasProduction_Shown(object sender, EventArgs e)
        {
            StartExcelFile();
        }
    }

    
}
